import { createBottomTabNavigator } from 'react-navigation';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons'
import React from 'react'

import { LiveViewStack, VehiclesStack } from './stack'

export const LiveTabs = createBottomTabNavigator({
    LiveViewStack: {
        screen: LiveViewStack,
        navigationOptions: {
            tabBarLabel: 'Live View',
            tabBarIcon: ({ tintColor }) => (
                <Icon
                    name='map'
                    type='material-community'
                    size={25}
                    color={tintColor} />

            )
        },
    },
    VehiclesStack: {
        screen: VehiclesStack,
        navigationOptions: {
            tabBarLabel: 'Vehicles',
            tabBarIcon: ({ tintColor }) => (
                <Icon
                    name='car'
                    type='material-community'
                    size={25}
                    color={tintColor} />
            )
        },
    }
}, {
    tabBarOptions: {
        showLabel: true,
        activeTintColor: 'rgba(50, 154, 152, 1.0)'
    },
});

