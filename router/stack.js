
import { createStackNavigator } from 'react-navigation-stack'
import { DrawerActions } from 'react-navigation'
import { TouchableOpacity } from 'react-native-gesture-handler'
import Icon from 'react-native-vector-icons/MaterialCommunityIcons'
import React from 'react'

//pages
import LiveViewPage from '../pages/liveview'
import VehiclesPage from '../pages/vehicles'
import VehicleInfo from '../pages/vehicleinfo'
import TracksPage from '../pages/tracks'
import AlertsPage from '../pages/alerts'
import IdlingPage from '../pages/reports/idlingreport'
import SpeedingPage from '../pages/reports/speedingreport'
import StartStopPage from '../pages/reports/startstopreport'
import GeofencingPage from '../pages/reports/geofencingreport'
import ReportLocation from '../fragments/reportlocation'

const openDrawer = (navigation) => {
    return (
        <TouchableOpacity onPress={() => navigation.dispatch(DrawerActions.openDrawer())}>
            <Icon name='menu' size={25} color='rgba(50, 154, 152, 1.0)' style={{ marginLeft: 15 }} />
        </TouchableOpacity>
    )
}

const styles = {
    tintColor: 'rgba(50, 154, 152, 1.0)',
    bgColor: {
        backgroundColor: '#FFFFFF',
    },
    titleStyle: {
        fontSize: 15
    }
}

export const LiveViewStack = createStackNavigator({
    LiveView: {
        screen: LiveViewPage,
        navigationOptions: ({ navigation }) => ({
            title: 'Home',
            headerLeft: openDrawer(navigation),
            headerStyle: styles.bgColor,
            headerTintColor: styles.tintColor,
            headerTitleStyle: styles.titleStyle,
        })
    },
    VehicleInfo: {
        screen: VehicleInfo,
        navigationOptions: () => ({
            title: 'Vehicle Information',
            headerStyle: styles.bgColor,
            headerTintColor: styles.tintColor,
            headerTitleStyle: styles.titleStyle,
        })
    },
    Tracks: {
        screen: TracksPage,
        navigationOptions: () => ({
            title: 'Draw Tracks',
            headerStyle: styles.bgColor,
            headerTintColor: styles.tintColor,
            headerTitleStyle: styles.titleStyle,
        })
    }
});

export const VehiclesStack = createStackNavigator({
    Vehicles: {
        screen: VehiclesPage,
        navigationOptions: ({ navigation }) => ({
            title: 'Home',
            headerLeft: openDrawer(navigation),
            headerStyle: styles.bgColor,
            headerTintColor: styles.tintColor,
            headerTitleStyle: styles.titleStyle,
        })
    },
    VehicleInfo: {
        screen: VehicleInfo,
        navigationOptions: () => ({
            title: 'Vehicle Information',
            headerStyle: styles.bgColor,
            headerTintColor: styles.tintColor,
            headerTitleStyle: styles.titleStyle,
        })
    },
    Tracks: {
        screen: TracksPage,
        navigationOptions: () => ({
            title: 'Draw Tracks',
            headerStyle: styles.bgColor,
            headerTintColor: styles.tintColor,
            headerTitleStyle: styles.titleStyle,
        })
    }
});

export const TracksStack = createStackNavigator({
    Tracks: {
        screen: TracksPage,
        navigationOptions: ({ navigation }) => ({
            title: 'Draw Tracks',
            headerLeft: openDrawer(navigation),
            headerStyle: styles.bgColor,
            headerTintColor: styles.tintColor,
            headerTitleStyle: styles.titleStyle,
        })
    }
});

export const IdlingStack = createStackNavigator({
    Idling: {
        screen: IdlingPage,
        navigationOptions: ({ navigation }) => ({
            title: 'Idling',
            headerLeft: openDrawer(navigation),
            headerStyle: styles.bgColor,
            headerTintColor: styles.tintColor,
            headerTitleStyle: styles.titleStyle,
        })
    },
    ReportLocation: {
        screen: ReportLocation,
        navigationOptions: () => ({
            title: 'Idling location',
            headerStyle: styles.bgColor,
            headerTintColor: styles.tintColor,
            headerTitleStyle: styles.titleStyle,
        })
    }
});

export const SpeedingStack = createStackNavigator({
    Speeding: {
        screen: SpeedingPage,
        navigationOptions: ({ navigation }) => ({
            title: 'Speeding',
            headerLeft: openDrawer(navigation),
            headerStyle: styles.bgColor,
            headerTintColor: styles.tintColor,
            headerTitleStyle: styles.titleStyle,
        })
    },
    ReportLocation: {
        screen: ReportLocation,
        navigationOptions: () => ({
            title: 'Speeding location',
            headerStyle: styles.bgColor,
            headerTintColor: styles.tintColor,
            headerTitleStyle: styles.titleStyle,
        })
    }
});

export const StartStopStack = createStackNavigator({
    StartStop: {
        screen: StartStopPage,
        navigationOptions: ({ navigation }) => ({
            title: 'Start-stop',
            headerLeft: openDrawer(navigation),
            headerStyle: styles.bgColor,
            headerTintColor: styles.tintColor,
            headerTitleStyle: styles.titleStyle,
        })
    },
    ReportLocation: {
        screen: ReportLocation,
        navigationOptions: () => ({
            title: 'Start-stop location',
            headerStyle: styles.bgColor,
            headerTintColor: styles.tintColor,
            headerTitleStyle: styles.titleStyle,
        })
    }
});

export const AlertsStack = createStackNavigator({
    Alerts: {
        screen: AlertsPage,
        navigationOptions: ({ navigation }) => ({
            title: 'Alerts',
            headerLeft: openDrawer(navigation),
            headerStyle: styles.bgColor,
            headerTintColor: styles.tintColor,
            headerTitleStyle: styles.titleStyle,
        })
    }
});

export const GeofencingStack = createStackNavigator({
    Geofencing: {
        screen: GeofencingPage,
        navigationOptions: ({ navigation }) => ({
            title: 'Geofencing',
            headerLeft: openDrawer(navigation),
            headerStyle: styles.bgColor,
            headerTintColor: styles.tintColor,
            headerTitleStyle: styles.titleStyle,
        })
    },
    ReportLocation: {
        screen: ReportLocation,
        navigationOptions: () => ({
            title: 'Geofencing Information',
            headerStyle: styles.bgColor,
            headerTintColor: styles.tintColor,
            headerTitleStyle: styles.titleStyle,
        })
    }
});