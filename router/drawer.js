import { createDrawerNavigator } from 'react-navigation'
import MainDrawerNavigation from '../fragments/navdrawer'
import { Dimensions } from 'react-native'
const { width, height } = Dimensions.get('screen')

import { TracksStack, IdlingStack, SpeedingStack, StartStopStack, AlertsStack, GeofencingStack } from './stack'
import { LiveTabs } from './bottombar'

export const DrawerStack = createDrawerNavigator({
    LiveTabs: { screen: LiveTabs },
    Tracks: { screen: TracksStack },
    Idling: { screen: IdlingStack },
    Speeding: { screen: SpeedingStack },
    StartStop: { screen: StartStopStack },
    Alerts: { screen: AlertsStack },
    Geofencing: { screen: GeofencingStack }
}, {
    initialRouteName: 'LiveTabs',
    contentComponent: MainDrawerNavigation,
    drawerOpenRoute: 'DrawerOpen',
    drawerCloseRoute: 'DrawerClose',
    drawerToggleRoute: 'DrawerToggle',
    drawerWidth: Math.min(height, width) * 0.8,
})

