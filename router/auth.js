import { createStackNavigator } from 'react-navigation'

import LoginPage from '../pages/login'
import ForgotPasswordPage from '../pages/forgotpassword'

const styles = {
    tintColor: 'rgba(50, 154, 152, 1.0)',
    bgColor: {
        backgroundColor: '#FFFFFF',
    },
    titleStyle: {
        fontSize: 15
    }
}

export const AuthStack = createStackNavigator({
    Login: LoginPage,
    ForgotPassword: {
        screen: ForgotPasswordPage,
        navigationOptions: () => ({
            title: 'Forgot Password?',
            headerStyle: styles.bgColor,
            headerTintColor: styles.tintColor,
            headerTitleStyle: styles.titleStyle,
        })
    }
})