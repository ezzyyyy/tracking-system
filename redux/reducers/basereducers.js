import { UPDATE_VEHICLES, UPDATE_TOKEN, LOAD_MAP, LOAD_MARKER, LOAD_GEOFENCES, UPDATE_USER_DATA, UPDATE_ALERTS, EMPTY_PROPS, GET_USERS } from '../actions/types';
isLoaded = false;
export default function basereducers(state = initiateState, action) {
    switch (action.type) {
        case UPDATE_VEHICLES:
            return {
                ...state,
                vehicles: action.payload
            }
        case UPDATE_TOKEN:
            return {
                ...state,
                token: action.payload
            }
        case LOAD_MAP:
            return {
                ...state,
                map: action.payload
            }
        case LOAD_MARKER:
            return {
                ...state,
                marker: action.payload
            }
        case LOAD_GEOFENCES:
            return {
                ...state,
                geofences: action.payload
            }
        case UPDATE_USER_DATA:
            return {
                ...state,
                userData: action.payload
            }
        case UPDATE_ALERTS:
            return {
                ...state,
                alerts: action.payload
            }
        case GET_USERS:
            return {
                ...state,
                users: action.payload
            }
        case EMPTY_PROPS:
            return {
                ...state,
                geofences: action.payload.geofences,
                marker: action.payload.marker,
                token: action.payload.token,
                vehicles: action.payload.vehicles,
                users: action.payload.users
            }
    }

    return state;
}