import { createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import appReducers from './reducers/basereducers';
const middleware = [thunk];
const initialState = {};
const store = createStore(
    appReducers,
    initialState,
    applyMiddleware(...middleware)
);

export default store;