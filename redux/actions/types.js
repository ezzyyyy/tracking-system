// auth
export const UPDATE_VEHICLES = 'UPDATE_VEHICLES';
export const LOAD_MAP = 'LOAD_MAP';
export const LOAD_MARKER = 'LOAD_MARKER';
export const LOAD_GEOFENCES = 'LOAD_GEOFENCES';
export const UPDATE_TOKEN = 'UPDATE_TOKEN';
export const EMPTY_PROPS = 'UPDATE_TOKEN';
export const UPDATE_USER_DATA = 'UPDATE_USER_DATA';
export const UPDATE_ALERTS = 'UPDATE_ALERTS';
export const GET_USERS = 'GET_USERS';
