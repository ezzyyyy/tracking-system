import { UPDATE_VEHICLES, UPDATE_TOKEN, EMPTY_PROPS, LOAD_MAP, LOAD_MARKER, LOAD_GEOFENCES, UPDATE_USER_DATA, GET_USERS } from './types';

import AsyncStorage from '@react-native-community/async-storage';
import Intercom from 'react-native-intercom';
import moment from 'moment';

//asyncstorage
import { GetToken, GetUserCred, GetUserData, GetCompanies } from '../../api/asyncstorage'

//api
import { Login, GetVehicles, GetGeofences } from '../../api/vehicleapi'
import { GetUsers } from '../../api/authenticationapi';

let tryCount = 0;

export const UpdateUsers = () => dispatch => {
    GetToken().then(token => {
        GetUsers(token).then(data => {
            users = data.GetUsersResult;
            dispatch({
                type: GET_USERS,
                payload: users
            })
        })
    })
}

export const UpdateUserData = () => dispatch => {
    GetUserData().then((data) => {
        user = JSON.parse(data);
        if (user) {
            GetCompanies().then((data) => {
                let comList = [];
                companies = JSON.parse(data);
                companies.forEach((company) => {
                    let object = {
                        company: company.Name,
                        id: company.CompanyID
                    }
                    comList.push(object);
                })
                dispatch({
                    type: UPDATE_USER_DATA,
                    payload: {
                        user: user,
                        companies: comList
                    }
                })
            })
        }
    })
}

export const UpdateVehicle = (props) => dispatch => {
    this.isUpdate = true;

    this.setTimeout(() => {
        getVehicles(props, dispatch);
    }, 1500);

    this.timer = setInterval(() => getVehicles(props, dispatch), 10000)
}

export const UpdateToken = (data) => dispatch => {
    dispatch({
        type: UPDATE_TOKEN,
        payload: data
    });
}

export const ToggleUpdateVehicle = (status) => dispatch => {
    this.isUpdate = status;
}

export const LoadMap = (map) => dispatch => {
    dispatch({
        type: LOAD_MAP,
        payload: map
    })
}

export const LoadGeofences = () => dispatch => {
    GetToken().then(token => {
        try {
            GetGeofences(token).then((resp) => {
                dispatch({
                    type: LOAD_GEOFENCES,
                    payload: resp.GetGeofencesResult
                })
            }).catch((err) => {
                console.log(err);
            });
        } catch (err) {
            console.log(err)
        }
    })
}

export const LoadMarker = (marker) => dispatch => {
    dispatch({
        type: LOAD_MARKER,
        payload: marker
    })
}

export const LoadVehicles = (props) => dispatch => {
    getVehicles(props, dispatch);
}

const getVehicles = async (props, dispatch) => {
    let token;

    if (this.isUpdate) {
        token = await GetToken();
        userCredStr = await GetUserCred();
        var online = 0;
        var offline = 0;
        var idle = 0;

        GetVehicles(token).then((resp) => {
            if (resp == 'ACCESS_DENIED')
                throw new Error(resp);

            resp.GetVehiclesResult.map(v => {
                if (v.LastFix) {
                    ts = v.LastFix.Timestamp;
                    if (moment().diff(moment(ts), 'days') > 0)
                        offline++;
                    else
                        online++;
                }
                else
                    offline++;
            });

            dispatch({
                type: UPDATE_VEHICLES,
                payload: {
                    vehicles: resp.GetVehiclesResult,
                    online: online,
                    idle: idle,
                    offline: offline
                }
            })
        }).catch((e) => {
            // handle error
            switch (e.message) {
                case 'ACCESS_DENIED': // relogin and retry
                    if (tryCount < 3) // try 2 times
                    {

                        // try login
                        GetUserCred().then((cred) => {

                            let userCred = JSON.parse(cred);
                            Login(userCred.u_name, userCred.u_pass).then((result) => {
                                if (result.LogInResult != undefined) {
                                    AsyncStorage.setItem('USER_TOKEN', result.LogInResult);
                                    tryCount = 0;
                                } else
                                    tryCount++;
                            }).catch((e2) => { });
                        }).catch((e3) => { });

                        if (tryCount == 2) // 2 failed, logout
                        {
                            AsyncStorage.clear();
                            EmptyProps();
                            Intercom.logout()
                            props.navigation.navigate('Login');
                        }
                    }
                    break;
            }
        });
    }
}

export const EmptyProps = () => dispatch => {
    dispatch({
        type: EMPTY_PROPS,
        payload: {
            geofences: [],
            token: '',
            vehicles: {
                vehicles: [],
                online: 0,
                idle: 0,
                offline: 0
            },
            marker: null,
            title: ''
        }
    })
}