import { ApiEndpoint, ApiLogin, ApiGetUser, ApiGetCompanies, ApiResetPassword, ApiGetUsers, ApiSaveUser } from './api'

export const Login = async (username, password, deviceToken) => {
  console.log('user: ' + username, 'pass: ' + password, 'FCM: ' + deviceToken)
  return fetch(ApiEndpoint + ApiLogin, {
    method: 'POST',
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
    },
    body: JSON.stringify({
      Username: username,
      Password: password,
      Reserved: '1|' + deviceToken
    }),
  })
    .then((response) => response.json())
}

export const GetUser = async (token) => {
  return fetch(ApiEndpoint + ApiGetUser, {
    method: 'POST',
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
    },
    body: JSON.stringify({
      Key: token
    }),
  })
    .then((response) => {
      if (response.status === 200) {
        return response.json()
      } else {
        console.log('Failed to retrieve user data');
      }
    })
}

export const GetCompanies = async (token, page) => {
  return fetch(ApiEndpoint + ApiGetCompanies, {
    method: 'POST',
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
    },
    body: JSON.stringify({
      Key: token,
      CompanyID: page,
    }),
  })
    .then((response) => {
      if (response.status === 200) {
        return response.json()
      } else {
        console.log(`Failed to retrieve companies' data`);
      }
    })
}

export const ResetPassword = async (username) => {
  return fetch(ApiEndpoint + ApiResetPassword, {
    method: 'POST',
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
    },
    body: JSON.stringify({
      User: username
    }),
  })
    .then((response) => response.json())
}

export const GetUsers = async (token) => {
  return fetch(ApiEndpoint + ApiGetUsers, {
    method: 'POST',
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
    },
    body: JSON.stringify({
      Key: token,
      Driver: true,
    }),
  })
  .then((response) => {
    if (response.status === 200) {
      return response.json()
    } else {
      console.log(`Failed to retrieve users' data`);
    }
  })
}

export const SaveUser = async (token, user) => {
  console.log(token, user)
  return fetch(ApiEndpoint + ApiSaveUser, {
    method: 'POST',
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
    },
    body: JSON.stringify({
      Key: token,
      User: user,
    }),
  })
    .then((response) => response.json())
}