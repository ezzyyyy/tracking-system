import AsyncStorage from '@react-native-community/async-storage';

export const GetToken = async () => {
    const token = await AsyncStorage.getItem('USER_TOKEN');
    return token;
}

export const GetFCMToken = async () => {
    const fcmToken = await AsyncStorage.getItem('FCM_TOKEN');
    return fcmToken;
}

export const GetAlerts = async () => {
    const alerts = await AsyncStorage.getItem('ALERTS');
    return JSON.parse(alerts);
}

export const GetUserData = async () => {
    const user = await AsyncStorage.getItem('USER');
    return user;
}

export const GetUserCred = async () => {
    const userCred = await AsyncStorage.getItem('USER_CRED');
    return userCred;
}

export const GetCompanies = async () => {
    const companies = await AsyncStorage.getItem('USER_COMPANIES');
    return companies;
}

export const GetCompanyID = async () => {
    const companyid = await AsyncStorage.getItem('COMPANY_ID');
    return JSON.parse(companyid);
}

export const GetMap = async () => {
    const mapType = await AsyncStorage.getItem('MAPTYPE');
    return mapType;
}
