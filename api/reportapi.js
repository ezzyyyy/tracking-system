
import { ApiEndpoint, ApiGetPositionReport, ApiGetIdlingReport, ApiGetSpeedingReport, ApiGetJourneyReport, ApiGetMessageReport, ApiGetUtilizationReport, ApiGetGeofencingReport, ApiGetDrivingReport } from './api'

export const GetIdlingReport = async (token, start, end, vehicle, page, company) => {
  return fetch(ApiEndpoint + ApiGetIdlingReport, {
    method: 'POST',
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
    },
    body: JSON.stringify({
      Key: token,
      Start: start,
      End: end,
      Filter: {
        VehicleID: vehicle,
        IdlingID: page,
        Group: {
          CompanyID: company
        }
      }
    })
  })
  .then((response) => response.json())
}

export const GetSpeedingReport = async (token, start, end, vehicle, page, company) => {
  return fetch(ApiEndpoint + ApiGetSpeedingReport, {
    method: 'POST',
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
    },
    body: JSON.stringify({
      Key: token,
      Start: start,
      End: end,
      Filter: {
        VehicleID: vehicle,
        SpeedingID: page,
        CompanyID: company
      }
    })
  })
  .then((response) => response.json())
}

export const GetJourneyReport = async (token, start, end, vehicle, page, company) => {
  return fetch(ApiEndpoint + ApiGetJourneyReport, {
    method: 'POST',
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
    },
    body: JSON.stringify({
      Key: token,
      Start: start,
      End: end,
      Filter: {
        VehicleID: vehicle,
        JourneyID: page,
        CompanyID: company
      }
    }),
  })
  .then((response) => response.json())
}

export const GetPositionReport = async(token, start, end, vehicleID, posID) => {
  return fetch(ApiEndpoint + ApiGetPositionReport, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
          Key: token,
          Start: start,
          End: end,
          Filter: {
              VehicleID: vehicleID,
              PosID: posID
          }
      })
  })
  .then((response) => response.json());
};

export const GetMessageReport = async (token, vehicle, start, end, page, company) => {
  return fetch(ApiEndpoint + ApiGetMessageReport, {
    method: 'POST',
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
    },
    body: JSON.stringify({
      Key: token,
      Start: start,
      End: end,
      Filter: {
        MessageID: page,
        TypeID: -1,
        VehicleID: vehicle,
        Group: {
          CompanyID: company
        }
      }, 
    })
  })
    .then((response) => response.json());
};

export const GetGeofencingReport = async (token, start, end, vehicle, page, company) => {
  return fetch(ApiEndpoint + ApiGetGeofencingReport, {
    method: 'POST',
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
    },
    body: JSON.stringify({
      Key: token,
      Start: start,
      End: end,
      Filter: {
        GeofencingID: page,
        VehicleID: vehicle,
        Group: {
          CompanyID: company
        }
      }, 
    })
  })
    .then((response) => response.json());
};

export const GetUtilizationReport = async (token, start, end, vehicle, page, company) => {
  return fetch(ApiEndpoint + ApiGetUtilizationReport, {
    method: 'POST',
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
    },
    body: JSON.stringify({
      Key: token,
      Start: start,
      End: end,
      Filter: {
        UtilID: page,
        VehicleID: vehicle,
        Group: {
          CompanyID: company
        }
      }, 
    })
  })
    .then((response) => response.json());
};

export const GetDrivingReport = async (token, start, end, driver, page, company) => {
  return fetch(ApiEndpoint + ApiGetDrivingReport, {
    method: 'POST',
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
    },
    body: JSON.stringify({
      Key: token,
      Start: start,
      End: end,
      Filter: {
        DrivingID: page,
        DriverID: driver,
        Group: {
          CompanyID: company
        }
      }, 
    })
  })
    .then((response) => response.json());
};