# Introduction
Track your vehicles anytime, anywhere! Our 24/7 Live Tracking feature captures vehicular information such as mileage, speed, idle timings, and location. 

### Now available in both iOS and Android devices with Skyfy VTS Mobile 2.0.

# Installation
Use the package manager [npm] to install VTS app:
```bash
npm install
```
Upon finishing installation, if met with CFBundleIdentifier error:
```bash
cd ./node_modules/react-native/third-party/glog-0.3.5 && ../../scripts/ios-configure-glog.sh;
```
iOS build: 
```bash
react-native run-ios
```
Android build (ensure that an AVD is ready for build):
```bash
react-native run-android
```

# Deployment
Check out the app on: 
[APP STORE](https://apps.apple.com/sg/app/skyfy-vts-2-0/id1449590451)
[PLAY STORE](https://play.google.com/store/apps/details?id=com.vts2&hl=en_SG)

Developed by: Gian Carlo Dimaculangan, Ezra Yeoshua