import { View, ScrollView, Text, TouchableOpacity, Alert, Linking } from 'react-native'
import React from 'react'
import { Icon } from 'react-native-elements'
import Dialog from 'react-native-dialog';

//redux 
import { connect } from 'react-redux'
import { UpdateUsers } from '../redux/actions/pages'

//api
import { SaveUser } from '../api/authenticationapi'

var moment = require('moment')
moment().format()

class BottomSheet extends React.Component {
    constructor(props) {
        super(props);
        this.props.UpdateUsers();
        this.state = {
            dialogVisible: false,
            contactNumber: '',
            isInvalid: false
        }
    }

    checkPhone(phone, btn, driverId) {
        if (!!driverId) {
            if (!!phone) {
                if (btn === 'whatsapp')
                    Linking.openURL(`https://wa.me/${phone}/`)
                else
                    Linking.openURL(`tel:${phone}`)
            } else
                this.setState({ dialogVisible: true })
        } else {
            Alert.alert(
                'Oops!',
                'Only when there is a driver assigned can contact number be added. Please assign a driver to this vehicle using VTS web portal.');
        }
    }

    isInvalid = () => this.setState({ dialogVisible: true })
    onCancel = () => this.setState({ dialogVisible: false })
    onChangeInput = typedNum => this.setState({ contactNumber: typedNum })

    async onPressSave(user) {
        let userObj = user;
        if (this.state.contactNumber == '' || isNaN(this.state.contactNumber))
            this.setState({ isInvalid: true })
        else {
            this.setState({ isInvalid: false })
            userObj.Phone = this.state.contactNumber;
            let response = await SaveUser(this.props.token, userObj)
            result = response.SaveUserResult;
            if (result.Message === 'OK') {
                Alert.alert(
                    'Success!',
                    `${user.Name}'s phone number has been saved.`,
                    [{ text: 'OK', onPress: () => { } }]
                );
                this.setState({ dialogVisible: false })
                this.props.UpdateUsers();
            } else {
                Alert.alert(
                    'Error!',
                    'There was a problem with the request. Please try again. ',
                    [{ text: 'OK', onPress: () => { } }]
                );
            }
        }
    }

    render() {
        const { vehicle, sendDetails, closeBS, goToTracks } = this.props;
        let vID = vehicle.VehicleID;
        let token;
        let vehicles = [];
        let activeVeh = null;

        let driver = '';
        let lastloc = 'No last location';
        let timestamp = 'No last timestamp';
        let speed = '0';
        let ignitionColor = 'grey';
        let ignition = 'OFF';
        let mileage = '0km';
        let engColor = '#2980b9';
        let phone;
        let selectedUser;
        let driverId;

        if (this.props) {
            if (!!this.props.token) {
                token = this.props.token;

                if (!!this.props.vehicles) {
                    vehicles = this.props.vehicles.vehicles;
                    vehicles.map(v => {
                        if (v.VehicleID === vID) {

                            if (!!v.LastFix) {
                                if (moment().diff(moment(v.LastFix.Timestamp), 'days') == 0) {
                                    if (v.LastFix.Engine == 1)
                                        engColor = 'orange';
                                    else if (v.LastFix.Engine == 2)
                                        engColor = '#27ae60';
                                }
                                else
                                    engColor = '#2980b9';
                            } else
                                engColor = 'grey';

                            activeVeh = v;
                            driver = !!activeVeh.Driver ? ' (' + activeVeh.Driver + ')' : driver;

                            let loc = !!activeVeh.LastFix ? activeVeh.LastFix.Location : 'No last location';
                            let cut = loc.split(',');
                            lastloc = !!activeVeh.LastFix ? (cut[0] + ',' + cut[1] + ',' + cut[2]) : 'No last location';

                            timestamp = !!activeVeh.LastFix ? activeVeh.LastFix.Timestamp : timestamp;
                            speed = !!activeVeh.LastFix && !!activeVeh.LastFix.Speed ? activeVeh.LastFix.Speed : speed;
                            ignitionColor = !!activeVeh.LastFix && !!activeVeh.LastFix.Ignition ? activeVeh.LastFix.Ignition === 1 ? '#27ae60' : 'grey' : ignitionColor;
                            ignition = !!activeVeh.LastFix && !!activeVeh.LastFix.Ignition ? activeVeh.LastFix.Ignition === 1 ? 'ON' : 'OFF' : ignition;
                            mileage = !!activeVeh.Mileage ? activeVeh.Mileage + 'km' : mileage;

                            driverId = activeVeh.DriverID;
                        }
                    })
                }

                if (!!this.props.users) {
                    let { users } = this.props;

                    users.map((user) => {
                        if (user.UserID === driverId) {
                            phone = user.Phone;
                            selectedUser = user;
                        }
                    })
                }
            }
        }

        return (
            <View pointerEvents='box-none' style={{ flex: 1 }}>
                <View pointerEvents='box-none' style={{ flex: 1, backgroundColor: 'transparent', flexDirection: 'row' }}>
                    <View pointerEvents='none' style={{ justifyContent: 'center', alignItems: 'center' }}>
                        <View style={{ margin: 10, backgroundColor: '#FFFFFF', width: 60, height: 60, borderColor: '#f2f2f2', borderWidth: 1, borderRadius: 30, alignItems: 'center', justifyContent: 'center' }}>
                            <Text style={{ fontSize: 16, fontWeight: 'bold', color: engColor }}>{speed}</Text>
                            <Text style={{ fontSize: 11, fontWeight: 'bold', color: engColor }}>KM/H</Text>
                        </View>
                    </View>
                    <View pointerEvents='none' style={{ flex: 1 }}>
                    </View>
                </View>
                <View pointerEvents='auto' style={{ flex: 1, paddingTop: 15, backgroundColor: '#FFFFFF', borderTopWidth: 1, borderTopColor: '#f2f2f2' }}>
                    <Dialog.Container visible={this.state.dialogVisible}>
                        <Dialog.Title style={{ fontSize: 15, color: '#3d3f4c' }}>Enter contact number</Dialog.Title>
                        <Dialog.Description style={{ fontSize: 13 }}>This driver does not have a saved phone number. Enter contact number below.</Dialog.Description>
                        <Dialog.Input
                            placeholder='e.g 12345678'
                            underlineColorAndroid='#abadac50'
                            style={{ paddingLeft: 10 }}
                            keyboardType={'phone-pad'}
                            onChangeText={this.onChangeInput} />
                        {this.state.isInvalid === true &&
                            <Dialog.Description style={{ fontSize: 13, color: '#FF0000' }}>Invalid number. Please try again.</Dialog.Description>}
                        <Dialog.Button label='LATER' onPress={this.onCancel} />
                        <Dialog.Button label='SAVE' onPress={() => this.onPressSave(selectedUser)} />
                    </Dialog.Container>
                    <View style={{ flex: 1, paddingHorizontal: 15, flexDirection: 'row', paddingBottom: 15 }}>
                        <TouchableOpacity style={{ width: '85%' }} onPress={() => sendDetails(vID, driverId)}>
                            <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'center' }}>
                                <Text style={{ fontSize: 15, color: '#3d3f4c', fontWeight: 'bold' }}>{activeVeh.Name + driver}</Text>
                                <Icon
                                    iconStyle={{ marginLeft: 5, color: engColor }}
                                    size={20}
                                    type='material-community'
                                    name='record' />
                            </View>
                            <Text style={{ fontSize: 13, color: '#abadac' }}>{lastloc}</Text>
                            <Text style={{ fontSize: 13, color: '#abadac' }}>{moment.unix(timestamp.substring(6, 19) / 1000).format('DD/MM/YYYY hh:mm:ss A')}</Text>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={() => { closeBS() }} style={{ flex: 1, padding: 5, justifyContent: 'center', alignItems: 'flex-end', flexDirection: 'column' }}>
                            <Icon name='chevron-down' size={20} type='material-community' iconStyle={{ color: '#3d3f4c' }} />
                        </TouchableOpacity>
                    </View>
                    <View style={{ justifyContent: 'space-between', alignItems: 'center', flexDirection: 'row', borderTopColor: '#f2f2f2', borderTopWidth: 1, padding: 10 }}>
                        <TouchableOpacity onPress={() => { this.checkPhone(phone, 'call', driverId) }} style={{ flex: 1, padding: 5, justifyContent: 'center', alignItems: 'center', flexDirection: 'column' }}>
                            <Icon name='phone' size={20} type='material-community' iconStyle={{ color: '#3d3f4c' }} />
                        </TouchableOpacity>
                        <TouchableOpacity onPress={() => { this.checkPhone(phone, 'whatsapp', driverId) }} style={{ flex: 1, padding: 5, justifyContent: 'center', alignItems: 'center', flexDirection: 'column' }}>
                            <Icon name='whatsapp' size={20} type='material-community' iconStyle={{ color: '#3d3f4c' }} />
                        </TouchableOpacity>
                        <TouchableOpacity onPress={() => {
                            let trackReqObj = {
                                vehicleID: vID,
                                start: '/Date(' + moment.utc().subtract(1, 'day').unix() * 1000 + ')/',
                                end: '/Date(' + moment().unix() * 1000 + ')/'
                            }
                            goToTracks(trackReqObj)
                        }} style={{ flex: 1, padding: 5, justifyContent: 'center', alignItems: 'center', flexDirection: 'column' }}>
                            <Icon name='navigation' size={20} type='material-community' iconStyle={{ color: '#3d3f4c' }} />
                        </TouchableOpacity>
                    </View>
                </View>
            </View>
        );
    }
}

function mapStateToProps(state) {
    return {
        vehicles: state.vehicles,
        token: state.token,
        users: state.users
    }
}

const mapDispatchToProps = {
    UpdateUsers
};

export default connect(mapStateToProps, mapDispatchToProps)(BottomSheet)