import React from 'react';
import { View, Text, Image, Platform } from 'react-native';
import { Marker } from 'react-native-maps'
import moment from 'moment';

//assets
import m_blue from '../assets/imgs/icons/m_blue.png';
import v_blue from '../assets/imgs/icons/v_blue.png';
import m_orange from '../assets/imgs/icons/m_orange.png';
import v_orange from '../assets/imgs/icons/v_orange.png';
import m_green from '../assets/imgs/icons/m_green.png';
import v_green from '../assets/imgs/icons/v_green.png';
import v_gray from '../assets/imgs/icons/v_gray.png';
import v_course from '../assets/imgs/icons/course.png';

//styles
import { styles } from '../assets/styles/markerstyle';

export default class LiveMapMarker extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            tracksViewChanges: true
        }
    }

    componentDidUpdate() {
        if (this.state.tracksViewChanges)
            this.setState({ tracksViewChanges: false });
    }

    render() {
        let vehicle = this.props.vinfo;
        let enStat = m_blue;
        let lat;
        let lng;
        let name;
        let lastfix;
        let eng;
        let course;
        let engColor = '#2980b9';
        let cat;

        enStat = m_blue;


        if (vehicle.LastFix) {
            lastfix = vehicle.LastFix;
            lat = vehicle.LastFix.Latitude;
            lng = vehicle.LastFix.Longitude;
            name = vehicle.Name;
            course = vehicle.LastFix.Course ? vehicle.LastFix.Course : '0';
            eng = vehicle.LastFix.Engine;
            cat = vehicle.Category;

            if (vehicle.Category == 'Mobile Device') {
                if (moment().diff(moment(vehicle.LastFix.Timestamp), 'days') == 0) {
                    if (vehicle.LastFix.Engine == 1) {
                        enStat = m_orange;
                        engColor = '#E8840E';
                    } else if (vehicle.LastFix.Engine == 2) {
                        enStat = m_green;
                        engColor = '#27ae60';
                    }
                }
                else {
                    enStat = v_gray;
                    engColor = 'grey';
                }
            }
            else {
                if (moment().diff(moment(vehicle.LastFix.Timestamp), 'days') == 0) {
                    if (vehicle.LastFix.Engine == 1) {
                        enStat = v_orange;
                        engColor = '#E8840E';
                    } else if (vehicle.LastFix.Engine == 2) {
                        enStat = v_green;
                        engColor = '#27ae60';
                    }
                    else {
                        enStat = v_blue;
                        engColor = '#2980b9';
                    }
                } else {
                    enStat = v_gray;
                    engColor = 'grey';
                }
            }
        } else {
            enStat = v_gray;
            engColor = 'grey';
        }

        return (
            <Marker
                onPress={this.props.onPress}
                key={'marker' + vehicle.VehicleID}
                identifier={'marker' + vehicle.VehicleID}
                ref={ref => this.marker = ref}
                coordinate={{ latitude: lat, longitude: lng }}
                tracksViewChanges={this.state.tracksViewChanges}>
                <View style={styles.markerView}>
                    <View style={styles.calloutView}>
                        <Text style={styles.calloutText}>{name}</Text>
                        {/* {
                            cat !== 'Mobile Device' && (
                                <View style={{
                                    width: 10,
                                    height: 10
                                }}>
                                    <Image
                                        style={{
                                            transform: [{ rotate: course + 'deg' }],
                                            width: '100%',
                                            height: '100%',
                                            tintColor: engColor
                                        }}
                                        source={v_course} />
                                </View>
                            )
                        } */}
                    </View>
                    <View style={styles.imageView}>
                        <Image style={styles.image} source={enStat} />
                    </View>
                </View>
            </Marker>
        )
    }
}