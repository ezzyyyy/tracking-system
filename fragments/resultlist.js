import React, { Component } from 'react';
import { Text, FlatList, View, TouchableOpacity } from 'react-native';
import { Icon, Card } from 'react-native-elements';
import Hyperlink from 'react-native-hyperlink'

//redux 
import { connect } from 'react-redux'

//styles
import { styles, idlingStyles, speedingStyles, startStopStyles, messageStyles, geoStyles } from '../assets/styles/resultliststyle'

var moment = require('moment');
moment().format();
const dateformat = 'MM/DD/YYYY hh:mm:ss A';

class ResultList extends Component {
    constructor(props) {
        super(props);
        this.iconType = 'material-community';
        this.state = {
            vehicle: undefined,
            driver: undefined,
            vehicles: []
        }
    }

    static getDerivedStateFromProps(nextProps, prevState) {
        if (prevState !== nextProps) {
            return {
                dataArray: nextProps.dataArray
            }
        }
    }

    _renderStartStopRow = ({ item }) => {
        let data = item;

        let startTime = 'No recorded timestamp';
        let endTime = 'No recorded timestamp';

        let locEnd = item.EndLocation;
        let cutEnd = locEnd.split(',');
        let cutEndLoc = (cutEnd[0] + ',' + cutEnd[1]);

        let locStart = item.StartLocation;
        let cutStart = locStart.split(',');
        let cutStartLoc = (cutStart[0] + ',' + cutStart[1]);

        if (item.StartTime && item.EndTime) {
            startTime = moment(new Date(parseInt(item.StartTime.substring(6, 19)))).format(dateformat)
            endTime = moment(new Date(parseInt(item.EndTime.substring(6, 19)))).format(dateformat)
        }

        let dur = '00:00:00';
        dur = moment(moment.duration(item.Duration * 60 * 1000)._data).format('HH:mm:ss');

        return (
            <TouchableOpacity
                style={styles.cardItem}
                onPress={() => {
                    this.props.goToStartStopMap({
                        reportType: 'StartStop',
                        start: item.StartLocation,
                        end: item.EndLocation,
                        startTime,
                        endTime,
                        latlngArray: data.Path.map(element => {
                            latitude = element.Latitude;
                            longitude = element.Longitude;
                            return { latitude, longitude }
                        }),
                    });
                }}>
                <View style={speedingStyles.detailsBody}>
                    <View style={speedingStyles.locationBody}>
                        <Text style={speedingStyles.label}>Start: </Text>
                        <Text style={speedingStyles.locMarquee}>{cutStartLoc}</Text>
                    </View>
                    <View style={speedingStyles.timeBody}>
                        <View style={speedingStyles.startTimeGroup}>
                            <Text style={speedingStyles.time}>{moment(data.StartTime).format('LTS')}</Text>
                            <Text style={speedingStyles.date}>{moment(data.StartTime).format('l')}</Text>
                        </View>
                        <View style={speedingStyles.lineDurationBody}>
                            <View style={speedingStyles.line} />
                            <Text style={speedingStyles.duration}>{!!data.Duration && !isNaN(data.Duration) ? dur : '00:00:00'}</Text>
                        </View>
                        <View style={speedingStyles.endTimeGroup}>
                            <Text style={speedingStyles.time}>{moment(data.EndTime).format('LTS')}</Text>
                            <Text style={speedingStyles.date}>{moment(data.EndTime).format('l')}</Text>
                        </View>
                    </View>
                    <View style={speedingStyles.locationBody}>
                        <Text style={speedingStyles.label}>End: </Text>
                        <Text style={speedingStyles.locMarquee}>{cutEndLoc}</Text>
                    </View>
                </View>
            </TouchableOpacity>
        );
    }

    _renderSpeedingRow = ({ item }) => {
        let data = item;

        let startTime = 'No recorded timestamp';
        let endTime = 'No recorded timestamp';

        let locEnd = item.EndLocation;
        let cutEnd = locEnd.split(',');
        let cutEndLoc = (cutEnd[0] + ',' + cutEnd[1]);

        let locStart = item.StartLocation;
        let cutStart = locStart.split(',');
        let cutStartLoc = (cutStart[0] + ',' + cutStart[1]);

        if (item.StartTime && item.EndTime) {
            startTime = moment(new Date(parseInt(item.StartTime.substring(6, 19)))).format(dateformat)
            endTime = moment(new Date(parseInt(item.EndTime.substring(6, 19)))).format(dateformat)
        }

        let dur = '00:00:00';
        dur = moment(moment.duration(item.Duration * 60 * 1000)._data).format('HH:mm:ss');

        return (
            <TouchableOpacity
                key={item.SpeedingID}
                style={styles.cardItem}
                onPress={() => {
                    this.props.goToSpeedingMap({
                        reportType: 'Speeding',
                        start: cutStartLoc,
                        end: cutEndLoc,
                        startTime,
                        endTime,
                        latlngArray: data.Path.map(element => {
                            latitude = element.Latitude;
                            longitude = element.Longitude;
                            return { latitude, longitude }
                        }),
                    });
                }}>
                <View style={speedingStyles.detailsBody}>
                    <View style={speedingStyles.locationBody}>
                        <Text style={speedingStyles.label}>Start: </Text>
                        <Text style={speedingStyles.locMarquee}>{cutStartLoc}</Text>
                    </View>
                    <View style={speedingStyles.timeBody}>
                        <View style={speedingStyles.startTimeGroup}>
                            <Text style={speedingStyles.time}>{moment(data.StartTime).format('LTS')}</Text>
                            <Text style={speedingStyles.date}>{moment(data.StartTime).format('l')}</Text>
                        </View>
                        <View style={speedingStyles.lineDurationBody}>
                            <Text style={speedingStyles.speedText}>{data.Speed}km/h</Text>
                            <View style={speedingStyles.line} />
                            <Text style={speedingStyles.duration}>{!!data.Duration && !isNaN(data.Duration) ? dur : '00:00:00'}</Text>
                        </View>
                        <View style={speedingStyles.endTimeGroup}>
                            <Text style={speedingStyles.time}>{moment(data.EndTime).format('LTS')}</Text>
                            <Text style={speedingStyles.date}>{moment(data.EndTime).format('l')}</Text>
                        </View>
                    </View>
                    <View style={speedingStyles.locationBody}>
                        <Text style={speedingStyles.label}>End: </Text>
                        <Text style={speedingStyles.locMarquee}>{cutEndLoc}</Text>
                    </View>
                </View>
            </TouchableOpacity>
        );
    }


    _renderIdlingRow = ({ item }) => {
        let data = item;

        let loc = item.Location;
        let cut = loc.split(',');
        let cutLoc = (cut[0] + ',' + cut[1]);

        let dur = '00:00:00';
        dur = moment(moment.duration(item.Duration * 60 * 1000)._data).format('HH:mm:ss');

        return (
            <TouchableOpacity
                key={item.IdlingID}
                style={styles.cardItem}
                onPress={() => {
                    this.props.goToIdlingMap({
                        reportType: 'Idling',
                        loc: cutLoc,
                        dur,
                        latlng: {
                            latitude: data.Position.Latitude,
                            longitude: data.Position.Longitude,
                        }
                    });
                }}>
                <View style={idlingStyles.detailsBody}>
                    <View style={idlingStyles.timeBody}>
                        <View style={idlingStyles.startTimeGroup}>
                            <Text style={idlingStyles.time}>{moment(data.StartTime).format('LTS')}</Text>
                            <Text style={idlingStyles.date}>{moment(data.StartTime).format('l')}</Text>
                        </View>
                        <View style={idlingStyles.lineDurationBody}>
                            <View
                                style={idlingStyles.line}
                            />
                            {
                                !isNaN(data.Duration) ? (
                                    <Text style={idlingStyles.duration}>{!!data.Duration && !isNaN(data.Duration) ? dur : '00:00:00'}</Text>
                                ) : (<Text>--</Text>)
                            }
                        </View>
                        <View style={idlingStyles.endTimeGroup}>
                            <Text style={idlingStyles.time}>{moment(data.EndTime).format('LTS')}</Text>
                            <Text style={idlingStyles.date}>{moment(data.EndTime).format('l')}</Text>
                        </View>
                    </View>
                    <View style={idlingStyles.locationBody}>
                        <Text style={speedingStyles.label}>Location: </Text>
                        <Text style={speedingStyles.locMarquee}>{cutLoc}</Text>
                    </View>
                </View>
            </TouchableOpacity>
        )
    }

    _renderMessageRow = ({ item }) => {
        const date = (parseInt(item.Timestamp.substring(6, 19)));
        return (
            <View key={item.MessageID} style={messageStyles.sectionContainer}>
                <View style={messageStyles.detailsContainer}>
                    <Hyperlink linkStyle={messageStyles.linkStyle} linkDefault={true}>
                        <Text style={messageStyles.body}>{item.Message}</Text>
                        <Text style={messageStyles.time}>
                            {moment(new Date(date)).format(dateformat)}
                        </Text>
                    </Hyperlink>
                </View>
            </View>
        )
    }

    _renderGeofencingRow = ({ item }) => {
        let geoArr = [];
        let geofence;
        let entryTime = 'No recorded timestamp';
        let exitTime = 'No recorded timestamp';

        if (item.EntryTime && item.ExitTime) {
            entryTime = (parseInt(item.EntryTime.substring(6, 19)));
            exitTime = (parseInt(item.ExitTime.substring(6, 19)));
        }

        let dur = '00:00:00';
        dur = moment(moment.duration(item.Duration * 60 * 1000)._data).format('HH:mm:ss');

        this.props.geofences.map(g => {
            if (g.GeofenceID === item.GeofenceID) {
                geofence = g;
                g.Path.forEach(p => {
                    geoArr.push({
                        latitude: p.Latitude,
                        longitude: p.Longitude,
                    })
                    return geoArr;
                })
            }
        })

        return (
            <TouchableOpacity
                key={item.GeofencingID}
                style={styles.cardItem}
                onPress={() => {
                    this.props.goToGeofencingMap({
                        reportType: 'Geofencing',
                        entryTime,
                        exitTime,
                        dur,
                        geoArr,
                        geofence,
                        geofences: this.props.geofences
                    });
                }}>
                <View style={speedingStyles.detailsBody}>
                    <View style={speedingStyles.timeBody}>
                        <View style={speedingStyles.startTimeGroup}>
                            <Text style={speedingStyles.time}>{moment(entryTime).format('LTS')}</Text>
                            <Text style={speedingStyles.date}>{moment(entryTime).format('l')}</Text>
                        </View>
                        <View style={speedingStyles.lineDurationBody}>
                            <Text style={{ color: geofence.Color, fontSize: 11, fontWeight: 'bold' }}>{geofence.Name}</Text>
                            <View style={speedingStyles.line} />
                            <Text style={speedingStyles.duration}>{dur}</Text>
                        </View>
                        <View style={speedingStyles.endTimeGroup}>
                            <Text style={speedingStyles.time}>{moment(exitTime).format('LTS')}</Text>
                            <Text style={speedingStyles.date}>{moment(exitTime).format('l')}</Text>
                        </View>
                    </View>
                </View>
            </TouchableOpacity>
        )
    }

    render() {
        const { dataArray, resultType } = this.props;
        let res;
        let _keyExtractor;
        if (resultType === 'speeding') {
            res = this._renderSpeedingRow;
            _keyExtractor = (item, index) => item.SpeedingID.toString();
        } else if (resultType === 'idling') {
            res = this._renderIdlingRow;
            _keyExtractor = (item, index) => item.IdlingID.toString();
        } else if (resultType === 'startstop') {
            res = this._renderStartStopRow;
            _keyExtractor = (item, index) => item.JourneyID.toString();
        } else if (resultType === 'message') {
            res = this._renderMessageRow;
            _keyExtractor = (item, index) => item.MessageID.toString();
            dataArray.sort(function (a, b) { return b.MessageID - a.MessageID });
        } else if (resultType === 'geofencing') {
            res = this._renderGeofencingRow;
            _keyExtractor = (item, index) => item.GeofencingID.toString();
            dataArray.sort(function (a, b) { return b.GeofencingID - a.GeofencingID });
        }

        return (
            <FlatList
                contentContainerStyle={{ paddingBottom: 10 }}
                style={styles.list}
                keyExtractor={_keyExtractor}
                data={dataArray}
                renderItem={res}
            />
        )
    }

}

function mapStateToProps(state) {
    return {
        geofences: state.geofences
    }
}

const mapDispatchToProps = {
};

export default connect(mapStateToProps, mapDispatchToProps)(ResultList)