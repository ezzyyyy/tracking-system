import React, { Component } from 'react';
import DatePicker from 'react-native-datepicker';
import { Text, View, TouchableOpacity } from 'react-native';
import { Icon } from 'react-native-elements';
import SearchableDropdown from 'react-native-searchable-dropdown';

//styles
import { reportHeaderStyles, dropdownStyles, datePickerStyles } from '../assets/styles/reportheaderstyle'

//redux 
import { connect } from 'react-redux'

var moment = require('moment');
moment().format();

class TrackHeader extends Component {
    constructor(props) {
        super(props);
        this.iconType = 'material-community';
        this.state = {
            vehicles: [],
            vehicle: undefined,
            startDate: moment().format('M/D/YY') + ' 00:00 AM',
            endDate: moment().add(1, 'day')
        };
    }

    componentDidMount() {
        if (this.props.params) {
            let params = this.props.params;
            this.setState({
                vehicle: params.vehicleID,
                startDate: moment.unix(parseInt(params.start.substring(6, 19)) / 1000).format('M/D/YY h:mm A'),
                endDate: moment.unix(parseInt(params.end.substring(6, 19)) / 1000).format('M/D/YY h:mm A')
            })
        }
    }

    pickerValChange(value) { this.setState({ vehicle: value }) }

    startDatePickerValChange = (date) => { this.setState({ startDate: date }) }

    endDatePickerValChange = (date) => { this.setState({ endDate: date }) }

    listVehicles = () => {
        let list = [];
        this.props.vehicles.vehicles.map((v, index) => {
            list.push({ label: v.Name, value: v.VehicleID, key: index });
        });
        return list;
    }

    render() {
        let vehIndex = '';

        if (this.props.params) {
            let params = this.props.params;

            if (!!this.props.vehicles) {
                const { vehicles } = this.props.vehicles;

                vehicles.map((v, index) => {
                    if (v.VehicleID === params.vehicleID) vehIndex = index;
                });
            }
        }

        return (
            <View style={reportHeaderStyles.headerBody}>
                <View style={reportHeaderStyles.pickerGroupBody}>
                    <Text style={reportHeaderStyles.label}>Start</Text>
                    <DatePicker
                        mode='date'
                        format='M/D/YY'
                        date={this.state.startDate}
                        maxDate={new Date(this.state.endDate)}
                        confirmBtnText='Confirm'
                        cancelBtnText='Cancel'
                        style={reportHeaderStyles.date}
                        customStyles={{ ...datePickerStyles }}
                        onDateChange={this.startDatePickerValChange}
                        showIcon={false}
                    />
                    <Text style={reportHeaderStyles.label}>End</Text>
                    <DatePicker
                        mode='date'
                        format='M/D/YY'
                        date={this.state.endDate}
                        minDate={this.state.startDate}
                        maxDate={new Date(moment().add(1, 'day'))}
                        confirmBtnText='Confirm'
                        cancelBtnText='Cancel'
                        style={reportHeaderStyles.datepickerStyle}
                        customStyles={{ ...datePickerStyles }}
                        onDateChange={this.endDatePickerValChange}
                        showIcon={false}
                    />
                </View>
                <View style={reportHeaderStyles.vehiclePickerBody}>
                    <View style={dropdownStyles.viewContainer}>
                        <SearchableDropdown
                            defaultIndex={vehIndex.toString()}
                            onTextChange={text => { }}
                            onItemSelect={item => this.setState({ vehicle: item.value })}
                            containerStyle={dropdownStyles.container}
                            textInputStyle={dropdownStyles.textInput}
                            itemStyle={dropdownStyles.item}
                            itemTextStyle={dropdownStyles.itemText}
                            itemsContainerStyle={dropdownStyles.itemsContainer}
                            items={this.listVehicles()}
                            placeholder='Select Vehicle'
                            resetValue={false}
                            underlineColorAndroid='transparent'
                        />
                    </View>
                    <TouchableOpacity
                        style={{ padding: 10 }}
                        onPress={(!!this.state.vehicle && !!this.state.startDate && !!this.state.endDate) ?
                            () => { this.props.getTracks(this.state) } : () => { }}>
                        <Icon iconStyle={{ color: 'rgb(50, 154, 152)' }} type={this.iconType} name='magnify' />
                    </TouchableOpacity>
                </View>
            </View>
        )
    }
}

function mapStateToProps(state) {
    return {
        vehicles: state.vehicles,
        token: state.token
    }
}

const mapDispatchToProps = {

};

export default connect(mapStateToProps, mapDispatchToProps)(TrackHeader)