import React, { Component } from 'react';
import MapView, { Marker, Polyline, PROVIDER_GOOGLE } from 'react-native-maps';
import { View, Text } from 'react-native';
import { Icon } from 'react-native-elements';

//styles
import { styles } from '../assets/styles/reportlocationstyle';

//fragments
import Geofences from '../fragments/geofences'

var moment = require('moment');
moment().format();
const dateformat = 'MM/DD/YYYY hh:mm:ss A';

const INITIAL_REGION = {
    latitude: 1.351616,
    longitude: 103.808053,
    latitudeDelta: 0.4000,
    longitudeDelta: 0.4000,
}

const EDGE_PADDING = {
    top: 150,
    right: 50,
    bottom: 150,
    left: 50
}

export default class ReportLocation extends Component {
    constructor(props) {
        super(props);
        this.iconType = 'material-community';
    }

    componentDidUpdate() {
        const { params } = this.props.navigation.state;
        if (!!params) {
            if (params.reportType === 'StartStop') {
                this.mapRef.fitToCoordinates(params.latlngArray, { edgePadding: EDGE_PADDING });
            } else if (params.reportType === 'Idling') {
                this.mapRef.fitToCoordinates([params.latlng], { edgePadding: EDGE_PADDING });
            } else if (params.reportType === 'Speeding') {
                this.mapRef.fitToCoordinates(params.latlngArray, { edgePadding: EDGE_PADDING });
            }
        }
    }

    showIdlingMarker = (params) => {
        let marker = [];
        let orangeColor = '#e67e22';
        let title = params.loc;
        let desc = params.dur;

        marker.push(
            <Marker
                style={[styles.markerContainer, { height: 56 }]}
                key={'marker'}
                ref={ref => this.marker = ref}
                coordinate={params.latlng}
                title={title}
                description={desc}>
                <View style={[styles.vehIcon, { borderColor: orangeColor }]}>
                    <Icon
                        iconStyle={{ color: orangeColor }}
                        name='clock-alert'
                        type={this.iconType} />
                </View>
                <Icon
                    iconStyle={[styles.downIcon, { color: orangeColor }]}
                    name='menu-down'
                    type={this.iconType} />
            </Marker>
        );
        return marker;
    }

    showMarkers = (params) => {
        let markersLatLng = [];
        let polyline = [];

        let markers = [
            {
                latitude: params.latlngArray[0].latitude,
                longitude: params.latlngArray[0].longitude,
                title: params.start,
                desc: params.startTime,
                color: '#2ecc71',
                icon: 'play'
            },
            {
                latitude: params.latlngArray[params.latlngArray.length - 1].latitude,
                longitude: params.latlngArray[params.latlngArray.length - 1].longitude,
                title: params.end,
                desc: params.endTime,
                color: '#EB4F51',
                icon: 'stop'
            }
        ];

        polyline.push(
            <Polyline
                key='track'
                coordinates={params.latlngArray}
                strokeColor='rgba(230, 126, 34,1.0)'
                fillColor='rgba(230, 126, 34,1.0)'
                strokeWidth={3}
            />
        );

        markers.map((element, index) => {
            markersLatLng.push(
                <Marker
                    style={[styles.markerContainer, { height: 56 }]}
                    key={'marker' + index}
                    ref={ref => this.marker = ref}
                    coordinate={element}
                    title={element.title}
                    description={element.desc}>
                    <View style={[styles.vehIcon, { borderColor: element.color }]}>
                        <Icon
                            iconStyle={{ color: element.color }}
                            name={element.icon}
                            type={this.iconType} />
                    </View>
                    <Icon
                        iconStyle={[styles.downIcon, { color: element.color }]}
                        name='menu-down'
                        type={this.iconType} />
                </Marker>
            )
        });
        return [markersLatLng, polyline];
    }

    render() {
        const { params } = this.props.navigation.state;
        let geoArr = [];

        if (!!params) {
            if (params.reportType === 'Idling') {
                return (
                    <View style={styles.container}>
                        <MapView
                            style={styles.map}
                            provider={PROVIDER_GOOGLE}
                            ref={(ref) => this.mapRef = ref}
                            onMapReady={() => this.mapRef.fitToCoordinates([params.latlng], { edgePadding: EDGE_PADDING })}
                            initialRegion={INITIAL_REGION}>
                            {this.showIdlingMarker(params)}
                        </MapView>
                    </View>
                )
            } else if (params.reportType === 'StartStop') {
                return (
                    <View style={styles.container}>
                        <MapView
                            style={styles.map}
                            provider={PROVIDER_GOOGLE}
                            onMapReady={() => this.mapRef.fitToCoordinates(params.latlngArray, { edgePadding: EDGE_PADDING })}
                            ref={(ref) => this.mapRef = ref}
                            initialRegion={INITIAL_REGION}>
                            {this.showMarkers(params)}
                        </MapView>
                    </View>
                )
            } else if (params.reportType === 'Speeding') {
                return (
                    <View style={styles.container}>
                        <MapView
                            style={styles.map}
                            provider={PROVIDER_GOOGLE}
                            onMapReady={() => this.mapRef.fitToCoordinates(params.latlngArray, { edgePadding: EDGE_PADDING })}
                            ref={(ref) => this.mapRef = ref}
                            initialRegion={INITIAL_REGION}>
                            {this.showMarkers(params)}
                        </MapView>
                    </View>
                )
            } else if (params.reportType === 'Geofencing') {
                return (
                    <View style={styles.container}>
                        <MapView
                            style={styles.map}
                            zoomEnabled={false}
                            rotateEnabled={false}
                            scrollEnabled={false}
                            pitchEnabled={false}
                            provider={PROVIDER_GOOGLE}
                            onMapReady={() => this.mapRef.fitToCoordinates(params.geoArr, { edgePadding: EDGE_PADDING })}
                            ref={(ref) => this.mapRef = ref}
                            initialRegion={INITIAL_REGION}>
                            <Geofences geofences={params.geofences} geoName={params.geofence.Name} />
                        </MapView>
                        <View style={{ padding: 10, paddingBottom: 20, borderTopWidth: 1, borderTopColor: '#abadac50' }}>
                            <Text style={{ color: params.geofence.Color, fontWeight: 'bold', fontSize: 15 }}>{params.geofence.Name.toUpperCase()}</Text>
                            <Text style={{ fontSize: 13, marginTop: 10, color: '#3d3f4c' }}>Address</Text>
                            <Text style={{ fontSize: 13, color: '#abadac' }}>{params.geofence.Address}</Text>
                            <Text style={{ fontSize: 13, marginTop: 10, color: '#3d3f4c' }}>Type</Text>
                            <Text style={{ fontSize: 13 , color: '#abadac'}}>{params.geofence.Type}</Text>
                            <Text style={{ fontSize: 13, marginTop: 10, color: '#3d3f4c' }}>Entry - Exit Time</Text>
                            <Text style={{ fontSize: 13 , color: '#abadac'}}>{moment(params.entryTime).format('DD/MM/YYYY')}</Text>
                            <Text style={{ fontSize: 13 , color: '#abadac'}}>{moment(params.entryTime).format('HH:mm:ss A')} - {moment(params.exitTime).format('HH:mm:ss A')}</Text>
                            <Text style={{ fontSize: 13, marginTop: 10, color: '#3d3f4c' }}>Duration</Text>
                            <Text style={{ fontSize: 13 , color: '#abadac'}}>{params.dur}</Text>
                            <Text style={{ fontSize: 13, marginTop: 10, color: '#3d3f4c' }}>Last Modified</Text>
                            <Text style={{ fontSize: 13 , color: '#abadac'}}>{moment(params.geofence.LastUpdate).format('DD/MM/YYYY HH:mm:ss A')}</Text>
                            <Text style={{ fontSize: 13 , color: '#abadac'}}>By: {params.geofence.User}</Text>
                        </View>
                    </View>
                )
            }
        }
    }
}