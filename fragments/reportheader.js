import React, { Component } from 'react';
import { Text, View, TouchableOpacity } from 'react-native';
import DatePicker from 'react-native-datepicker';
import SearchableDropdown from 'react-native-searchable-dropdown';

//styles
import { reportHeaderStyles, dropdownStyles, datePickerStyles } from '../assets/styles/reportheaderstyle'

//redux 
import { connect } from 'react-redux'

var moment = require('moment');
moment().format();

class ReportHeader extends Component {
    constructor(props) {
        super(props);
        this.state = {
            token: undefined,
            vehicle: 0,
            vehicleName: undefined,
            startDate: moment().format('M/D/YY') + ' 00:00 AM',
            endDate: moment().add(1, 'day')
        };
    }

    startDatePickerValChange = date => this.setState({ startDate: date })

    endDatePickerValChange = date => this.setState({ endDate: date })

    componentDidMount() {
        this.props.viewReport();
        this.getDetails(this.listVehicles()[0].value);
    }

    componentDidUpdate(prevState) {
        const { startDate, endDate } = this.state;
        if (prevState !== this.state && !!startDate && !!endDate) {
            this.props.viewReport();
            this.getDetails();
        }
    }

    getDetails(initVehicle) {
        let reportObject = {
            token: this.props.token,
            vehicle: !!initVehicle ? initVehicle : this.state.vehicle,
            start: '/Date(' + Date.parse(new Date(this.state.startDate)) + ')/',
            end: '/Date(' + Date.parse(new Date(this.state.endDate)) + ')/',
            vehicleName: this.state.vehicleName
        }
        this.props.passReportDetails(reportObject);
        console.log(reportObject)
    }

    listVehicles = () => {
        let list = [];
        this.props.vehicles.vehicles.map((vehicle, index) => {
            list.push({ label: vehicle.Name, value: vehicle.VehicleID, key: index })
        });
        return list;
    }

    render() {
        return (
            <View style={reportHeaderStyles.headerBody}>
                <View style={reportHeaderStyles.pickerGroupBody}>
                    <Text style={reportHeaderStyles.label}>Start</Text>
                    <TouchableOpacity style={reportHeaderStyles.dateBtn}>
                        <DatePicker
                            mode='date'
                            format='M/D/YY'
                            date={this.state.startDate}
                            maxDate={new Date(this.state.endDate)}
                            confirmBtnText='Confirm'
                            cancelBtnText='Cancel'
                            style={reportHeaderStyles.date}
                            customStyles={{ ...datePickerStyles }}
                            onDateChange={this.startDatePickerValChange}
                            showIcon={false}
                        />
                    </TouchableOpacity>
                    <Text style={reportHeaderStyles.label}>End</Text>
                    <TouchableOpacity style={reportHeaderStyles.dateBtn}>
                        <DatePicker
                            mode='date'
                            format='M/D/YY'
                            date={this.state.endDate}
                            minDate={this.state.startDate}
                            maxDate={new Date(moment().add(1, 'day'))}
                            confirmBtnText='Confirm'
                            cancelBtnText='Cancel'
                            style={reportHeaderStyles.datepickerStyle}
                            customStyles={{ ...datePickerStyles }}
                            onDateChange={this.endDatePickerValChange}
                            showIcon={false}
                        />
                    </TouchableOpacity>
                </View>
                <View style={reportHeaderStyles.vehiclePickerBody}>
                    <View style={dropdownStyles.viewContainer}>
                        <SearchableDropdown
                            defaultIndex={'0'}
                            onTextChange={text => { }}
                            onItemSelect={item => this.setState({ vehicle: item.value, vehicleName: item.label })}
                            containerStyle={dropdownStyles.container}
                            textInputStyle={dropdownStyles.textInput}
                            itemStyle={dropdownStyles.item}
                            itemTextStyle={dropdownStyles.itemText}
                            itemsContainerStyle={dropdownStyles.itemsContainer}
                            placeholder='Select Vehicle'
                            items={this.listVehicles()}
                            resetValue={false}
                            underlineColorAndroid='transparent'
                        />
                    </View>
                </View>
            </View>
        )
    }
}

function mapStateToProps(state) {
    return {
        vehicles: state.vehicles,
        token: state.token
    }
}

const mapDispatchToProps = {

};

export default connect(mapStateToProps, mapDispatchToProps)(ReportHeader)