import React from 'react'
import { View, Text } from 'react-native'
import Slider from '@react-native-community/slider';
import MapView, { Polyline, Marker, PROVIDER_GOOGLE, Callout } from 'react-native-maps'
import { TouchableOpacity } from 'react-native-gesture-handler'
import { Icon } from 'react-native-elements'

import LiveViewStyle from '../assets/styles/liveviewstyle'
import { styles } from '../assets/styles/maptracksstyle'

const INITIAL_REGION = {
    latitude: 1.290270,
    longitude: 103.851959,
    latitudeDelta: 0.3000,
    longitudeDelta: 0.3000
}

var moment = require('moment');
moment().format();

class MapTracks extends React.Component {
    constructor(props) {
        super(props);
        this.lineColor = '#000000';
        this.iconType = 'material-community',
            this.state = {
                markerIndex: 0,
                isPlaying: false,
                numTracks: props.latLng.length - 1,
                markerLat: props.latLng[0].latitude,
                markerLng: props.latLng[0].longitude,
                markerLatDelta: 0.300,
                markerLngDelta: 0.300,
            }
    }

    mapReady = () => this.mapRef.fitToCoordinates(this.props.latLng);

    play = () => {
        let status = !this.state.isPlaying;
        if (this.state.markerIndex >= this.state.numTracks) {
            status = false;
        }
        if (status) {
            this.interval = setInterval(() => {
                let nextPos = this.state.markerIndex + 1;
                if (nextPos > this.state.numTracks) {
                    clearInterval(this.interval);
                    this.setState({
                        isPlaying: false,
                    });
                } else {
                    let newLat = this.props.latLng[nextPos].latitude;
                    let newLng = this.props.latLng[nextPos].longitude;
                    this.setState({
                        markerIndex: nextPos,
                        markerLat: newLat,
                        markerLng: newLng,
                        markerLatDelta: 0.0300,
                        markerLngDelta: 0.0300,
                    });
                    // console.log('playing!');
                    if (this.marker !== null) {
                        this.marker.showCallout();
                    }
                    // console.log(this.state.markerIndex);
                }
            }, 1000);
        } else {
            clearInterval(this.interval);
        }

        this.setState({ isPlaying: status })
    }

    pressPrevious = () => {
        if (this.state.markerIndex > 0) {
            let newIndex = this.state.markerIndex - 1;
            let newLat = this.props.latLng[newIndex].latitude;
            let newLng = this.props.latLng[newIndex].longitude;
            this.setState({
                markerIndex: newIndex,
                markerLat: newLat,
                markerLng: newLng,
                markerLatDelta: 0.0300,
                markerLngDelta: 0.0300,
            });
        }
    }

    pressNext = () => {
        if (this.state.markerIndex < this.state.numTracks - 1) {
            let newIndex = this.state.markerIndex + 1;
            let newLat = this.props.latLng[newIndex].latitude;
            let newLng = this.props.latLng[newIndex].longitude;
            this.setState({
                markerIndex: newIndex,
                markerLat: newLat,
                markerLng: newLng,
            });
        }
    }

    render() {
        let location = !!this.props.latLng[this.state.markerIndex] ? this.props.latLng[this.state.markerIndex].loc : 'No recorded location'
        let timestamp = moment(new Date(parseInt(this.props.latLng[this.state.markerIndex].ts.substring(6, 19)))).format('DD/MM/YYYY hh:mm:ss A')

        return (
            <View style={styles.container}>
                <MapView
                    style={LiveViewStyle.map}
                    provider={PROVIDER_GOOGLE}
                    loadingEnabled
                    onMapReady={this.mapReady}
                    ref={(ref) => this.mapRef = ref}
                    initialRegion={INITIAL_REGION}>
                    <Marker
                        ref={ref => this.marker = ref}
                        title={location}
                        description={timestamp}
                        coordinate={!!this.props.latLng && this.props.latLng[this.state.markerIndex]}>
                        <Icon
                            size={25}
                            iconStyle={styles.icon}
                            name={'map-marker'}
                            type={this.iconType} />
                    </Marker>
                    <Polyline
                        key='track-1'
                        coordinates={this.props.latLng}
                        strokeColor={this.lineColor}
                        fillColor={this.lineColor}
                        strokeWidth={3} />
                </MapView>
                <View style={styles.sliderContainer}>
                    <Slider
                        style={styles.slider}
                        value={this.state.markerIndex}
                        minimumValue={0}
                        maximumValue={this.state.numTracks}
                        minimumTrackTintColor='#999999'
                        maximumTrackTintColor='#99999950'
                        thumbTintColor='#000000'
                        onValueChange={value => {
                            value = Math.ceil(value);

                            let newLat = this.props.latLng[value].latitude;
                            let newLng = this.props.latLng[value].longitude;

                            if (this.marker !== null) this.marker.showCallout();

                            this.setState({
                                markerIndex: value,
                                markerLat: newLat,
                                markerLng: newLng,
                                markerLatDelta: 0.0300,
                                markerLngDelta: 0.0300,
                            });
                        }
                        }
                    />
                    <View style={styles.buttonsContainer}>
                        <TouchableOpacity onPress={this.pressPrevious}>
                            <Icon name='skip-previous' type='material-community' size={35} iconStyle={styles.prev} />
                        </TouchableOpacity>
                        <TouchableOpacity onPress={this.play}>
                            <Icon name={this.state.isPlaying ? 'pause' : 'play'} type='material-community' size={40} iconStyle={styles.pause} />
                        </TouchableOpacity>
                        <TouchableOpacity onPress={this.pressNext}>
                            <Icon name='skip-next' type='material-community' size={35} iconStyle={styles.next} />
                        </TouchableOpacity>
                    </View>
                </View>
            </View>
        )
    }
}

export default MapTracks;