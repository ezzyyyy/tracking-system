import React from 'react';
import { View, Text, ScrollView, TouchableOpacity, Alert, Platform, Dimensions, SafeAreaView } from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import { Icon } from 'react-native-elements';
import Intercom from 'react-native-intercom';
import Firebase from 'react-native-firebase';

//style
import { styles } from '../assets/styles/navdrawerstyle'

//redux 
import { connect } from 'react-redux'
import { EmptyProps, UpdateVehicle, ToggleUpdateVehicle, StartLocation, StopLocation, UpdateUserData } from '../redux/actions/pages';

//asyncstorage
import { GetCompanyID } from '../api/asyncstorage';

class MainDrawerNavigation extends React.Component {
    constructor(props) {
        super(props);
        this.bg = { backgroundColor: '#f2f2f2' };
        this.iconType = 'material-community';
        this.state = {
            user: [],
            companies: [],
            selected: undefined,
            headerIPhoneXorAbove: false
        }
    }

    navigateToScreen = (props, route) => {
        //enable/disable vehicle api
        if (route == 'Vehicles' || route == 'LiveView') {
            props.ToggleUpdateVehicle(true);
        } else {
            props.ToggleUpdateVehicle(false);
        }
        props.navigation.navigate(route);
    }

    componentDidMount() {
        this.props.UpdateUserData();
        GetCompanyID().then((id) => this.setState({ selected: id }))

        let d = Dimensions.get('window');
        const { height, width } = d;

        if (Platform.OS === 'ios' && ((height === 812 || width === 812) || (height === 896 || width === 896))) {
            this.setState({ iphoneXorAbove: true });
        }
    }

    renderUserData = () => {
        if (this.props.userData) {
            return (
                <View>
                    <Text style={styles.navwelcome}>Welcome, {this.props.userData.user.Name}!</Text>
                    <Text style={styles.navemail}>{this.props.userData.user.Email}</Text>
                </View>
            )
        }
    }

    onValueChange(value) {
        AsyncStorage.setItem('COMPANY_ID', JSON.stringify(value));
        this.setState({ selected: value });
    }

    listCompany = () => {
        let comp = [];
        if (this.props.userData && this.props.userData.companies) {
            this.props.userData.companies.map((element, index) => {
                comp.push({ label: element.company, value: index, key: index });
            });
        }
        return comp;
    }

    render() {
        return (
            <SafeAreaView style={styles.container}>
                <View style={{ backgroundColor: 'rgba(50, 154, 152, 0.7)' }}>
                    <View style={styles.navuserinfo}>
                        {this.renderUserData()}
                    </View>
                    <View>
                        <TouchableOpacity style={{
                            justifyContent: 'center',
                            alignItems: 'flex-end',
                            margin: 20
                        }} onPress={() => this.navigateToScreen(this.props, 'Alerts')}>
                            <Icon type='material-community' iconStyle={{ color: '#FFFFFF' }} name='bell' />
                        </TouchableOpacity>
                    </View>
                </View>
                <ScrollView>
                    <View style={styles.navgroup}>
                        <View style={styles.navmenu}>
                            <View style={[styles.menuitem, ((this.props.activeItemKey == 'LiveTabs') ? this.bg : {})]}>
                                <TouchableOpacity style={styles.touchButton} onPress={() => this.navigateToScreen(this.props, 'LiveTabs')}>
                                    <Icon type='font-awesome' iconStyle={styles.menuitemicon} name='home' />
                                    <Text style={styles.menuitemtext}>Home</Text>
                                </TouchableOpacity>
                            </View>
                            <View style={[styles.menuitem, ((this.props.activeItemKey == 'Tracks') ? this.bg : {})]}>
                                <TouchableOpacity style={styles.touchButton} onPress={() => this.navigateToScreen(this.props, 'Tracks')}>
                                    <Icon type='font-awesome' iconStyle={styles.menuitemicon} name='location-arrow' />
                                    <Text style={styles.menuitemtext}>Draw Tracks</Text>
                                </TouchableOpacity>
                            </View>
                            <View style={[styles.menuitem, ((this.props.activeItemKey == 'Idling') ? this.bg : {})]}>
                                <TouchableOpacity style={styles.touchButton} onPress={() => this.navigateToScreen(this.props, 'Idling')}>
                                    <Icon type={this.iconType} iconStyle={styles.menuitemiconreport} name='clock-alert' />
                                    <Text style={styles.menuitemtext}>Idling</Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                        <View style={styles.navmenu}>

                            <View style={[styles.menuitem, ((this.props.activeItemKey == 'Speeding') ? this.bg : {})]}>
                                <TouchableOpacity style={styles.touchButton} onPress={() => this.navigateToScreen(this.props, 'Speeding')}>
                                    <Icon type={this.iconType} iconStyle={styles.menuitemiconreport} name='speedometer' />
                                    <Text style={styles.menuitemtext}>Speeding</Text>
                                </TouchableOpacity>
                            </View>
                            <View style={[styles.menuitem, ((this.props.activeItemKey == 'StartStop') ? this.bg : {})]}>
                                <TouchableOpacity style={styles.touchButton} onPress={() => this.navigateToScreen(this.props, 'StartStop')}>
                                    <Icon type={this.iconType} iconStyle={styles.menuitemiconreport} name='play-pause' />
                                    <Text style={styles.menuitemtext}>Start-stop</Text>
                                </TouchableOpacity>
                            </View>
                            <View style={[styles.menuitem, ((this.props.activeItemKey == 'Geofencing') ? this.bg : {})]}>
                                <TouchableOpacity style={styles.touchButton} onPress={() => this.navigateToScreen(this.props, 'Geofencing')}>
                                    <Icon type={this.iconType} iconStyle={styles.menuitemiconreport} name='map-marker-radius' />
                                    <Text style={styles.menuitemtext}>Geofencing</Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                        <View style={styles.navmenu}>
                            <View style={styles.menuitem}>
                                <TouchableOpacity style={styles.touchButton} onPress={() => Intercom.displayMessageComposer()}>
                                    <Icon type={this.iconType} iconStyle={styles.menuitemicon} name='forum' />
                                    <Text style={styles.menuitemtext}>Support</Text>
                                </TouchableOpacity>
                            </View>
                            <View style={styles.menuitem}>
                                <TouchableOpacity style={styles.touchButton} onPress={() => {
                                    Alert.alert(
                                        'Logout',
                                        'Are you sure?',
                                        [
                                            { text: 'Cancel', onPress: () => { } },
                                            {
                                                text: 'Yes', onPress: async () => {
                                                    this.props.navigation.navigate('Auth');
                                                    Intercom.logout();
                                                    AsyncStorage.clear();
                                                    this.props.EmptyProps();

                                                    Firebase.messaging().getToken()
                                                    .then(t => Firebase.messaging().deleteToken(t))
                                                    .then(r => console.log(r))
                                                    .catch(e => console.log(e));
                                                }
                                            }
                                        ])
                                }}>
                                    <Icon type={this.iconType} iconStyle={styles.menuitemicon} name='exit-to-app' />
                                    <Text style={styles.menuitemtext}>Logout</Text>
                                </TouchableOpacity>
                            </View>
                            <View style={[styles.menuitem, ((this.props.activeItemKey == 'Reports') ? this.bg : {})]}>
                            </View>
                        </View>
                    </View>
                </ScrollView>
            </SafeAreaView>
        );
    }
}

function mapStateToProps(state) {
    return {
        vehicles: state.vehicles,
        userData: state.userData
    }
}

const mapDispatchToProps = {
    EmptyProps,
    UpdateVehicle,
    ToggleUpdateVehicle,
    StartLocation,
    StopLocation,
    UpdateUserData
};

export default connect(mapStateToProps, mapDispatchToProps)(MainDrawerNavigation)