import React, { Component } from 'react';
import { View, Text, ActivityIndicator } from 'react-native';

//components
import ReportHeader from '../fragments/reportheader';
import ResultList from '../fragments/resultlist';
//style
import { styles } from '../assets/styles/alertsstyle';

//redux 
import { connect } from 'react-redux'

//API
import { GetMessageReport } from '../api/reportapi';

//asyncstorage
import { GetCompanyID } from '../api/asyncstorage';

const messageReport = {
    type: 'message',
    note: 'To show Message report, please select a start time, end time and a vehicle.'
}

var moment = require('moment');
moment().format();

class AlertsPage extends Component {
    constructor(props) {
        super(props);
        this.state = {
            messageTrackPoints: [],
            trackLength: undefined,
            loading: false,
            report: []
        }
    }

    viewMessageReport = async (token, vehicle, start, end) => {
        this.setState({ messageTrackPoints: [], trackLength: undefined, loading: true });
        let trackPoints = [];
        let count = 300;
        let page = 0;

        let company = await GetCompanyID();

        token = this.state.report.token;
        start = this.state.report.start;
        vehicle = this.state.report.vehicle;
        end = this.state.report.end;

        console.log(this.state)

        try {
            while (count === 300) {
                let result = await GetMessageReport(token, vehicle, start, end, page, company);
                result = result.GetMessageReportResult;
                count = result.length;

                if (result.length === 0) {
                    alert('There are no alerts during this period.');
                } else {
                    page = result[result.length - 1].MessageID;
                    result.forEach((point) => {
                        trackPoints.push(point)
                        return true
                    });
                    trackPoints.sort(function (a, b) {
                        return trackPoints[trackPoints.length - 1].MessageID - trackPoints[0].MessageID;
                    });
                    this.setState({ trackLength: trackPoints.length });
                }
            }
            this.setState({ messageTrackPoints: trackPoints, loading: false });
        } catch (error) {
            throw new Error(400);
        }
    }

    getDetails = (data) => { this.setState({ report: data }); }

    render() {
        return (
            <View style={styles.container}>
                <ReportHeader
                    viewReport={this.viewMessageReport}
                    passReportDetails={this.getDetails}
                />
                <View style={styles.reportList}>
                    {(this.state.messageTrackPoints.length !== 0) ? (
                        <ResultList
                            resultType={messageReport.type}
                            dataArray={this.state.messageTrackPoints}
                        />
                    ) : (
                            <View style={styles.note}>
                                <Text style={styles.noteText}>
                                    {messageReport.note}
                                </Text>
                            </View>
                        )}
                    {(!!this.state.loading) && (
                        <View style={styles.loadingView}>
                            <View style={styles.spinnerView}>
                                <ActivityIndicator size='large' style={styles.spinner} color='rgb(50, 154, 152)' />
                                <Text style={styles.spinnerText}>
                                    {this.state.trackLength ?
                                        ('Loading (' + this.state.trackLength + ')') :
                                        ('Loading...')
                                    }
                                </Text>
                            </View>
                        </View>
                    )}
                </View>
            </View>
        )
    }
}


function mapStateToProps(state) {
    return {
    }
}

const mapDispatchToProps = {
    
};

export default connect(mapStateToProps, mapDispatchToProps)(AlertsPage)