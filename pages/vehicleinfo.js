
import React from 'react'
import { View, ScrollView, Text, TouchableOpacity, Alert, Linking } from 'react-native'
import { Icon, Tooltip } from 'react-native-elements'
import { AnimatedCircularProgress } from 'react-native-circular-progress'
import Dialog from 'react-native-dialog'

//redux
import { connect } from 'react-redux'
import { UpdateUsers, LoadMarker } from '../redux/actions/pages'

//api
import { ImmobilizeVehicle } from '../api/vehicleapi'
import { SaveUser } from '../api/authenticationapi'

//styles
import { styles } from '../assets/styles/vehicleinfostyle'

var moment = require('moment')
moment().format()

class VehicleInfo extends React.Component {
    constructor(props) {
        super(props);
        this.props.UpdateUsers();
        this.state = {
            dialogVisible: false,
            contactNumber: '',
            isInvalid: false,
            switchTab: 1
        }
    }

    componentDidMount() {
        const { params } = this.props.navigation.state;
        let vID = params.vID;
        let dID = params.dID;
        let durPercent = params.durPercent;
        let durPercentPrev = params.durPercentPrev;
        let drivingObj = params.drivingObj;
        let drivingObjPrev = params.drivingObjPrev;

        if (!!dID) {
            this.setState({ dID, drivingObj, drivingObjPrev })
        }

        this.setState({ vID, durPercent, durPercentPrev })
    }

    immobilize = (vehicle) => {
        Alert.alert(
            ((vehicle.Immobilizer == 1) ? 'Disable' : 'Enable') + ' Immobilizer - ' + vehicle.Name,
            'Please double check the vehicle before proceeding.',
            [{
                text: 'Proceed', onPress: () => {
                    ImmobilizeVehicle(this.props.token, vehicle.TrackerID,
                        ((vehicle.Immobilizer == undefined || vehicle.Immobilizer == 0) ? 1 : 0)).then((resp) => {
                            if (resp.OtaTrackersResult) {
                                Alert.alert(
                                    'Sucess!',
                                    'The immobilizer has been ' + ((vehicle.Immobilizer == 1) ? 'disabled.' : 'enabled. \n'
                                        + 'This change will be applied shortly.'));
                            }
                            else
                                Alert.alert('Error!', 'Something went wrong, please try again.')
                        }).catch((error) => {
                            Alert.alert('Error!', 'Something went wrong, please try again.')
                        })
                }
            }, { text: 'Cancel', style: 'cancel' }], { cancelable: false })
    }

    checkPhone(phone, btn, driverId) {
        if (!!driverId) {
            if (!!phone) {
                if (btn === 'whatsapp')
                    Linking.openURL(`https://wa.me/${phone}/`)
                else
                    Linking.openURL(`tel:${phone}`)
            } else
                this.setState({ dialogVisible: true })
        } else {
            Alert.alert(
                'Oops!',
                'Only when there is a driver assigned can contact number be added. ' +
                'Please assign a driver to this vehicle using VTS web portal.');
        }
    }

    isInvalid = () => this.setState({ dialogVisible: true })
    handleCancel = () => this.setState({ dialogVisible: false })
    onChangeText = input => this.setState({ contactNumber: input })

    async handleSave(user) {
        let userObj = user;
        if (this.state.contactNumber == '' || isNaN(this.state.contactNumber))
            this.setState({ isInvalid: true })
        else {
            this.setState({ isInvalid: false })
            userObj.Phone = this.state.contactNumber;
            let response = await SaveUser(this.props.token, userObj)
            result = response.SaveUserResult;
            if (result.Message === 'OK') {
                Alert.alert(
                    'Success!',
                    `${user.Name}'s phone number has been saved.`,
                    [{ text: 'OK', onPress: () => this.setState({ dialogVisible: false }) }]
                );
                this.props.UpdateUsers();
            } else {
                Alert.alert(
                    'Error!',
                    'There was a problem with the request. Please try again. ',
                    [{ text: 'OK', onPress: () => { } }]
                );
            }
        }
    }

    navigateToScreen = (props, route, vehicle) => {
        props.LoadMarker('marker' + vehicle.VehicleID);
        let lat = !!vehicle.LastFix.Latitude ? vehicle.LastFix.Latitude : 1.210574;
        let lng = !!vehicle.LastFix.Longitude ? vehicle.LastFix.Longitude : 103.574804;
        let region = {
            latitude: lat,
            longitude: lng,
            latitudeDelta: 0.0042,
            longitudeDelta: 0.0042,
        };
        props.map.animateToRegion(region, 1000);
        props.navigation.navigate(route);
    }

    goToMarker = (veh) => {
        if (!!veh.LastFix && !!veh.LastFix.Latitude && !!veh.LastFix.Longitude)
            this.navigateToScreen(this.props, 'LiveView', veh);
        else
            alert('This vehicle has no last recorded location. It cannot be shown on map.');
    }

    render() {
        let vID = this.state.vID;
        let token;
        let vehicles = [];
        let activeVeh;

        let name = '';
        let driverId;
        let phone;
        let category = 'No category';
        let driver = 'No driver assigned';
        let company = 'No company assigned';
        let lastloc = 'No last location';
        let timestamp = 'No last timestamp';
        let speed = '0';
        let ignitionColor = 'grey';
        let ignition = 'OFF';
        let engine = 'Stopped';
        let engColor = '#2980b9';
        let mileage = '0km';
        let selectedUser;
        let battery = 'No recorded value';
        let trackerBat = 'No recorded value';

        let immoStat = 'Disabled';

        let scoreObj;
        let harshAcceleration = 0;
        let harshBraking = 0;
        let harshCornering = 0;
        let excessiveIdling = 0;
        let dMileage = 0;
        let month = '-';
        let score = 0;
        let speeding = 0;

        //from Previous Month
        let scoreObjPrev;
        let harshAccelerationPrev = 0;
        let harshBrakingPrev = 0;
        let harshCorneringPrev = 0;
        let excessiveIdlingPrev = 0;
        let dMileagePrev = 0;
        let monthPrev = '-';
        let scorePrev = 0;
        let speedingPrev = 0;

        //fuel savings
        let eIdlSaved = 0;
        let dMlgSaved = 0;
        let speedSaved = 0;
        let hAccSaved = 0;
        let hBkgSaved = 0;
        let hCorSaved = 0;
        let total = 0;

        //industrial avg text
        let indDrivingScoreAvg;
        let indUtilizationAvg;

        let scoreArrow;


        if (this.props) {
            if (!!this.props.token) {
                token = this.props.token;

                if (!!this.props.vehicles) {
                    vehicles = this.props.vehicles.vehicles;
                    vehicles.map(v => {
                        if (v.VehicleID === vID) {

                            if (!!v.LastFix) {
                                if (moment().diff(moment(v.LastFix.Timestamp), 'days') == 0) {
                                    if (v.LastFix.Engine == 1) {
                                        engColor = 'orange';
                                        engine = 'Idle'
                                    }
                                    else if (v.LastFix.Engine == 2) {
                                        engColor = '#27ae60';
                                        engine = 'Moving';
                                    }
                                }
                                else {
                                    engColor = '#2980b9';
                                    engine = 'Stopped';
                                }
                            } else {
                                engColor = 'grey';
                                engine = 'Stopped';
                            }

                            activeVeh = v;
                            name = !!activeVeh.Name ? activeVeh.Name : name;
                            category = !!activeVeh.Category ? activeVeh.Category : category;
                            driver = !!activeVeh.Driver ? activeVeh.Driver : driver;
                            company = !!activeVeh.Group.Company ? activeVeh.Group.Company : company;
                            lastloc = !!activeVeh.LastFix && !!activeVeh.LastFix.Location ? activeVeh.LastFix.Location : lastloc;
                            timestamp = !!activeVeh.LastFix ? activeVeh.LastFix.Timestamp : timestamp;
                            speed = !!activeVeh.LastFix && !!activeVeh.LastFix.Speed ? activeVeh.LastFix.Speed : speed;
                            ignitionColor = !!activeVeh.LastFix && !!activeVeh.LastFix.Ignition ? activeVeh.LastFix.Ignition === 1 ? '#27ae60' : 'grey' : ignitionColor;
                            ignition = !!activeVeh.LastFix && !!activeVeh.LastFix.Ignition ? activeVeh.LastFix.Ignition === 1 ? 'ON' : 'OFF' : ignition;
                            mileage = !!activeVeh.Mileage ? activeVeh.Mileage : mileage;
                            speedLimit = !!activeVeh.SpeedLimit ? activeVeh.SpeedLimit : 200;
                            battery = !!activeVeh.LastFix.Adc3 ? activeVeh.LastFix.Adc3 + 'V' : battery;
                            trackerBat = !!activeVeh.LastFix.Battery ? activeVeh.LastFix.Battery + 'V' : battery;

                            driverId = activeVeh.DriverID;

                            if (activeVeh.Immobilizer === -1) {
                                immoStat = 'Not Installed';
                            } else if (activeVeh.Immobilizer === 1) {
                                immoStat = 'Enabled';
                            } else if (activeVeh.Immobilizer === undefined) {
                                immoStat = 'Disabled';
                            }
                        }
                    })
                }

                if (!!this.props.users) {
                    let { users } = this.props;

                    users.map((user) => {
                        if (user.UserID === driverId) {
                            phone = user.Phone;
                            selectedUser = user;
                        }
                    })
                }

                let drivingObj = this.state.drivingObj;
                let drivingObjPrev = this.state.drivingObjPrev;
                let dur = this.state.durPercent;
                let durPrev = this.state.durPercentPrev;

                console.log(dur, durPrev)

                if (!!drivingObj) {
                    scoreObj = drivingObj;

                    harshAcceleration = !!scoreObj.HarshAcceleration ? scoreObj.HarshAcceleration : 0;
                    harshBraking = !!scoreObj.HarshBraking ? scoreObj.HarshBraking : 0;
                    harshCornering = !!scoreObj.HarshCornering ? scoreObj.HarshCornering : 0;
                    excessiveIdling = !!scoreObj.ExcessiveIdling ? scoreObj.ExcessiveIdling : 0;
                    dMileage = !!scoreObj.Mileage ? scoreObj.Mileage : 0;

                    month = !!scoreObj.Month ? scoreObj.Month.replace('/Date(', '') : 0;
                    month = month.replace(')/', '');
                    month = moment.unix(month / 1000).format('MMMM YYYY');
                    month = !!scoreObj.Month ? month : 0;

                    score = !!scoreObj.Score ? scoreObj.Score : 0;
                    speeding = !!scoreObj.Speeding ? scoreObj.Speeding : 0;


                    // potential fuel savings
                    eIdlSaved = excessiveIdling * 2; // x $2
                    dMlgSaved = (dMileage * 0.1).toFixed(2); // x $0.10
                    speedSaved = speeding * 1; // x $1
                    hAccSaved = harshAcceleration;
                    hBkgSaved = harshBraking;
                    hCorSaved = harshCornering;
                    // total = parseFloat(hAccSaved + hBkgSaved + hCorSaved + eIdlSaved + dMlgSaved + speedSaved).toFixed(2);
                    total = hAccSaved + hBkgSaved + hCorSaved + eIdlSaved + parseFloat(dMlgSaved) + speedSaved
                    console.log(hAccSaved + hBkgSaved + hCorSaved + eIdlSaved + parseFloat(dMlgSaved) + speedSaved)

                    let belowScore = {
                        text: 'Below Industrial Average by: ' + (10 - score),
                        color: '#009000'
                    }
                    let eqScore = {
                        text: 'Score is the Industrial Average (10)',
                        color: 'orange'
                    }
                    let aboveScore = {
                        text: 'Above Industrial Average by: ' + (score - 10),
                        color: '#cc0000'
                    }

                    indDrivingScoreAvg = score < 10 ? belowScore :
                        score === 10 ? eqScore : aboveScore
                }

                if (!!drivingObjPrev) {
                    scoreObjPrev = drivingObjPrev;

                    harshAccelerationPrev = !!scoreObjPrev.HarshAcceleration ? scoreObjPrev.HarshAcceleration : 0;
                    harshBrakingPrev = !!scoreObjPrev.HarshBraking ? scoreObjPrev.HarshBraking : 0;
                    harshCorneringPrev = !!scoreObjPrev.HarshCornering ? scoreObjPrev.HarshCornering : 0;
                    excessiveIdlingPrev = !!scoreObjPrev.ExcessiveIdling ? scoreObjPrev.ExcessiveIdling : 0;
                    dMileagePrev = !!scoreObjPrev.Mileage ? scoreObjPrev.Mileage : 0;

                    monthPrev = !!scoreObjPrev.Month ? scoreObjPrev.Month.replace('/Date(', '') : 0;
                    monthPrev = monthPrev.replace(')/', '');
                    monthPrev = moment.unix(monthPrev / 1000).format('MMMM YYYY');
                    monthPrev = !!scoreObjPrev.Month ? monthPrev : 0;

                    scorePrev = !!scoreObjPrev.Score ? scoreObjPrev.Score : 0;
                    speedingPrev = !!scoreObjPrev.Speeding ? scoreObjPrev.Speeding : 0;
                }

                let up = { icon: 'arrow-up-bold', color: '#cc0000' };
                let eq = { icon: 'equal', color: 'orange' };
                let down = { icon: 'arrow-down-bold', color: '#009900' };

                if (!!drivingObj && !!drivingObjPrev) {
                    let numDays = moment().diff(moment().startOf('month'), 'days') + 1;

                    scoreArrow = ((score / 30) * numDays > scorePrev) ?
                        up : ((score / 30) * numDays === scorePrev) ?
                            eq : down;

                    hAccArrow = ((harshAcceleration / 30) * numDays > harshAccelerationPrev) ?
                        up : ((harshAcceleration / 30) * numDays === harshAccelerationPrev) ?
                            eq : down;

                    hBkgArrow = ((harshBraking / 30) * numDays > harshBrakingPrev) ?
                        up : ((harshBraking / 30) * numDays === harshBrakingPrev) ?
                            eq : down;

                    hCorArrow = ((harshCornering / 30) * numDays > harshCorneringPrev) ?
                        up : ((harshCornering / 30) * numDays === harshCorneringPrev) ?
                            eq : down;

                    eIdlArrow = ((excessiveIdling / 30) * numDays > excessiveIdlingPrev) ?
                        up : ((excessiveIdling / 30) * numDays === excessiveIdlingPrev) ?
                            eq : down;

                    dMlgArrow = ((dMileage / 30) * numDays > dMileagePrev) ?
                        up : ((dMileage / 30) * numDays === dMileagePrev) ?
                            eq : down;

                    speedArrow = ((speeding / 30) * numDays > speedingPrev) ?
                        up : ((speeding / 30) * numDays === speedingPrev) ?
                            eq : down;
                }

                utilArrow = (dur > durPrev) ? up : (dur === durPrev) ? eq : down;

                let belowUtil = {
                    text: 'Below Industrial Average by: ' + (30 - dur) + '%',
                    color: '#009000'
                }
                let eqUtil = {
                    text: 'Percentage is the Industrial Average (30)',
                    color: 'orange'
                }
                let aboveUtil = {
                    text: 'Above Industrial Average by: ' + (dur - 30) + '%',
                    color: '#cc0000'
                }

                indUtilizationAvg = dur < 30 ? belowUtil :
                    dur === 30 ? eqUtil : aboveUtil
            }
        }

        return (
            <View style={styles.container}>
                <TouchableOpacity
                    onPress={() => this.setState({ switchTab: !this.state.switchTab })}
                    style={styles.switchTab}>
                    <Icon
                        name={!!this.state.switchTab ? 'chevron-right' : 'chevron-left'}
                        type='material-community'
                        size={20}
                        iconStyle={styles.switchTabIcon}
                    />
                </TouchableOpacity>
                <Dialog.Container visible={this.state.dialogVisible}>
                    <Dialog.Title style={styles.dialogTitle}>Enter contact number</Dialog.Title>
                    <Dialog.Description style={styles.dialogDesc}>This driver does not have a saved phone number. Enter contact number below.</Dialog.Description>
                    <Dialog.Input
                        placeholder='e.g. 12345678'
                        underlineColorAndroid='#abadac50'
                        style={styles.dialogInput}
                        keyboardType={'phone-pad'}
                        onChangeText={this.onChangeText} />
                    {this.state.isInvalid === true &&
                        <Dialog.Description style={styles.dialogInvalidDesc}>Invalid number. Please try again.</Dialog.Description>}
                    <Dialog.Button label='LATER' onPress={this.handleCancel} />
                    <Dialog.Button label='SAVE' onPress={() => this.handleSave(selectedUser)} />
                </Dialog.Container>
                <ScrollView contentContainerStyle={styles.contentContainer} style={styles.scrollView}>
                    {!!this.state.switchTab ? (
                        <View>
                            <TouchableOpacity onPress={() => this.goToMarker(activeVeh)}>
                                <Text style={styles.vName}>{name}</Text>
                            </TouchableOpacity>
                            <Text style={styles.vTitle}>Category</Text>
                            <Text style={styles.vSubtitle}>{category}</Text>
                            <Text style={styles.vTitle}>Driver</Text>
                            <Text style={styles.vSubtitle}>{driver}</Text>
                            {!!phone && <Text style={styles.vSubtitle}>{phone}</Text>}
                            <Text style={styles.vTitle}>Company</Text>
                            <Text style={styles.vSubtitle}>{company}</Text>
                            <Text style={styles.vTitle}>Last location</Text>
                            <Text style={styles.vSubtitle}>{lastloc}</Text>
                            <Text style={styles.vTitle}>Last seen</Text>
                            <Text style={styles.vSubtitle}>{moment.unix(timestamp.substring(6, 19) / 1000).format('DD/MM/YYYY hh:mm:ss A')}</Text>
                            <Text style={styles.vTitle}>Ignition</Text>
                            <View style={styles.rowContainer}>
                                <Text style={{ fontSize: 13, color: ignitionColor }}>{ignition}</Text>
                                <Icon
                                    name='record'
                                    size={20}
                                    type='material-community'
                                    iconStyle={{
                                        color: ignitionColor,
                                        alignSelf: 'flex-start'
                                    }}
                                />
                            </View>
                            <Text style={styles.vTitle}>Engine Status</Text>
                            <View style={styles.rowContainer}>
                                <Text style={{ fontSize: 13, color: engColor }}>{engine}</Text>
                                <Icon
                                    name='record'
                                    size={20}
                                    type='material-community'
                                    iconStyle={{ color: engColor, alignSelf: 'flex-start' }}
                                />
                            </View>
                            <Text style={styles.vTitle}>Speed</Text>
                            <Text style={{ fontSize: 13, color: engColor }}>{speed + 'km/h'}</Text>
                            <Text style={styles.vTitle}>Mileage</Text>
                            <Text style={styles.vSubtitle}>{mileage + 'km'}</Text>
                            <Text style={styles.vTitle}>Vehicle Battery</Text>
                            <Text style={styles.vSubtitle}>{battery}</Text>
                            <Text style={styles.vTitle}>Tracker Battery</Text>
                            <Text style={styles.vSubtitle}>{trackerBat}</Text>
                            <View style={styles.rowContainer}>
                                <Text style={styles.vTitle}>Total Monthly Fuel Savings</Text>
                                <Tooltip
                                    width={250}
                                    backgroundColor='#329a98'
                                    popover={<Text style={styles.popoverText}>Go to the next page to learn more!</Text>}>
                                    <Icon
                                        name='information'
                                        size={17}
                                        type='material-community'
                                        iconStyle={styles.infoIcon}
                                    />
                                </Tooltip>
                            </View>
                            <Text style={styles.vSubtitle}>${total}</Text>
                        </View>
                    ) : (
                            <View>
                                <TouchableOpacity onPress={() => this.goToMarker(activeVeh)}>
                                    <Text style={styles.vName}>{name}</Text>
                                </TouchableOpacity>
                                <Text style={styles.vSubtitle}>{month}</Text>
                                <View style={styles.circlesContainer}>
                                    <View style={styles.circleContainer}>
                                        <View style={styles.rowContainer}>
                                            <AnimatedCircularProgress
                                                size={70}
                                                width={10}
                                                fill={score}
                                                tintColor='rgb(242,158,113)'
                                                backgroundColor='rgb(247,214,198)'
                                                lineCap='round'
                                                rotation={0}
                                                duration={1000}
                                                style={styles.scoreCircle}>
                                                {
                                                    (fill) => (
                                                        <Text style={styles.scoreCircleText}>
                                                            {score}
                                                        </Text>
                                                    )
                                                }
                                            </AnimatedCircularProgress>
                                            {
                                                !!scoreArrow &&
                                                <Icon
                                                    name={scoreArrow.icon}
                                                    size={20}
                                                    type='material-community'
                                                    iconStyle={[styles.arrowIcon, { color: scoreArrow.color }]} />
                                            }

                                        </View>
                                        <View style={styles.rowContainer}>
                                            <Text style={styles.scoreTitle}>Driving Score</Text>
                                            {
                                                !!indDrivingScoreAvg &&
                                                <Tooltip
                                                    width={250}
                                                    backgroundColor={indDrivingScoreAvg.color}
                                                    withOverlay={false}
                                                    popover={<Text style={styles.popoverText}>{indDrivingScoreAvg.text}</Text>}>
                                                    <Icon
                                                        name='information'
                                                        size={17}
                                                        type='material-community'
                                                        iconStyle={styles.tooltipIcon}
                                                    />
                                                </Tooltip>
                                            }

                                        </View>
                                    </View>
                                    <View style={styles.circleContainer}>
                                        <View style={styles.rowContainer}>
                                            <AnimatedCircularProgress
                                                size={70}
                                                width={10}
                                                fill={this.state.durPercent}
                                                tintColor='#2dbebb'
                                                backgroundColor='#2dbebb50'
                                                lineCap='round'
                                                rotation={0}
                                                duration={1000}
                                                style={styles.utilCircle}>
                                                {
                                                    (fill) => (
                                                        <Text style={styles.utilCircleText}>
                                                            {Number.isNaN(this.state.durPercent) ? 0 : this.state.durPercent}
                                                        </Text>
                                                    )
                                                }
                                            </AnimatedCircularProgress>
                                            {
                                                !!utilArrow &&
                                                <View style={styles.rowContainer}>
                                                    <Icon
                                                        name={utilArrow.icon}
                                                        size={20}
                                                        type='material-community'
                                                        iconStyle={[styles.arrowIcon, { color: utilArrow.color }]} />
                                                </View>
                                            }
                                        </View>
                                        <View style={styles.rowContainer}>
                                            <Text style={styles.utilTitle}>Utilization %</Text>
                                            <Tooltip
                                                width={250}
                                                backgroundColor={indUtilizationAvg.color}
                                                withOverlay={false}
                                                popover={<Text style={styles.popoverText}>{indUtilizationAvg.text}</Text>}>
                                                <Icon
                                                    name='information'
                                                    size={17}
                                                    type='material-community'
                                                    iconStyle={styles.tooltipIcon}
                                                />
                                            </Tooltip>
                                        </View>
                                    </View>
                                </View>
                                {
                                    !!this.state.drivingObj && (
                                        <View style={styles.scoreDetailsContainer}>
                                            <View style={styles.scoreRow}>
                                                <View style={styles.rowContainer}>
                                                    <View style={styles.scoreItemContainer}>
                                                        <View style={styles.rowContainer}>
                                                            <Text style={styles.scoreItem}>{harshAcceleration}</Text>
                                                            <Icon
                                                                name={hAccArrow.icon}
                                                                size={20}
                                                                type='material-community'
                                                                iconStyle={styles.arrowIcon, { color: hAccArrow.color }}
                                                            />
                                                        </View>
                                                        <View style={styles.rowContainer}>
                                                            <View style={styles.colContainer}>
                                                                <Text style={styles.scoreItemTitle}>Harsh</Text>
                                                                <Text style={styles.scoreItemTitle}>Acceleration</Text>
                                                            </View>
                                                            <Tooltip
                                                                width={200}
                                                                backgroundColor={'#202020'}
                                                                withOverlay={false}
                                                                popover={<Text style={styles.popoverText}>${hAccSaved} Potential Fuel Savings</Text>}>
                                                                <Icon
                                                                    name='information'
                                                                    size={17}
                                                                    type='material-community'
                                                                    iconStyle={styles.tooltipIcon}
                                                                />
                                                            </Tooltip>
                                                        </View>
                                                    </View>
                                                    <View style={styles.scoreItemContainer}>
                                                        <View style={styles.rowContainer}>
                                                            <Text style={styles.scoreItem}>{harshBraking}</Text>
                                                            <Icon
                                                                name={hBkgArrow.icon}
                                                                size={20}
                                                                type='material-community'
                                                                iconStyle={styles.arrowIcon, { color: hBkgArrow.color }}
                                                            />
                                                        </View>
                                                        <View style={styles.rowContainer}>
                                                            <View style={styles.colContainer}>
                                                                <Text style={styles.scoreItemTitle}>Harsh</Text>
                                                                <Text style={styles.scoreItemTitle}>Braking</Text>
                                                            </View>
                                                            <Tooltip
                                                                width={200}
                                                                backgroundColor={'#202020'}
                                                                withOverlay={false}
                                                                popover={<Text style={styles.popoverText}>${hBkgSaved} Potential Fuel Savings</Text>}>
                                                                <Icon
                                                                    name='information'
                                                                    size={17}
                                                                    type='material-community'
                                                                    iconStyle={styles.tooltipIcon}
                                                                />
                                                            </Tooltip>
                                                        </View>
                                                    </View>
                                                    <View style={styles.scoreItemContainer}>
                                                        <View style={styles.rowContainer}>
                                                            <Text style={styles.scoreItem}>{harshCornering}</Text>
                                                            <Icon
                                                                name={hCorArrow.icon}
                                                                size={20}
                                                                type='material-community'
                                                                iconStyle={styles.arrowIcon, { color: hCorArrow.color }}
                                                            />
                                                        </View>
                                                        <View style={styles.rowContainer}>
                                                            <View style={styles.colContainer}>
                                                                <Text style={styles.scoreItemTitle}>Harsh</Text>
                                                                <Text style={styles.scoreItemTitle}>Cornering</Text>
                                                            </View>
                                                            <Tooltip
                                                                width={200}
                                                                backgroundColor={'#202020'}
                                                                withOverlay={false}
                                                                popover={<Text style={styles.popoverText}>${hCorSaved} Potential Fuel Savings</Text>}>
                                                                <Icon
                                                                    name='information'
                                                                    size={17}
                                                                    type='material-community'
                                                                    iconStyle={styles.tooltipIcon}
                                                                />
                                                            </Tooltip>
                                                        </View>
                                                    </View>
                                                </View>
                                            </View>
                                            <View style={styles.scoreRow}>
                                                <View style={styles.rowContainer}>
                                                    <View style={styles.scoreItemContainer}>
                                                        <View style={styles.rowContainer}>
                                                            <Text style={styles.scoreItem}>{excessiveIdling}</Text>
                                                            <Icon
                                                                name={eIdlArrow.icon}
                                                                size={20}
                                                                type='material-community'
                                                                iconStyle={styles.arrowIcon, { color: eIdlArrow.color }}
                                                            />
                                                        </View>
                                                        <View style={styles.rowContainer}>
                                                            <View style={styles.colContainer}>
                                                                <Text style={styles.scoreItemTitle}>Excessive</Text>
                                                                <Text style={styles.scoreItemTitle}>Idling</Text>
                                                            </View>
                                                            <Tooltip
                                                                width={200}
                                                                backgroundColor={'#202020'}
                                                                withOverlay={false}
                                                                popover={<Text style={styles.popoverText}>${eIdlSaved} Potential Fuel Savings</Text>}>
                                                                <Icon
                                                                    name='information'
                                                                    size={17}
                                                                    type='material-community'
                                                                    iconStyle={styles.tooltipIcon}
                                                                />
                                                            </Tooltip>
                                                        </View>
                                                    </View>
                                                    <View style={styles.scoreItemContainer}>
                                                        <View style={styles.rowContainer}>
                                                            <Text style={styles.scoreItem}>{dMileage}</Text>
                                                            <Icon
                                                                name={dMlgArrow.icon}
                                                                size={20}
                                                                type='material-community'
                                                                iconStyle={styles.arrowIcon, { color: dMlgArrow.color }}
                                                            />
                                                        </View>
                                                        <View style={styles.rowContainer}>
                                                            <View style={styles.colContainer}>
                                                                <Text style={styles.scoreItemTitle}>Mileage</Text>
                                                                <Text style={styles.scoreItemTitle}>(KM)</Text>
                                                            </View>
                                                            <Tooltip
                                                                width={250}
                                                                backgroundColor={'#202020'}
                                                                withOverlay={false}
                                                                popover={<Text style={styles.popoverText}>${dMlgSaved} Potential Fuel Savings</Text>}>
                                                                <Icon
                                                                    name='information'
                                                                    size={17}
                                                                    type='material-community'
                                                                    iconStyle={styles.tooltipIcon}
                                                                />
                                                            </Tooltip>
                                                        </View>
                                                    </View>
                                                    <View style={styles.scoreItemContainer}>
                                                        <View style={styles.rowContainer}>
                                                            <Text style={styles.scoreItem}>{speeding}</Text>
                                                            <Icon
                                                                name={speedArrow.icon}
                                                                size={20}
                                                                type='material-community'
                                                                iconStyle={styles.arrowIcon, { color: speedArrow.color }}
                                                            />
                                                        </View>
                                                        <View style={styles.rowContainer}>
                                                            <View style={styles.colContainer}>
                                                                <Text style={styles.scoreItemTitle}>Speeding</Text>
                                                                <Text style={styles.scoreItemTitle}>(MIN)</Text>
                                                            </View>
                                                            <Tooltip
                                                                width={200}
                                                                backgroundColor={'#202020'}
                                                                withOverlay={false}
                                                                popover={<Text style={styles.popoverText}>${speedSaved} Potential Fuel Savings</Text>}>
                                                                <Icon
                                                                    name='information'
                                                                    size={17}
                                                                    type='material-community'
                                                                    iconStyle={styles.tooltipIcon}
                                                                />
                                                            </Tooltip>
                                                        </View>
                                                    </View>
                                                </View>
                                            </View>
                                        </View>
                                    )
                                }
                            </View>
                        )}
                </ScrollView>
                <View style={styles.shortcutsContainer}>
                    <TouchableOpacity
                        onPress={() => this.checkPhone(phone, 'call', driverId)}
                        style={styles.shortcutButton}>
                        <Icon
                            name='phone'
                            size={20}
                            type='material-community'
                            iconStyle={styles.shortcutIcon}
                        />
                        <Text style={styles.shortcutText}>Call</Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                        onPress={() => this.checkPhone(phone, 'whatsapp', driverId)}
                        style={styles.shortcutButton}>
                        <Icon
                            name='whatsapp'
                            size={20}
                            type='material-community'
                            iconStyle={styles.shortcutIcon}
                        />
                        <Text style={styles.shortcutText}>Whatsapp</Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                        onPress={() => {
                            let trackReqObj = {
                                vehicleID: vID,
                                start: '/Date(' + moment.utc().subtract(1, "day").unix() * 1000 + ')/',
                                end: '/Date(' + moment().unix() * 1000 + ')/'
                            }
                            this.props.navigation.navigate('Tracks', trackReqObj);
                        }}
                        style={styles.shortcutButton}>
                        <Icon
                            name='navigation'
                            size={20}
                            type='material-community'
                            iconStyle={styles.shortcutIcon}
                        />
                        <Text style={styles.shortcutText}>Trackpoints</Text>
                        <Text style={styles.shortcutText}>24h</Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                        onPress={() => {
                            (activeVeh.Immobilizer !== -1) && (activeVeh.Category !== 'Mobile Device') ?
                                this.immobilize(activeVeh) : {}
                        }}
                        style={styles.shortcutButton}>
                        {!!activeVeh && (activeVeh.Immobilizer !== -1) && (activeVeh.Category !== 'Mobile Device') ?
                            //if immobilizer is either enabled or disabled,
                            (<Icon
                                iconStyle={{ color: (activeVeh.Immobilizer != 1 ? '#3d3f4c' : '#27ae60') }}
                                size={20}
                                type='material-community'
                                name={(activeVeh.Immobilizer != 1 ? 'lock-open-outline' : 'lock-outline')} />) :
                            //else if immobilizer is not installed,
                            (<View style={styles.immoIconContainer}>
                                <Icon
                                    iconStyle={styles.immoTinyIcon}
                                    size={10}
                                    type='material-community'
                                    name='cancel' />
                                <Icon
                                    iconStyle={styles.immoLockIcon}
                                    size={18}
                                    type='material-community'
                                    name='lock-open-outline' />
                            </View>)}
                        <Text style={styles.immoShortcutText}>Immobilizer</Text>
                        <Text style={styles.immoStatusText}>{immoStat}</Text>
                    </TouchableOpacity>
                </View>
            </View>
        );
    }
}

function mapStateToProps(state) {
    return {
        vehicles: state.vehicles,
        token: state.token,
        users: state.users,
        map: state.map
    }
}

const mapDispatchToProps = {
    UpdateUsers,
    LoadMarker
};

export default connect(mapStateToProps, mapDispatchToProps)(VehicleInfo)