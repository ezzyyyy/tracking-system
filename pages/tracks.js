import React from 'react';
import { Alert, Text, FlatList, View, TouchableOpacity, ActivityIndicator } from 'react-native';
import { Icon, Card } from 'react-native-elements';

//redux
import { connect } from 'react-redux'

//components
import TrackHeader from '../fragments/trackheader';
import MapTracks from '../fragments/maptracks'

//API
import { GetPositionReport } from '../api/reportapi';

//styles
import { styles, speedingStyles, idlingStyles } from '../assets/styles/resultliststyle'

var moment = require('moment');
moment().format();

class TracksPage extends React.Component {
    _isMounted = false;
    _keyExtractor = (item, index) => item.PosID.toString();
    constructor(props) {
        super(props);
        this.iconType = 'material-community';
        this.state = {
            latLng: [],
            draw: false,
            trackPoints: [],
            showLoader: false,
            loaderText: '',
            NoTracktext: 'To show Trackpoints, please select a start time, end time and a vehicle.'
        };
    }

    componentDidMount() {
        this._isMounted = true;
        const { params } = this.props.navigation.state;
        if (!!params) {
            this.showDayTracks(params)
        }
    }

    componentWillUnmount() {
        this._isMounted = false;
    }

    async showDayTracks(params) {
        if (params.vehicleID == 0)
            Alert.alert('Please select a vehicle.')
        else {
            try {
                let count = 300;
                let posID = 0;
                let positions = [];

                this.setState({ trackPoints: [], draw: false, showLoader: true, loaderText: 'Loading...' });

                while (count == 300) {
                    await GetPositionReport(this.props.token, params.start, params.end, params.vehicleID, posID)
                        .then((res) => {
                            count = res.GetPositionReportResult.length;
                            if (count === 0) {
                                alert('There are no reports for this vehicle during this period. Please try again.');
                            } else {
                                Array.prototype.push.apply(positions, res.GetPositionReportResult);
                                this.setState({ loaderText: 'Loading (' + positions.length + ')' });
                                if (count > 0)
                                    posID = res.GetPositionReportResult[count - 1].PosID;
                            }
                        })
                        .catch((error) => {
                            Alert.alert(error);
                        });
                }
                positions.sort(function (a, b) { return a.PosID - b.PosID });
                this.setState({ trackPoints: positions, showLoader: false }, () => {
                    if (count !== 0)
                        this.drawTracks();
                });
            } catch {
                Alert.alert('Error', 'Oops, something went wrong, please try again.');
            }
        }
    }

    getTracks = async (state) => {
        if (state.vehicle == 0)
            Alert.alert('Please select a vehicle.');
        else if (moment(new Date(state.startDate)) > moment(new Date(state.endDate)))
            Alert.alert('End date should be later than start date.');
        else {
            try {
                let count = 300;
                let posID = 0;
                positions = [];
                start = '/Date(' + moment(new Date(state.startDate)) + ')/';
                end = '/Date(' + moment(new Date(state.endDate)) + ')/';

                this.setState({ trackPoints: [], draw: false, showLoader: true, loaderText: 'Loading...' });
                while (count == 300) {
                    await GetPositionReport(this.props.token, start, end, state.vehicle, posID)
                        .then((res) => {
                            count = res.GetPositionReportResult.length;
                            if (count === 0) {
                                alert('There are no reports for this vehicle during this period. Please try again.');
                            } else {
                                Array.prototype.push.apply(positions, res.GetPositionReportResult);
                                this.setState({ loaderText: 'Loading (' + positions.length + ')' });
                                if (count > 0)
                                    posID = res.GetPositionReportResult[count - 1].PosID;
                            }
                        })
                        .catch((error) => {
                            Alert.alert(error);
                        });
                }
                positions.sort(function (a, b) { return a.PosID - b.PosID });
                this.setState({ trackPoints: positions, showLoader: false }, () => {
                    if (count !== 0)
                        this.drawTracks();
                });
            } catch {
                Alert.alert('Error', 'Oops, something went wrong, please try again.');
            }
        }
    }

    drawTracks = () => {
        var latLng = [];
        this.state.trackPoints.map(v => {
            const obj = {
                latitude: v.Latitude,
                longitude: v.Longitude,
                loc: !!v.Location ? v.Location : 'No recorded location',
                ts: v.Timestamp
            };
            latLng.push(obj)
        });
        if (this._isMounted)
            this.setState({ draw: true, latLng: latLng });
    }

    renderList = ({ item }) => {
        let v = item
        var ignition = 'STOPPED';
        if (v.Engine == 1)
            ignition = 'IDLING';
        else if (v.Engine == 2)
            ignition = 'MOVING';

        let loc = item.Location;
        let cut = loc.split(',');
        let cutLoc = (cut[0] + ',' + cut[1]);

        return (<Card id={'track-' + v.PosID}>
            <View style={styles.cardItem}>
                <View style={[speedingStyles.iconGroup, { width: 75 }]}>
                    <Icon iconStyle={speedingStyles.icon} name='speedometer' type={this.iconType} />
                    <Text style={speedingStyles.speedText}>{ignition}</Text>
                </View>
                <View style={speedingStyles.detailsBody}>
                    <View style={speedingStyles.timeBody}>
                        <View style={speedingStyles.startTimeGroup}>
                            <Text style={speedingStyles.time}>{moment(v.Timestamp).format('LTS')}</Text>
                            <Text style={speedingStyles.date}>{moment(v.Timestamp).format('l')}</Text>
                        </View>
                    </View>
                    <View style={idlingStyles.locationBody}>
                        <Text style={speedingStyles.label}>Location: </Text>
                        <Text style={speedingStyles.locMarquee}>{cutLoc}</Text>
                    </View>
                </View>
            </View>
        </Card>);
    }

    render() {
        const { params } = this.props.navigation.state;

        return (
            <View style={{ flex: 1 }}>
                <TrackHeader token={this.props.token} getTracks={this.getTracks} params={!!params ? params : null} />
                {(!this.state.draw) ?
                    (<View style={{ flex: 1 }}>
                        {(this.state.trackPoints.length > 0) ?
                            (<View>
                                <FlatList
                                    contentContainerStyle={{ paddingBottom: 10 }}
                                    style={styles.list}
                                    data={this.state.trackPoints}
                                    keyExtractor={this._keyExtractor}
                                    renderItem={this.renderList} />
                                <View style={{ paddingTop: 10, zIndex: 10, position: 'absolute', top: 0, right: 10 }}>
                                    <TouchableOpacity
                                        style={{ width: 50, height: 50, justifyContent: 'center', backgroundColor: '#3D3D3D' }}
                                        onPress={this.drawTracks}>
                                        <Icon iconStyle={{ color: '#fff', fontSize: 32 }} type={this.iconType} name='marker' />
                                    </TouchableOpacity>
                                </View>
                            </View>) :
                            (<View style={{ flex: 1, alignItems: 'center', justifyContent: 'center', padding: 20 }}>
                                <Text>{this.state.NoTracktext}</Text>
                            </View>)}
                    </View>) :
                    (<MapTracks latLng={this.state.latLng} />)}
                {(this.state.showLoader) ?
                    <View style={{
                        position: 'absolute',
                        top: 0,
                        bottom: 0,
                        left: 0,
                        right: 0,
                        justifyContent: 'center',
                        alignItems: 'center'
                    }}>
                        <View style={{
                            backgroundColor: 'rgba(255,255,255,1)',
                            width: 150, 
                            height: 150,
                            justifyContent: 'center',
                            alignItems: 'center',
                            borderRadius: 5,
                            shadowColor: '#000',
                            shadowOffset: {
                                width: 0,
                                height: 1,
                            },
                            shadowOpacity: 0.22,
                            shadowRadius: 2.22,
                            elevation: 3
                        }}>
                            <ActivityIndicator
                                size='large'
                                style={{
                                    width: 75,
                                    height: 75
                                }}
                                color='rgb(50, 154, 152)' />
                            <Text style={{
                                color: 'rgb(50, 154, 152)',
                                fontSize: 13,
                                margin: 10
                            }}>{this.state.loaderText}</Text>
                        </View>
                    </View> : null}
            </View>
        );
    }
}

function mapStateToProps(state) {
    return {
        token: state.token
    }
}

const mapDispatchToProps = {
};

export default connect(mapStateToProps, mapDispatchToProps)(TracksPage)