import React from 'react';
import { View, TouchableOpacity, Text, Alert, Platform, Dimensions, TextInput } from 'react-native';
import { Icon } from 'react-native-elements';

//styles
import { styles } from '../assets/styles/forgotstyle';
import { iosstyle } from '../assets/styles/iosstyle';

//api
import { ResetPassword } from '../api/authenticationapi';


class ForgotPasswordPage extends React.Component {

    constructor(props) {
        super(props);
        this.iconType = 'material-community';
        this.state = {
            username: '',
            headerIPhoneXorAbove: false
        }
    }

    componentDidMount() {
        let d = Dimensions.get('window');
        const { height, width } = d;

        if (Platform.OS === 'ios' && ((height === 812 || width === 812) || (height === 896 || width === 896))) {
            this.setState({ iphoneXorAbove: true });
        }
    }

    reset = async () => {
        let username = this.state.username;

        if (!username) {
            Alert.alert(
                'Empty fields!',
                'Please enter your username.',
                [{ text: 'OK', onPress: () => { } }]
            );
        } else {
            let result = await ResetPassword(username);
            result = result.ResetPasswordResult;
            if (result.Message === 'OK') {
                Alert.alert(
                    'Success!',
                    'Your password has been reset. Please check your email.',
                    [{ text: 'OK', onPress: () => { } }]
                );
            } else {
                Alert.alert(
                    'Failed!',
                    'Please enter a valid username.',
                    [{ text: 'OK', onPress: () => { } }]
                );
            }
        }
    }

    render() {
        return (
            <View style={this.state.iphoneXorAbove ? iosstyle.forgotContainer : styles.container}>
                <View>
                    <Text style={styles.forgotText}>We just need your registered username to send you a password reset.</Text>
                </View>
                <View style={styles.inputView}>
                    <View style={styles.searchSection}>
                        <Icon iconStyle={styles.searchIcon} name='account' size={20} color='rgb(50, 154, 152)' type='material-community' />
                        <TextInput
                            style={styles.input}
                            placeholder='Username'
                            placeholderTextColor='rgba(50, 154, 152, 0.7)'
                            selectionColor='rgb(50, 154, 152)'
                            onChangeText={e => this.setState({ username: e })}
                            underlineColorAndroid='transparent'
                        />
                    </View>
                    <TouchableOpacity
                        style={styles.resetBtn}
                        onPress={this.reset}>
                        <Text style={styles.resetText}>RESET</Text>
                    </TouchableOpacity>
                </View>
            </View>
        );
    }
}

export default ForgotPasswordPage

