import React from 'react';
import { View, Text, ActivityIndicator } from 'react-native';

//components
import ReportHeader from '../../fragments/reportheader';
import ResultList from '../../fragments/resultlist';

//API
import { GetGeofencingReport } from '../../api/reportapi';

//asyncstorage
import { GetCompanyID } from '../../api/asyncstorage'

//styles
import { styles } from '../../assets/styles/reportsstyle';

const geofencingReport = {
    type: 'geofencing',
    note: 'To show Geofencing report, please select a start time, end time and a vehicle.'
}

var moment = require('moment');
moment().format();

class GeofencingPage extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            geofencingTrackPoints: [],
            trackLength: undefined,
            loading: false,
            report: [],
        };
    }

    viewGeofencingReport = async (token, vehicle, start, end) => {
        this.setState({ geofencingTrackPoints: [], trackLength: undefined, loading: true });
        let trackPoints = [];
        let count = 300;
        let page = 0;

        let company = await GetCompanyID();

        token = this.state.report.token;
        vehicle = this.state.report.vehicle;
        start = this.state.report.start;
        end = this.state.report.end;
        
        try {
            while (count === 300) {
                let result = await GetGeofencingReport(token, start, end, vehicle, page, company);
                result = result.GetGeofencingReportResult;
                count = result.length;

                if (result.length === 0) {
                    alert('There are no reports for this vehicle during this period. Please try again.');
                } else {
                    page = result[result.length - 1].GeofencingID;
                    result.forEach((point) => {
                        point.Name = this.state.report.vehicleName;
                        trackPoints.push(point)
                        return true
                    });
                    this.setState({ trackLength: trackPoints.length });
                }
            }
            this.setState({ geofencingTrackPoints: trackPoints, loading: false });
            console.log(trackPoints)
        } catch (error) {
            console.log(error)
        }
    }

    getDetails = (data) => { this.setState({ report: data }); }

    goToMap = (locs) => { this.props.navigation.navigate('ReportLocation', locs); }

    render() {
        return (
            <View style={{ flex: 1 }}>
                <ReportHeader
                    viewReport={this.viewGeofencingReport}
                    passReportDetails={this.getDetails}
                />
                <View style={styles.reportList}>
                    {(this.state.geofencingTrackPoints.length !== 0) ? (
                        <ResultList
                            resultType={geofencingReport.type}
                            dataArray={this.state.geofencingTrackPoints}
                            goToGeofencingMap={this.goToMap} 
                        />
                    ) : (
                            <View style={styles.note}>
                                <Text style={styles.noteText}>
                                    {geofencingReport.note}
                                </Text>
                            </View>
                        )}
                    {(!!this.state.loading) && (
                        <View style={styles.loadingView}>
                            <View style={styles.spinnerView}>
                                <ActivityIndicator size='large' style={styles.spinner} color='rgb(50, 154, 152)' />
                                <Text style={styles.spinnerText}>
                                    {this.state.trackLength ?
                                        ('Loading (' + this.state.trackLength + ')') :
                                        ('Loading...')
                                    }
                                </Text>
                            </View>
                        </View>
                    )}
                </View>
            </View>
        );
    }
}

export default GeofencingPage