import React from 'react';
import { View, Text, ActivityIndicator } from 'react-native';

//components
import ReportHeader from '../../fragments/reportheader';
import ResultList from '../../fragments/resultlist';

//API
import { GetJourneyReport } from '../../api/reportapi';

//asyncstorage
import { GetCompanyID } from '../../api/asyncstorage'

//styles
import { styles } from '../../assets/styles/reportsstyle';

const startStopReport = {
    type: 'startstop',
    note: 'To show Start-stop report, please select a start time, end time and a vehicle.'
}

var moment = require('moment');
moment().format();

class StartStopPage extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            startStopTrackPoints: [],
            trackLength: undefined,
            loading: false,
            report: [],
        };
    }

    viewStartStopReport = async (token, vehicle, start, end) => {
        this.setState({ startStopTrackPoints: [], trackLength: undefined, loading: true });
        let trackPoints = [];
        let count = 300;
        let page = 0;

        let company = await GetCompanyID();

        token = this.state.report.token;
        vehicle = this.state.report.vehicle;
        start = this.state.report.start;
        end = this.state.report.end;

        try {
            while (count === 300) {
                let result = await GetJourneyReport(token, start, end, vehicle, page, company);
                result = result.GetJourneyReportResult;
                count = result.length;

                if (count === 0) {
                    alert('There are no reports for this vehicle during this period. Please try again.');
                } else {
                    page = result[result.length - 1].JourneyID;
                    result.forEach((point) => {
                        point.Name = this.state.report.vehicleName;
                        trackPoints.push(point)
                        return true
                    });
                    this.setState({ trackLength: trackPoints.length });
                }
            }
            this.setState({ startStopTrackPoints: trackPoints, loading: false });
        } catch (error) {
            throw new Error(400);
        }
    }

    getDetails = (data) => { this.setState({ report: data }); }

    goToMap = (data) => { this.props.navigation.navigate('ReportLocation', data) }

    render() {
        return (
            <View style={{ flex: 1 }}>
                <ReportHeader
                    viewReport={this.viewStartStopReport}
                    passReportDetails={this.getDetails}
                />
                <View style={styles.reportList}>
                    {(this.state.startStopTrackPoints.length !== 0) ? (
                        <ResultList
                            resultType={startStopReport.type}
                            dataArray={this.state.startStopTrackPoints}
                            goToStartStopMap={this.goToMap}
                        />
                    ) : (
                            <View style={styles.note}>
                                <Text style={styles.noteText}>
                                    {startStopReport.note}
                                </Text>
                            </View>
                        )}
                    {(!!this.state.loading) && (
                        <View style={styles.loadingView}>
                            <View style={styles.spinnerView}>
                                <ActivityIndicator size='large' style={styles.spinner} color='rgb(50, 154, 152)' />
                                <Text style={styles.spinnerText}>
                                    {this.state.trackLength ?
                                        ('Loading (' + this.state.trackLength + ')') :
                                        ('Loading...')
                                    }
                                </Text>
                            </View>
                        </View>
                    )}
                </View>
            </View>
        );
    }
}

export default StartStopPage