import React from 'react';
import { ScrollView, Text, View, TouchableOpacity, FlatList, ActivityIndicator } from 'react-native';
import { Icon, SearchBar } from 'react-native-elements';
import moment from 'moment';
import Highlighter from 'react-native-highlight-words';

//redux 
import { connect } from 'react-redux'
import { UpdateVehicle, ToggleUpdateVehicle, LoadMarker } from '../redux/actions/pages';

//styles
import { styles } from '../assets/styles/vehiclestyle';

//api
import { GetUtilizationReport, GetDrivingReport } from '../api/reportapi'

//asyncstorage
import { GetCompanyID } from '../api/asyncstorage';

class VehiclesPage extends React.Component {
    constructor(props) {
        super(props);
        this.list = [];
        this.iconType = 'material-community';
        this.state = {
            vehicles: [],
            online: 0,
            offline: 0,
            token: undefined,
            statusToggle: 1,
            activeStatus: '#FFF',
            search: '',
            drivingObj: undefined,
            drivingObjPrev: undefined,
            durPercent: 0,
            durPercentPrev: 0,
            loading: false,
            loadingText: 'Generating...'
        }
    }

    navigateToScreen = (props, route, vehicle) => {
        props.LoadMarker('marker' + vehicle.VehicleID);
        let lat = !!vehicle.LastFix.Latitude ? vehicle.LastFix.Latitude : 1.210574;
        let lng = !!vehicle.LastFix.Longitude ? vehicle.LastFix.Longitude : 103.574804;
        let region = {
            latitude: lat,
            longitude: lng,
            latitudeDelta: 0.0042,
            longitudeDelta: 0.0042,
        };
        props.map.animateToRegion(region, 1000);
        props.navigation.navigate(route);
    }

    componentDidMount() { this.props.UpdateVehicle() }

    goToMarker = (veh) => {
        if (!!veh.LastFix && !!veh.LastFix.Latitude && !!veh.LastFix.Longitude)
            this.navigateToScreen(this.props, 'LiveView', veh);
        else
            alert('This vehicle has no last recorded location. It cannot be shown on map.');
    }

    updateSearch = search => this.setState({ search })

    onPressStatus = () => {
        const { statusToggle } = this.state;

        if (statusToggle === 1) this.setState({ statusToggle: 2 })
        else if (statusToggle === 2) this.setState({ statusToggle: 3 })
        else if (statusToggle === 3) this.setState({ statusToggle: 1 })
    }

    listVehicles = () => {
        let onlineVeh = [];
        let offlineVeh = [];
        let { vehicles } = this.props;
        let { statusToggle } = this.state;

        if (vehicles) {
            vehicles.vehicles.filter(veh => {
                if (veh.LastFix) {
                    ts = veh.LastFix.Timestamp;
                    if (moment().diff(moment(ts), 'days') > 0) {
                        offlineVeh.push(veh);
                        offlineVeh = offlineVeh.filter(veh => {
                            return (veh.Name.toLowerCase().indexOf(this.state.search.toLowerCase()) > -1 ||
                                veh.LastFix &&
                                veh.LastFix.Location.toLowerCase().indexOf(this.state.search.toLowerCase()) > -1);
                        })
                        return offlineVeh;
                    }
                    else {
                        onlineVeh.push(veh);
                        onlineVeh = onlineVeh.filter(veh => {
                            return (veh.Name.toLowerCase().indexOf(this.state.search.toLowerCase()) > -1 ||
                                veh.LastFix &&
                                veh.LastFix.Location.toLowerCase().indexOf(this.state.search.toLowerCase()) > -1);
                        })
                        return onlineVeh;
                    }
                }
                else {
                    offlineVeh.push(veh);
                    offlineVeh = offlineVeh.filter(veh => {
                        return (veh.Name.toLowerCase().indexOf(this.state.search.toLowerCase()) > -1 ||
                            veh.LastFix &&
                            veh.LastFix.Location.toLowerCase().indexOf(this.state.search.toLowerCase()) > -1);
                    });
                    return offlineVeh;
                }
            });

            let list = vehicles.vehicles;

            if (statusToggle === 1)
                list = list.filter(veh => {
                    return (veh.Name.toLowerCase().indexOf(this.state.search.toLowerCase()) > -1 ||
                        veh.LastFix &&
                        veh.LastFix.Location.toLowerCase().indexOf(this.state.search.toLowerCase()) > -1);
                });
            else if (statusToggle === 2) list = onlineVeh;
            else if (statusToggle === 3) list = offlineVeh;

            this.list = list;

            return list;
        }
    }

    goToVehicleInfo = async (vID, dID) => {
        let startPrevMonth = '/Date(' + moment().subtract(1, 'months').startOf('month').unix() * 1000 + ')/';
        let endPrevMonth = '/Date(' + moment().subtract(1, 'months').endOf('month').unix() * 1000 + ')/';

        let start = '/Date(' + moment().startOf('month').unix() * 1000 + ')/';
        let end = '/Date(' + moment().unix() * 1000 + ')/';

        let drivingObj = {};
        let drivingObjPrev = {};

        // Generate Driver Report this month and last month
        if (!!dID) {
            drivingObjPrev = await this.getDrivingReport(dID, startPrevMonth, endPrevMonth);
            drivingObj = await this.getDrivingReport(dID, start, end);
        }

        // Generate Utilization Report this month and last month
        let durPercent = await this.getUtilizationReport(vID, start, end);
        let durPercentPrev = await this.getUtilizationReport(vID, startPrevMonth, endPrevMonth);

        this.props.navigation.navigate('VehicleInfo', {
            vID,
            dID,
            durPercent: !!durPercent ? durPercent : 0,
            durPercentPrev: !!durPercentPrev ? durPercentPrev : 0,
            drivingObj: !!drivingObj ? drivingObj : undefined,
            drivingObjPrev: !!drivingObjPrev ? drivingObjPrev : undefined
        })
    }

    getDrivingReport = async (driver, start, end) => {
        this.setState({ loading: true, loadingText: 'Driving...' })
        let trackPoints = [];
        let count = 300;
        let page = 0;

        let company = await GetCompanyID();
        let token = this.props.token;

        try {
            while (count === 300) {
                let result = await GetDrivingReport(token, start, end, driver, page, company);
                result = result.GetDrivingReportResult;
                count = result.length;

                if (result.length === 0) {
                    // alert('There are no reports for this vehicle during this period. Please try again.');
                    //do nothing
                } else {
                    page = result[result.length - 1].DriverID;
                    result.forEach((point, index) => {
                        trackPoints.push(point)
                        return true
                    });
                    trackPoints.sort(function (a, b) {
                        return trackPoints[trackPoints.length - 1].DriverID - trackPoints[0].DriverID;
                    });
                    this.setState({ loading: false, loadingText: 'Driving...' })
                }
            }
        } catch (error) {
            alert('Driver report request failed: 400');
        }

        return trackPoints[0];
    }

    getUtilizationReport = async (vehicle, start, end) => {
        this.setState({ loading: true, loadingText: 'Utilization...' })
        let durPoints = [];
        let trackPoints = [];
        let count = 300;
        let page = 0;
        let durPercent = 0;

        let company = await GetCompanyID();
        let token = this.props.token;

        try {
            while (count === 300) {
                let result = await GetUtilizationReport(token, start, end, vehicle, page, company);
                result = result.GetUtilizationReportResult;
                count = result.length;

                if (result.length === 0) {
                    // alert('There are no reports for this vehicle during this period. Please try again.');
                    //do nothing
                } else {
                    page = result[result.length - 1].UtilID;
                    result.forEach((point, index) => {
                        trackPoints.push(point)
                        return true
                    });
                    trackPoints.sort(function (a, b) {
                        return trackPoints[trackPoints.length - 1].UtilID - trackPoints[0].UtilID;
                    });
                    this.setState({ loading: false, loadingText: 'Done!' })
                }
            }
        } catch (error) {
            alert('Utilization report request failed: 400');
        }

        trackPoints.forEach((point, index) => {
            let move = !!point.MoveDuration ? point.MoveDuration : 0;
            let idle = !!point.IdleDuration ? point.IdleDuration : 0;
            let total = !!point.TotalDuration ? point.TotalDuration : 0;

            durPoints.push({ move, idle, total });
            return durPoints
        })

        let totalSum = durPoints.reduce((a, b) => durPoints[durPoints.length - 1].total + durPoints[0].total, 0);
        let moveSum = durPoints.reduce((a, b) => durPoints[durPoints.length - 1].move + durPoints[0].move, 0);
        let idleSum = durPoints.reduce((a, b) => durPoints[durPoints.length - 1].idle + durPoints[0].idle, 0);

        durPercent = ((moveSum + idleSum) / totalSum) * 100;
        durPercent = Math.round(durPercent);

        return durPercent;
    }

    renderVehicle = ({ item }) => {
        let catId = [
            'human',
            'bicycle',
            'bus-side',
            'car-side',
            'truck',
            'truck',
            'forklift',
            'towing',
            'motorbike',
            'truck',
            'truck',
            'ferry',
            'truck',
            'taxi',
            'truck',
            'truck',
            'train',
            'truck',
            'van-utility',
            'cellphone-iphone'
        ]

        let name = item.Name;

        let loc = !!item.LastFix ? item.LastFix.Location : 'No last location';
        let cut = loc.split(',');
        let cutLoc = !!item.LastFix && !!item.LastFix.Location ? (cut[0] + ',' + cut[1] + ',' + cut[2]) : 'No last location';

        let lastseen = !!item.LastFix ? parseInt(item.LastFix.Timestamp.substring(6, 19)) : 'No last seen';
        lastseen = !!lastseen ? moment.unix(lastseen / 1000).format('DD/MM/YYYY hh:mm:ss A') : 'No last seen';

        let engColor = '#2980b9';
        let stat = 'STOPPED';

        if (item.LastFix) {
            if (moment().diff(moment(item.LastFix.Timestamp), 'days') == 0) {
                if (item.LastFix.Engine == 1) {
                    engColor = 'orange';
                    stat = 'IDLE'
                }
                else if (item.LastFix.Engine == 2) {
                    engColor = '#27ae60';
                    stat = 'MOVING';
                }
            }
            else {
                engColor = '#95a5a6';
                stat = 'STOPPED';
            }
        } else {
            engColor = '#7a7a7a';
            stat = 'STOPPED';
        }

        return (
            <TouchableOpacity
                onPress={() => this.goToVehicleInfo(item.VehicleID, item.DriverID)}
                style={styles.infoContainer}>
                <View style={styles.iconContainer}>
                    <Icon
                        name={!!item.CategoryID ? catId[item.CategoryID] : 'cellphone-iphone'}
                        type={this.iconType}
                        size={25}
                        iconStyle={{ color: engColor }} />
                </View>
                <View style={styles.detailsContainer}>
                    <Text style={styles.name}>
                        <Highlighter
                            highlightStyle={styles.highlight}
                            searchWords={[this.state.search]}
                            textToHighlight={name}
                        />
                    </Text>
                    <Text style={styles.otherText}>
                        <Highlighter
                            highlightStyle={styles.highlight}
                            searchWords={[this.state.search]}
                            textToHighlight={cutLoc}
                        />
                    </Text>
                    <Text style={styles.otherText}>{lastseen}</Text>
                </View>
                <TouchableOpacity
                    onPress={() => this.goToMarker(item)}
                    style={{
                        alignItems: 'center',
                        justifyContent: 'center',
                        padding: 10
                    }}>
                    <Icon
                        name='map-search'
                        type={this.iconType}
                        size={18}
                        iconStyle={{ color: '#abadac' }} />
                </TouchableOpacity>
            </TouchableOpacity>
        )
    }

    render() {
        const { statusToggle, search } = this.state;
        const { vehicles } = this.props;
        const toggle = statusToggle === 1 ? (vehicles ? 'ALL: ' + vehicles.vehicles.length : 0) :
            statusToggle === 2 ? (vehicles ? 'ONLINE: ' + vehicles.online : 0) :
                (vehicles ? 'OFFLINE: ' + vehicles.offline : 0)

        return (
            <View style={styles.container}>
                <View style={styles.searchContainer}>
                    <SearchBar
                        value={search}
                        placeholder='Search for vehicle or location...'
                        onChangeText={this.updateSearch}
                        searchIcon={{ size: 20, color: '#ffffff' }}
                        containerStyle={styles.searchBarContainer}
                        inputContainerStyle={styles.searchInputContainer}
                        inputStyle={styles.searchInput}
                        placeholderTextColor='#ffffff70'
                        selectionColor='yellow'
                    />
                    <TouchableOpacity
                        style={styles.statusToggleBtn}
                        onPress={this.onPressStatus}>
                        <Text style={styles.statusText}>{toggle}</Text>
                    </TouchableOpacity>
                </View>
                <ScrollView
                    style={styles.scrollView}
                    contentContainerStyle={styles.contentContainer}>
                    <FlatList
                        data={this.listVehicles()}
                        renderItem={this.renderVehicle}
                        keyExtractor={item => item.Name}
                    />
                </ScrollView>
                {(!!this.state.loading) && (
                    <View style={styles.loadingView}>
                        <View style={styles.spinnerView}>
                            <ActivityIndicator size='large' style={styles.spinner} color='rgb(50, 154, 152)' />
                            <Text style={styles.spinnerText}>{this.state.loadingText}</Text>
                        </View>
                    </View>
                )}
            </View>
        );
    }
}

function mapStateToProps(state) {
    return {
        vehicles: state.vehicles,
        map: state.map,
        token: state.token
    }
}

const mapDispatchToProps = {
    ToggleUpdateVehicle,
    UpdateVehicle,
    LoadMarker
};

export default connect(mapStateToProps, mapDispatchToProps)(VehiclesPage)