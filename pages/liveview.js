import React from 'react'
import { TouchableOpacity, View, LayoutAnimation, Text, Switch, Platform, ActivityIndicator } from 'react-native'
import AsyncStorage from '@react-native-community/async-storage';
import Toast from 'react-native-easy-toast'
import MapView, { PROVIDER_GOOGLE, PROVIDER_DEFAULT } from 'react-native-maps'
import { Icon } from 'react-native-elements'
import Firebase from 'react-native-firebase'
import type, { Notification, NotificationOpen } from 'react-native-firebase'

//redux 
import { connect } from 'react-redux'
import { UpdateVehicle, LoadMap, LoadGeofences } from '../redux/actions/pages'

//components
import LiveMapMarker from '../fragments/markers'
import Geofences from '../fragments/geofences'
import BottomSheet from '../fragments/bottomsheet'

//styles
import LiveViewStyle from '../assets/styles/liveviewstyle'

//asyncstorage
import { GetUserData, GetMap, GetUserCred, GetCompanyID } from '../api/asyncstorage'

//API
import { Login } from '../api/authenticationapi'
import { GetDrivingReport, GetUtilizationReport } from '../api/reportapi'

var moment = require('moment')
moment().format()

const mapTypes = ['standard', 'hybrid', 'satellite', 'none']
const mapTiles = ['Google Map', 'One Map', 'Openstreetmap']

class LiveViewPage extends React.Component {
    constructor(props) {
        super(props);
        this.iconType = 'material-community';
        this.mapRef = null;
        this.mapTout = null;
        this.mapTLoad = null;
        this.coor = [];
        this.isLoaded = false;
        this.state = {
            selectedMarkerIndex: null,
            regionLat: 1.290270,
            regionLng: 103.851959,
            showTraffic: false,
            activeIndex: 0,
            user: [],
            bottom: -100,
            mapType: 0,
            maxZoomLevel: 0,
            geofenceToggle: false,
            sgCoordinate: [{
                latitude: 1.210574,
                longitude: 103.574804
            }, {
                latitude: 1.420460,
                longitude: 104.003752
            }],
            provider: PROVIDER_GOOGLE,
            drivingObj: undefined,
            drivingObjPrev: undefined,
            durPercent: 0,
            durPercentPrev: 0,
            loading: false,
            loadingText: 'Generating...'
        };
    }

    componentDidMount() {
        this.props.UpdateVehicle(this.props);
        // load geofences
        this.props.LoadGeofences();
        this.props.LoadMap(this.mapRef);
        this.getUser();
        this.getInitMap();

        AsyncStorage.getItem('GEO_TOGGLE').then(res => {
            if (!!res) {
                let geoStatus = JSON.parse(res);
                this.setState({ showGeofences: geoStatus })
            }
        })

        //FCM functions
        this.onTokenRefreshListener();
        Firebase.messaging().hasPermission().then(hasPermission => {
            if (hasPermission) {
                this.subscribeToNotificationListeners();
                this.getFCMToken();
            } else {
                Firebase.messaging().requestPermission().then(() => {
                    this.subscribeToNotificationListeners();
                }).catch(error => console.error(error));
            }
        })
    }

    componentDidUpdate() {
        const { params } = this.props.navigation.state;

        if (params) {
            this.setState({ vinfo: params });
        }

        if (this.props.vehicles && !this.isLoaded) {
            if (this.coor.length == 0) {
                this.props.vehicles.vehicles.map(v => {
                    if (!!v.LastFix && !!v.LastFix.Latitude && !!v.LastFix.Longitude) {
                        this.coor.push({
                            latitude: v.LastFix.Latitude,
                            longitude: v.LastFix.Longitude
                        });
                    }
                });
            }

            if (this.mapTout == null) {
                this.mapTout = setTimeout(() => {
                    if (this.mapRef !== null) {
                        this.mapRef.fitToCoordinates(this.coor, {
                            edgePadding: {
                                top: 150,
                                right: 150,
                                bottom: 150,
                                left: 150
                            }
                        });
                    }
                    this.isLoaded = true;
                }, 3000);
            }
        }
    }

    componentWillUnmount() {
        this.onTokenRefreshListener();
        this.notificationListener();
        this.notificationOpenedListener();
    }

    subscribeToNotificationListeners() {
        const channel = new Firebase.notifications.Android.Channel(
            'skyfy-vts-channel',
            'Skyfy VTS Channel',
            Firebase.notifications.Android.Importance.Max)
            .setDescription('Channel for Skyfy VTS Notifications');

        Firebase.notifications().android.createChannel(channel);

        this.notificationListener = Firebase.notifications().onNotification(async (notification) => {
            console.log('onNotification notification-->', notification);
            this.displayNotification(notification)
        });

        this.notificationOpenedListener = Firebase.notifications().onNotificationOpened((notification) => {
            this.props.navigation.navigate('Alerts');
        });

        // you can check the initial notification here
        this.getInitialNotificationListener = Firebase.notifications().getInitialNotification().then((notification) => {
            if (notification) {
                this.displayNotification(notification)
            }
        });
    }

    displayNotification = (notification) => {
        if (Platform.OS === 'android') {
            const localNotification = new Firebase.notifications.Notification({
                data: notification.data,
                sound: 'default',
                show_in_foreground: true,
                title: notification.title,
                body: notification.body
            }).setNotificationId(notification.notificationId)
                .setTitle(notification.title)
                .setSubtitle(notification.subtitle)
                .setBody(notification.body)
                .setData(notification.data)
                .android.setChannelId('skyfy-vts-channel') // e.g. the id you chose above
                .android.setSmallIcon('@drawable/notification_icon') // create this icon in Android Studio
                .android.setColor('#7dc6b7') // you can set a color here
                .android.setCategory(Firebase.notifications.Android.Category.Alarm)
                .android.setPriority(Firebase.notifications.Android.Priority.Max)
                .android.setVisibility(Firebase.notifications.Android.Visibility.Public)
                .android.setVibrate(1000)

            Firebase.notifications()
                .displayNotification(localNotification)
                .catch(err => console.error(err));

        } else if (Platform.OS === 'ios') {
            const localNotification = new Firebase.notifications.Notification()
                .setNotificationId(notification.notificationId)
                .setTitle(notification.title)
                .setSubtitle(notification.subtitle)
                .setBody(notification.body)
                .setData(notification.data)
                .ios.setBadge(notification.ios.badge)

            Firebase.notifications()
                .displayNotification(localNotification)
                .catch(err => console.error(err));
        }
    }

    async onTokenRefreshListener() {
        Firebase.messaging().onTokenRefresh(
            fcmToken => {
                if (!!fcmToken) AsyncStorage.setItem('FCM_TOKEN', fcmToken);
            }
        );
    }

    async getFCMToken() {
        let fcmToken = await AsyncStorage.getItem('FCM_TOKEN');
        if (!fcmToken) {
            fcmToken = await Firebase.messaging().getToken();
            if (fcmToken) {
                await AsyncStorage.setItem('FCM_TOKEN', fcmToken);
                let user = await GetUserCred();
                if (user) {
                    user = JSON.parse(user);
                    let u_name = user.u_name;
                    let u_pass = user.u_pass;
                    let res = await Login(u_name, u_pass, fcmToken);
                    let key = res.LogInResult;
                    if (key) {
                        console.log('Re-saved FCM token. [' + key + ']');
                        AsyncStorage.setItem('USER_TOKEN', key);
                        let UserCred = { u_name: u_name, u_pass: u_pass, u_fcmToken: fcmToken };
                        AsyncStorage.setItem('USER_CRED', JSON.stringify(UserCred));
                    }
                }
            }
        }
    }

    getUser = async () => {
        let user = await GetUserData();
        this.setState({ user: JSON.parse(user) });
    }

    getInitMap = async () => {
        let smap = await GetMap();
        let nZoom = 0;
        let provider = PROVIDER_GOOGLE;
        if (smap == '' || smap == null) {
            smap = 0;
        }
        if (smap == '1') {
            this.mapRef.fitToCoordinates(this.state.sgCoordinate);
            this.coor = this.state.sgCoordinate;
            nZoom = 11;
            provider = PROVIDER_DEFAULT;
        }
        this.setState({ provider: provider, mapType: smap, maxZoomLevel: nZoom, activeIndex: (smap == '0' ? 0 : 3) });
    }

    changeMapLayout = async () => {
        const smap = await GetMap();
        let nMap = 0;
        let nZoom = 0;
        let nmapType = 3;
        let provider = PROVIDER_DEFAULT;
        if (smap == null || smap == '0') {
            this.mapRef.fitToCoordinates(this.state.sgCoordinate);
            nZoom = 11;
            nMap = 1;
        }
        else if (smap == '1') {
            nMap = 2;
        }
        else {
            nMap = 0;
            nmapType = 0;
            provider = PROVIDER_GOOGLE
        }

        this.refs.toast.show('Changed map to ' + mapTiles[nMap] + `${nMap == 2 ? ' (iOS only)' : ''}`);
        AsyncStorage.setItem('MAPTYPE', nMap.toString());
        this.setState({ mapType: nMap, maxZoomLevel: nZoom, activeIndex: nmapType, provider: provider });
    }

    changeMapStyle = () => {
        let activeIndex = !!this.state.activeIndex ? this.state.activeIndex : 0;
        if (activeIndex === (mapTypes.length - 1)) { activeIndex = 0 }
        else { activeIndex++ }
        this.setState({ activeIndex }, () => {
            this.refs.toast.show('Map type changed to ' +
                mapTypes[this.state.activeIndex]);
        });
    }

    loadMapType = () => {
        if (this.state.mapType == 1) {
            return <MapView.UrlTile
                urlTemplate='https://maps-a.onemap.sg/v3/Default/{z}/{x}/{y}.png' />;
        }
        else if (this.state.mapType == 2) {
            return <MapView.UrlTile
                urlTemplate='http://c.tile.openstreetmap.org/{z}/{x}/{y}.png' />;
        }
        else {
            return null;
        }
    }

    toggleGeofence = (value) => {
        this.setState({ showGeofences: value })
        AsyncStorage.setItem('GEO_TOGGLE', JSON.stringify(value));
    }

    toggleTraffic = () => {
        this.setState({ showTraffic: !this.state.showTraffic });
        this.refs.toast.show(!this.state.showTraffic ? 'Display traffic enabled!' :
            'Display traffic disabled!');
    }

    _onPress(vehicle, index) {
        LayoutAnimation.spring();
        this.setState({ selectedMarkerIndex: index, vInfo: vehicle, bottom: 0 });
    }

    showMarkers = (v, i) => {
        return (
            <LiveMapMarker
                key={'marker' + v.VehicleID}
                ref={ref => this.marker = ref}
                onPress={() => {
                    this._onPress(v, i)
                    console.log(v)
                }}
                vinfo={v}
            />
        )
    }

    closeBS = () => this.setState({ vInfo: null })

    goToVehInfo = async (vID, dID) => {
        let startPrevMonth = '/Date(' + moment().subtract(1, 'months').startOf('month').unix() * 1000 + ')/';
        let endPrevMonth = '/Date(' + moment().subtract(1, 'months').endOf('month').unix() * 1000 + ')/';

        let start = '/Date(' + moment().startOf('month').unix() * 1000 + ')/';
        let end = '/Date(' + moment().unix() * 1000 + ')/';

        let drivingObj = {};
        let drivingObjPrev = {};

        // Generate Driver Report this month and last month
        if (!!dID) {
            drivingObjPrev = await this.getDrivingReport(dID, startPrevMonth, endPrevMonth);
            drivingObj = await this.getDrivingReport(dID, start, end);
        }

        // Generate Utilization Report this month and last month
        let durPercent = await this.getUtilizationReport(vID, start, end);
        let durPercentPrev = await this.getUtilizationReport(vID, startPrevMonth, endPrevMonth);

        this.props.navigation.navigate('VehicleInfo', {
            vID,
            dID,
            durPercent: !!durPercent ? durPercent : 0,
            durPercentPrev: !!durPercentPrev ? durPercentPrev : 0,
            drivingObj: !!drivingObj ? drivingObj : undefined,
            drivingObjPrev: !!drivingObjPrev ? drivingObjPrev : undefined
        })
    }

    getDrivingReport = async (driver, start, end) => {
        this.setState({ loading: true, loadingText: 'Driving...' })
        let trackPoints = [];
        let count = 300;
        let page = 0;

        let company = await GetCompanyID();
        let token = this.props.token;

        try {
            while (count === 300) {
                let result = await GetDrivingReport(token, start, end, driver, page, company);
                result = result.GetDrivingReportResult;
                count = result.length;

                if (result.length === 0) {
                    // alert('There are no reports for this vehicle during this period. Please try again.');
                    //do nothing
                } else {
                    page = result[result.length - 1].DriverID;
                    result.forEach((point, index) => {
                        trackPoints.push(point)
                        return true
                    });
                    trackPoints.sort(function (a, b) {
                        return trackPoints[trackPoints.length - 1].DriverID - trackPoints[0].DriverID;
                    });
                    this.setState({ loading: false, loadingText: 'Driving...' })
                }
            }
        } catch (error) {
            alert('Driver report request failed: 400');
        }

        return trackPoints[0];
    }

    getUtilizationReport = async (vehicle, start, end) => {
        this.setState({ loading: true, loadingText: 'Utilization...' })
        let durPoints = [];
        let trackPoints = [];
        let count = 300;
        let page = 0;
        let durPercent = 0;

        let company = await GetCompanyID();
        let token = this.props.token;

        try {
            while (count === 300) {
                let result = await GetUtilizationReport(token, start, end, vehicle, page, company);
                result = result.GetUtilizationReportResult;
                count = result.length;

                if (result.length === 0) {
                    // alert('There are no reports for this vehicle during this period. Please try again.');
                    //do nothing
                } else {
                    page = result[result.length - 1].UtilID;
                    result.forEach((point, index) => {
                        trackPoints.push(point)
                        return true
                    });
                    trackPoints.sort(function (a, b) {
                        return trackPoints[trackPoints.length - 1].UtilID - trackPoints[0].UtilID;
                    });
                    this.setState({ loading: false, loadingText: 'Done!' })
                }
            }
        } catch (error) {
            alert('Utilization report request failed: 400');
        }

        trackPoints.forEach((point, index) => {
            let move = !!point.MoveDuration ? point.MoveDuration : 0;
            let idle = !!point.IdleDuration ? point.IdleDuration : 0;
            let total = !!point.TotalDuration ? point.TotalDuration : 0;

            durPoints.push({ move, idle, total });
            return durPoints
        })

        let totalSum = durPoints.reduce((a, b) => durPoints[durPoints.length - 1].total + durPoints[0].total, 0);
        let moveSum = durPoints.reduce((a, b) => durPoints[durPoints.length - 1].move + durPoints[0].move, 0);
        let idleSum = durPoints.reduce((a, b) => durPoints[durPoints.length - 1].idle + durPoints[0].idle, 0);

        durPercent = ((moveSum + idleSum) / totalSum) * 100;
        durPercent = Math.round(durPercent);

        return durPercent;
    }

    goToTracks = (data) => this.props.navigation.navigate('Tracks', data)

    render() {
        const isNotGoogleMap = (this.state.mapType == 1 || this.state.mapType == 2); //Not Google map type boolean
        return (
            <View style={LiveViewStyle.container}>
                <View style={LiveViewStyle.mapContainer}>
                    <MapView
                        style={LiveViewStyle.map}
                        provider={this.state.provider}
                        showsCompass={false}
                        toolbarEnabled={false}
                        rotateEnabled={false}
                        mapType={mapTypes[this.state.activeIndex]}
                        showsTraffic={this.state.showTraffic}
                        ref={(ref) => { this.mapRef = ref }}
                        showsCompass={false}
                        minZoomLevel={this.state.maxZoomLevel}
                        initialRegion={{
                            latitude: this.state.regionLat,
                            longitude: this.state.regionLng,
                            latitudeDelta: 0.3000,
                            longitudeDelta: 0.3000
                        }}>
                        {this.loadMapType()}
                        {(this.props.vehicles) ?
                            this.props.vehicles.vehicles.map((v, i) => (
                                !!v.LastFix && !!v.LastFix.Latitude && !!v.LastFix.Longitude ?
                                    this.showMarkers(v, i) : null
                            )) : null}
                        {(this.state.showGeofences) ?
                            (<Geofences geofences={this.props.geofences} />) : null}
                    </MapView>
                    <View style={LiveViewStyle.geoContainer}>
                        <Text style={LiveViewStyle.geoText}>GEOFENCES</Text>
                        <Switch
                            onValueChange={this.toggleGeofence}
                            value={!!this.state ? this.state.showGeofences : null}
                            style={{ transform: Platform.OS === 'ios' ? [{ scaleX: .5 }, { scaleY: .5 }] : [{ scaleX: .8 }, { scaleY: .8 }] }}
                            thumbColor={'#e67e23'}
                            trackColor={{
                                false: '#e67e2360',
                                true: '#e67e2320'
                            }}
                        />
                    </View>
                    <View style={LiveViewStyle.buttonsView}>
                        <TouchableOpacity
                            onPress={this.changeMapLayout}
                            style={LiveViewStyle.actionButton}>
                            <Icon
                                type={this.iconType} iconStyle={LiveViewStyle.sideBtnIcon} name='map' />
                        </TouchableOpacity>
                        <TouchableOpacity
                            onPress={this.changeMapStyle}
                            disabled={isNotGoogleMap ? true : false}
                            style={isNotGoogleMap ? LiveViewStyle.disabledBtn : LiveViewStyle.actionButton}>
                            <Icon type={this.iconType} iconStyle={LiveViewStyle.sideBtnIcon} name='layers' />
                        </TouchableOpacity>
                        <TouchableOpacity
                            onPress={this.toggleTraffic}
                            disabled={isNotGoogleMap ? true : false}
                            style={isNotGoogleMap ? LiveViewStyle.disabledBtn : LiveViewStyle.actionButton}>
                            <Icon type={this.iconType} iconStyle={LiveViewStyle.sideBtnIcon} name='traffic-light' />
                        </TouchableOpacity>
                    </View>
                    {(this.state.vInfo) ?
                        (
                            <View pointerEvents='box-none' style={LiveViewStyle.bottomSheet}>
                                <BottomSheet vehicle={this.state.vInfo} sendDetails={this.goToVehInfo} closeBS={this.closeBS} goToTracks={this.goToTracks} />
                            </View>
                        ) : null}
                    <Toast
                        ref='toast'
                        style={LiveViewStyle.toast}
                        position='bottom'
                        positionValue={200}
                        fadeInDuration={100}
                        fadeOutDuration={100}
                        textStyle={LiveViewStyle.toastText}
                    />
                    {(!!this.state.loading) && (
                        <View style={LiveViewStyle.loadingView}>
                            <View style={LiveViewStyle.spinnerView}>
                                <ActivityIndicator size='large' style={LiveViewStyle.spinner} color='rgb(50, 154, 152)' />
                                <Text style={LiveViewStyle.spinnerText}>{this.state.loadingText}</Text>
                            </View>
                        </View>
                    )}
                </View>
            </View>
        );
    }
}

function mapStateToProps(state) {
    return {
        vehicles: state.vehicles,
        map: state.map,
        marker: state.marker,
        geofences: state.geofences,
        token: state.token
    }
}

const mapDispatchToProps = {
    LoadMap,
    UpdateVehicle,
    LoadGeofences
};

export default connect(mapStateToProps, mapDispatchToProps)(LiveViewPage)
