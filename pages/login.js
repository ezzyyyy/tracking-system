import React from 'react';
import { View, Text, Image, Alert, TouchableOpacity, ActivityIndicator, TextInput } from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import Intercom from 'react-native-intercom';
import Firebase from 'react-native-firebase';
import type, { Notification, NotificationOpen } from 'react-native-firebase';
import { Icon } from 'react-native-elements'

//api
import { Login, GetUser, GetCompanies } from '../api/authenticationapi';

//redux 
import { connect } from 'react-redux'
import { UpdateToken, StartLocation, LoadVehicles, ToggleUpdateVehicle, UpdateUserData, LoadGeofences } from '../redux/actions/pages';

//styles
import { styles } from '../assets/styles/loginstyle';

class LoginPage extends React.Component {
    static navigationOptions = {
        header: null,
    };

    constructor(props) {
        super(props);
        this.iconType = 'material-community';
        this.state = {
            username: '',
            password: '',
            token: undefined,
            fcmToken: undefined,
            trackLength: undefined,
            loading: false,
            eye: true
        }
    }

    componentDidMount() {
        Firebase.messaging().hasPermission().then(hasPermission => {
            if (hasPermission) {
                this.getFCMToken()
            } else {
                Firebase.messaging().requestPermission().then(() => {
                }).catch(error => console.error(error))
            }
        })
    }

    async getFCMToken() {
        let fcmToken = await AsyncStorage.getItem('FCM_TOKEN');
        if (fcmToken == null || fcmToken == undefined) {
            fcmToken = await Firebase.messaging().getToken();
            if (fcmToken) {
                this.setState({ fcmToken })
                await AsyncStorage.setItem('FCM_TOKEN', fcmToken);
            }
        } else {
            this.setState({ fcmToken })
            console.log(fcmToken)
        }
    }

    login = async (username, password, fcmToken) => {
        username = this.state.username;
        password = this.state.password;
        fcmToken = this.state.fcmToken;

        if (!username || !password) {
            Alert.alert(
                'Empty fields!',
                'Please enter your username & password.',
                [{ text: 'OK', onPress: () => console.log('Trying logging in again...') }]
            );
        } else {
            let result = await Login(username, password, fcmToken);
            let token = result.LogInResult;
            if (!token) {
                Alert.alert(
                    'Incorrect username/password',
                    'Please try again.',
                    [{ text: 'OK', onPress: () => { } }]
                );
            } else {
                this.setState({ loading: true });
                console.log(token)
                let UserCred = { u_name: username, u_pass: password, u_fcmToken: fcmToken };

                AsyncStorage.setItem('USER_TOKEN', token);
                AsyncStorage.setItem('USER_CRED', JSON.stringify(UserCred));
                this.setState({ trackLength: undefined });

                let result = await GetUser(token);
                let userData = result.GetUserResult;

                Intercom.logEvent('viewed_screen', { extra: 'metadata' });
                Intercom.registerIdentifiedUser({ userId: userData.Name });
                Intercom.updateUser({
                    // Pre-defined user attributes
                    email: userData.Email,
                    user_id: userData.UserID.toString(),
                    name: userData.Name,
                    phone: userData.Phone.toString(),
                    app_id: 'hi30znee',
                    companies: [{
                        company_id: userData.Group.CompanyID.toString(),
                        name: userData.Group.Company
                    }]
                });

                if (!!userData) {
                    AsyncStorage.setItem('USER', JSON.stringify(userData));
                    let companyID = userData.Group.CompanyID;
                    AsyncStorage.setItem('COMPANY_ID', JSON.stringify(companyID));

                    let page = 0;
                    let count = 300;
                    let companies = [];

                    while (count === 300) {
                        let data = await GetCompanies(token, page);
                        let result = data.GetCompaniesResult;
                        count = result.length;

                        //Sort by ascending order
                        result.sort(function (a, b) { return a.CompanyID - b.CompanyID });

                        //Get the highest ID
                        page = result[count - 1].CompanyID;
                        result.forEach((point) => { companies.push(point); return true; });
                        this.setState({ trackLength: companies.length })
                    }
                    AsyncStorage.setItem('USER_COMPANIES', JSON.stringify(companies));
                    this.setState({ loading: false });

                    this.props.UpdateUserData();
                    this.props.navigation.navigate('LiveView');
                    this.props.ToggleUpdateVehicle(true);
                    this.props.LoadVehicles();
                    this.props.LoadGeofences(token);

                    // if(userData.EnableTracking)
                    //     this.props.StartLocation();

                    this.props.UpdateToken(token);
                }
            }
        }
    }

    handlePressIn = () => this.setState({ eye: false })
    handlePressOut = () => this.setState({ eye: true })

    render() {
        return (
            <View style={styles.container}>
                <Image style={styles.image} source={require('../assets/imgs/mbg1.jpg')} />
                <View style={styles.logo}>
                    <View style={styles.imageContainer}>
                        <Image style={styles.icon} source={require('../assets/imgs/skyfy_logo.png')} />
                    </View>
                </View>
                <View style={{ paddingHorizontal: 40 }}>
                    <View style={styles.userInputContainer}>
                        <View style={styles.searchSection}>
                            <Icon iconStyle={styles.searchIcon} name='account' size={20} color='rgb(50, 154, 152)' type='material-community' />
                            <TextInput
                                style={styles.input}
                                placeholder='Username'
                                placeholderTextColor='rgba(50, 154, 152, 0.5)'
                                selectionColor='rgb(50, 154, 152)'
                                onChangeText={e => this.setState({ username: e })}
                                underlineColorAndroid='transparent'
                            />
                        </View>
                    </View>
                    <View style={styles.pwInputContainer}>
                        <View style={styles.searchSection}>
                            <Icon iconStyle={styles.searchIcon} name='key' size={20} color='rgb(50, 154, 152)' type='material-community' />
                            <TextInput
                                style={styles.input}
                                placeholder='Password'
                                placeholderTextColor='rgba(50, 154, 152, 0.5)'
                                selectionColor='rgb(50, 154, 152)'
                                onChangeText={e => this.setState({ password: e })}
                                underlineColorAndroid='transparent'
                                secureTextEntry={this.state.eye}
                            />
                            <TouchableOpacity
                                onPressIn={this.handlePressIn}
                                onPressOut={this.handlePressOut}
                                style={styles.searchIcon}>
                                <Icon name='eye' size={20} color='rgb(50, 154, 152)' type='material-community' />
                            </TouchableOpacity>
                        </View>
                    </View>
                </View>
                <View style={styles.buttonContainer}>
                    <TouchableOpacity
                        style={styles.button}
                        onPress={this.state.loading ? () => { } : this.login}>
                        <Text style={styles.loginText}>LOGIN</Text>
                    </TouchableOpacity>
                </View>
                <View style={styles.forgotContainer}>
                    <TouchableOpacity
                        onPress={() => this.props.navigation.navigate('ForgotPassword')}
                        style={styles.forgotButton}>
                        <Text style={styles.forgot}>Forgot Password? Click here</Text>
                    </TouchableOpacity>
                </View>
                {(!!this.state.loading) && (
                    <View style={styles.loadingView}>
                        <View style={styles.spinnerView}>
                            <ActivityIndicator size='large' style={styles.spinner} color='rgb(50, 154, 152)' />
                            <Text style={styles.spinnerText}>
                                {this.state.trackLength ?
                                    ('Loading (' + this.state.trackLength + ')') :
                                    ('Loading...')
                                }
                            </Text>
                        </View>
                    </View>
                )}
            </View>
        );
    }
}

function mapStateToProps(state) {
    return {
        vehicles: state.vehicles,
        map: state.map,
        token: state.token
    }
}

const mapDispatchToProps = {
    StartLocation,
    ToggleUpdateVehicle,
    LoadVehicles,
    UpdateToken,
    UpdateUserData,
    LoadGeofences
}

export default connect(mapStateToProps, mapDispatchToProps)(LoginPage)

