import React from 'react';
import { View, Image } from 'react-native'

//redux 
import { connect } from 'react-redux'
import { UpdateToken, StartLocation, ToggleUpdateVehicle, LoadVehicles, UpdateUserData } from '../redux/actions/pages';

//asyncstorage
import { GetToken } from '../api/asyncstorage';

//styles
import { loadingStyles } from '../assets/styles/loadingstyle';

class LoadingPage extends React.Component {
    constructor(props) {
        super(props);
    }

    componentDidMount() { this.getToken(); }

    async getToken() {
        let token = await GetToken();
        if (token) {
            this.props.UpdateToken(token);
            this.props.UpdateUserData();
            this.props.ToggleUpdateVehicle(true);
            this.props.LoadVehicles(this.props);
            setTimeout(() => { this.props.navigation.navigate('LiveView') }, 1000);
        }
        else
            this.props.navigation.navigate('Login');
    }

    render() {
        return (
            <View style={{ flex: 1 }}>
                <Image style={loadingStyles.image} source={require('../assets/imgs/mbg1.jpg')} />
                <View style={loadingStyles.imgOverlayColor}>
                    <View style={loadingStyles.container}>
                        <View style={loadingStyles.imageContainer}>
                            <Image style={loadingStyles.icon} source={require('../assets/imgs/skyfy_logo.png')} />
                        </View>
                    </View>
                </View>
            </View>
        );
    }
}

function mapStateToProps(state) {
    return {
        vehicles: state.vehicles,
        map: state.map
    }
}

const mapDispatchToProps = {
    StartLocation,
    UpdateToken,
    UpdateUserData,
    ToggleUpdateVehicle,
    LoadVehicles
};
export default connect(mapStateToProps, mapDispatchToProps)(LoadingPage)
