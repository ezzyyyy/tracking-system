import { createAppContainer, createSwitchNavigator } from 'react-navigation'

//redux
import { Provider } from 'react-redux'
import store from './redux/store'
import React from 'react'

//pages
import { AuthStack } from './router/auth'
import { DrawerStack } from './router/drawer'
import LoadingPage from './pages/loading'

const AppContainer = createAppContainer(
    createSwitchNavigator(
        {
            Loading: LoadingPage,
            Main: DrawerStack,
            Auth: AuthStack
        },
        { initialRouteName: 'Loading' }
    )
);

export default class App extends React.Component {
    constructor(props) { super(props); }

    render() {
        return (
            <Provider store={store}>
                <AppContainer />
            </Provider>
        );
    }
}