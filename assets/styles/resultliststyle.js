import { StyleSheet } from 'react-native';

export const styles = StyleSheet.create({
  list: {
    zIndex: 1,
    margin: 0,
    marginBottom: 10
  },
  cardItem: {
    flex: 1,
    justifyContent: 'flex-start',
    alignItems: 'center',
    backgroundColor: '#ffffff',
    flexDirection: 'row',
    padding: 10,
    borderBottomWidth: 1,
    borderBottomColor: '#f2f2f2'
  },
})

export const startStopStyles = StyleSheet.create({
  iconGroup: {
    flexDirection: 'column',
    alignItems: 'center',
  },
  iconStop: {
    color: '#EB4F51',
    fontSize: 50,
  },
  iconStart: {
    color: '#27ae60',
    fontSize: 50,
  },
})

export const messageStyles = StyleSheet.create({
  sectionContainer: {
    flex: 1,
    justifyContent: 'flex-start',
    alignItems: 'center',
    backgroundColor: '#ffffff',
    flexDirection: 'row',
    padding: 10,
    borderBottomWidth: 1,
    borderBottomColor: '#f2f2f2'
  },
  detailsContainer: {
    flex: 1,
    flexDirection: 'column',
    padding: 10
  },
  body: {
    fontSize: 13,
    color: '#666'
  },
  time: {
    fontSize: 12,
    color: '#999',
    paddingTop: 10,
    textAlign: 'right'
  },
  linkStyle: {
    justifyContent: 'center',
    color: '#2980b9',
    fontSize: 13
  },
})

export const speedingStyles = StyleSheet.create({
  iconGroup: {
    flexDirection: 'column',
    alignItems: 'center',
  },
  icon: {
    color: '#EB4F51',
    fontSize: 45,
  },
  speedText: {
    fontWeight: 'bold',
    color: '#EB4F51',
    fontSize: 14,
  },
  detailsBody: {
    height: '100%',
    flex: 1,
    padding: 10,
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'space-between'
  },
  locationBody: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'flex-start'
  },
  label: {
    color: '#999',
    fontSize: 13
  },
  locMarquee: {
    width: '85%',
    fontSize: 13,
    color: '#555'
  },
  timeBody: {
    paddingVertical: 10,
    height: '100%',
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center'
  },
  startTimeGroup: {
    marginLeft: 10,
    flex: 1,
    flexDirection: 'column',
    alignItems: 'flex-start'
  },
  endTimeGroup: {
    marginRight: 10,
    flex: 1,
    flexDirection: 'column',
    alignItems: 'flex-end',
  },
  time: {
    color: '#555',
    fontWeight: 'bold',
    fontSize: 13
  },
  date: {
    color: '#999',
    fontSize: 13
  },
  lineDurationBody: {
    flex: 1,
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center'
  },
  line: {
    borderColor: '#555',
    borderBottomWidth: 1.5,
    width: '90%'
  },
  duration: {
    color: '#999',
    fontSize: 12
  }
});

export const idlingStyles = StyleSheet.create({
  cardItem: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between'
  },
  detailsBody: {
    height: '100%',
    flex: 1,
    padding: 10,
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'space-between'
  },
  icon: {
    color: '#e67e22',
    fontSize: 45,
    marginRight: 10
  },
  timeBody: {
    paddingBottom: 10,
    height: '100%',
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center'
  },
  startTimeGroup: {
    marginLeft: 10,
    flex: 1,
    flexDirection: 'column',
    alignItems: 'flex-start',
    justifyContent: 'center'
  },
  time: {
    color: '#555',
    fontWeight: 'bold',
    fontSize: 13
  },
  date: {
    color: '#999',
    fontSize: 13
  },
  endTimeGroup: {
    marginRight: 10,
    flex: 1,
    flexDirection: 'column',
    alignItems: 'flex-end',
    justifyContent: 'center'
  },
  line: {
    borderColor: '#555',
    borderBottomWidth: 1.5,
    width: '90%'
  },
  duration: {
    color: '#999',
    fontSize: 13
  },
  lineDurationBody: {
    flex: 1,
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center'
  },
  locMarquee: {
    fontSize: 13,
    alignSelf: 'center',
    color: '#555'
  },
  locationBody: {
    flexDirection: 'row',
    marginLeft: 10,
    flex: 1
  }
});

export const geoStyles = StyleSheet.create({
  container: {
    borderWidth: 1,
    borderColor: '#555',
  }
})