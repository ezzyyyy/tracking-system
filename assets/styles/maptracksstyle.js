import { StyleSheet } from 'react-native';

export const styles = StyleSheet.create({
    container: {
        flex: 1,
        width: '100%',
        alignItems: 'center',
        justifyContent: 'center'
    },
    icon: {
        color: 'red'
    },
    sliderContainer: {
        backgroundColor: 'rgba(255,255,255,0.7)',
        position: 'absolute',
        bottom: 20,
        borderRadius: 5,
        padding: 10,
        left: 20,
        right: 20
    },
    slider: {
        height: 40
    },
    buttonsContainer: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row'
    },
    prev: {
        color: '#000000'
    },
    next: {
        color: '#000000'
    },
    pause: {
        color: '#000000',
        padding: 10
    }
});