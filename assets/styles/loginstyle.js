import { StyleSheet } from 'react-native';

export const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'rgba(50, 154, 152, 0.5)'
    },
    image: {
        height: '100%',
        width: '100%',
        opacity: 0.3,
        position: 'absolute',
        top: 0,
        left: 0,
        bottom: 0,
        right: 0,
        resizeMode: 'cover'
    },
    logo: {
        flex: 1,
        justifyContent: 'flex-end',
        paddingBottom: 30,
        alignItems: 'center'
    },
    imageContainer: {
        width: '100%'
    },
    icon: {
        alignSelf: 'center',
        width: 100,
        height: 108
    },
    userInputContainer: {
        height: 60
    },
    label: {
        fontSize: 16,
        color: 'rgb(50, 154, 152)'
    },
    loginText: {
        fontWeight: 'bold',
        color: '#FFF'
    },
    pwInputContainer: {
        marginTop: 8,
        height: 60
    },
    buttonContainer: {
        flex: 1,
        paddingHorizontal: 40,
        justifyContent: 'flex-start'
    },
    button: {
        marginTop: 30,
        paddingVertical: 18,
        backgroundColor: 'rgba(50, 154, 152, 0.8)',
        alignItems: 'center'
    },
    forgotContainer: {
        alignItems: 'center'
    },
    forgotButton: {
        padding: 20
    },
    forgot: {
        color: '#FFFFFF',
    },
    loadingView: {
        flex: 1,
        ...StyleSheet.absoluteFillObject,
        alignItems: 'center',
        justifyContent: 'center',
        zIndex: 100
    },
    spinnerView: {
        backgroundColor: 'rgba(255,255,255,1)',
        width: 150,
        height: 150,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 5,
        shadowColor: '#000',
        shadowOffset: {
            width: 0,
            height: 1,
        },
        shadowOpacity: 0.22,
        shadowRadius: 2.22,
        elevation: 3
    },
    spinnerText: {
        color: 'rgb(50, 154, 152)',
        fontSize: 13,
        margin: 10
    },
    spinner: {
        width: 75,
        height: 75
    },
    searchSection: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#fff',
    },
    searchIcon: {
        padding: 10,
        paddingHorizontal: 20
    },
    input: {
        flex: 1,
        paddingTop: 10,
        paddingRight: 10,
        paddingBottom: 10,
        paddingLeft: 0,
        backgroundColor: '#fff',
        color: 'rgb(50, 154, 152)',
    },
});