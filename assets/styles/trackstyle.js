import { StyleSheet } from 'react-native';

const TrackStyle = StyleSheet.create({
    trackList: {
        flex: 1, 
        alignItems: 'center', 
        justifyContent: 'center'
    },
})

export default TrackStyle;