import { StyleSheet } from 'react-native';

export const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    switchTab: {
        position: 'absolute',
        zIndex: 10,
        width: 40,
        height: 40,
        right: 0,
        justifyContent: 'center',
        alignItems: 'center',
        padding: 10,
        margin: 10,
        backgroundColor: 'rgba(50, 154, 152, 0.8)',
        borderRadius: 5
    },
    switchTabIcon: {
        color: '#FFFFFF'
    },
    dialogTitle: {
        fontSize: 15,
        color: '#3d3f4c'
    },
    dialogDesc: {
        fontSize: 13
    },
    dialogInput: {
        paddingLeft: 10
    },
    dialogInvalidDesc: {
        fontSize: 13,
        color: '#FF0000'
    },
    contentContainer: {
        padding: 15
    },
    scrollView: {
        flex: 1
    },
    vName: {
        fontSize: 15,
        color: '#3d3f4c',
        fontWeight: 'bold'
    },
    vTitle: {
        fontSize: 13,
        color: '#3d3f4c',
        marginTop: 10
    },
    vSubtitle: {
        fontSize: 13,
        color: '#abadac'
    },
    rowContainer: {
        flexDirection: 'row',
        justifyContent: 'flex-start',
        alignItems: 'center'
    },
    infoIcon: {
        color: '#C9C9C9',
        alignSelf: 'flex-start',
        paddingLeft: 5,
        marginTop: 10
    },
    popoverText: {
        color: 'white'
    },
    tooltipIcon: {
        color: '#C9C9C9',
        alignSelf: 'flex-end',
        paddingLeft: 4
    },
    arrowIcon: {
        alignSelf: 'flex-start'
    },
    colContainer: {
        flexDirection: 'column',
        justifyContent: 'flex-start',
        alignItems: 'center'
    },
    circlesContainer: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'flex-start',
        padding: 10
    },
    circleContainer: {
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
        margin: 5,
        padding: 10
    },
    scoreCircle: {
        margin: 15,
        borderRadius: 70 / 2,
        marginRight: 5
    },
    scoreCircleText: {
        color: 'rgb(242,158,113)',
        fontWeight: 'bold'
    },
    scoreTitle: {
        fontSize: 13,
        color: '#3d3f4c',
    },
    monthTitle: {
        fontSize: 10,
        color: '#3d3f4c',
    },
    utilCircle: {
        margin: 15,
        borderRadius: 70 / 2,
        marginRight: 5
    },
    utilCircleText: {
        color: '#2dbebb',
        fontWeight: 'bold'
    },
    utilTitle: {
        color: '#3d3f4c',
        fontSize: 13,
    },
    scoreDetailsContainer: {
        borderBottomWidth: 0.5,
        borderBottomColor: '#f2f2f2',
        borderTopWidth: 0.5,
        borderTopColor: '#f2f2f2'
    },
    scoreMonth: {
        alignSelf: 'center',
        padding: 10,
        color: '#3d3f4c'
    },
    scoreRow: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center'
    },
    scoreItemContainer: {
        flex: 1,
        padding: 10,
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'center'
    },
    scoreItemTitle: {
        fontSize: 13,
        color: '#3d3f4c',
    },
    scoreItem: {
        color: 'rgb(242,158,113)',
        fontWeight: 'bold',
        fontSize: 20,
    },
    shortcutsContainer: {
        justifyContent: 'space-between',
        alignItems: 'center',
        flexDirection: 'row',
        borderTopColor: '#f2f2f2',
        borderTopWidth: 1
    },
    shortcutButton: {
        flex: 1,
        padding: 10,
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'column'
    },
    shortcutText: {
        fontSize: 10,
        color: '#3d3f4c'
    },
    shortcutIcon: {
        color: '#3d3f4c',
        alignSelf: 'flex-start',
        marginBottom: 5
    },
    immoIconContainer: {
        marginBottom: 5
    },
    immoTinyIcon: {
        color: '#abadac',
        marginRight: 20
    },
    immoLockIcon: {
        color: '#abadac'
    },
    immoShortcutText: {
        fontSize: 10,
        color: '#3d3f4c',
        marginTop: 5
    },
    immoStatusText: {
        fontSize: 10,
        color: '#abadac'
    }
})