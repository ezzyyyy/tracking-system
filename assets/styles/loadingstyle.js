import { StyleSheet } from "react-native";

export const loadingStyles = StyleSheet.create({
    imgOverlayColor: {
        flex: 1,
        paddingLeft: 15,
        paddingRight: 15,
        backgroundColor: 'rgba(50, 154, 152, 0.5)'
    },
    image: {
        top: 0, 
        left: 0, 
        bottom: 0, 
        right: 0, 
        height: '100%', 
        width: '100%',
        opacity: 0.3, 
        position: "absolute", 
        resizeMode: 'cover', 
    },
    container: {
        justifyContent: 'center', 
        flex: 1,
        padding: 30
    },
    imageContainer: {
        marginBottom: 60,
        width: '100%'
    },
    icon: {
        alignSelf: 'center'
    }
  });