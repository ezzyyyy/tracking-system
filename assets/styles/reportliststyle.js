import { StyleSheet } from 'react-native';

export const styles = StyleSheet.create({
    container: {
        padding: 10,
        paddingVertical: 10,
        flexDirection: 'row'
    },
    cardContainer: {
        paddingHorizontal: 10,
        flex: 1,
        alignItems: 'center'
    },
    cardIdling: {
        backgroundColor: '#f19066',
        width: '100%',
        alignItems: 'center'
    },
    cardSpeeding: {
        backgroundColor: '#e66767',
        width: '100%',
        alignItems: 'center'
    },
    cardStartStop: {
        backgroundColor: 'rgb(118,193,181)',
        width: '100%',
        alignItems: 'center'
    },
    cardView: {
        paddingVertical: 20,
        flexDirection: 'column',
        alignItems: 'center'
    },
    icon: {
        color: 'white',
        fontSize: 70,
        marginBottom: 20
    },
    label: {
        color: 'white',
        fontWeight: 'bold',
        fontSize: 18
    },
    rowContainer: {
        paddingHorizontal: 10,
        flexDirection: 'row'
    }
});