import { StyleSheet, Platform } from 'react-native';

export const companyPickerStyles = StyleSheet.create({
    underline: { 
        opacity: 0 
    }, 
    inputAndroid: { 
        color: '#FFF',
    }, 
    inputIOS: { 
        color: '#FFF',
        fontSize: 18, 
    },
    inputIOSContainer: {
        alignItems: 'center',
        justifyContent: 'center',
        padding: 10,
    },
    icon: {
        height: '100%'
    }
});

export const styles = StyleSheet.create({
    navInfoBody: { 
        flexDirection: 'column', 
        justifyContent: 'space-between'
    },
    navuserinfo: {
        paddingTop: Platform.OS === 'ios' ? 10 : 0,
        height: 200,
        backgroundColor: 'rgba(0,0,0,0)'
    },
    navwelcome: {
        color: '#fff',
        fontSize: 24,
        fontWeight: 'bold',
        padding: 10
    },
    navpicker: {
        color: '#fff',
        paddingLeft: 10,
        width: '100%',
    },
    navemail: {
        color: '#fff',
        paddingLeft: 10,
        fontSize: 15,
    },
    container: {
        flex: 1,
    },
    navgroup: {
        flexDirection: 'column',
        borderBottomColor: '#f7f7f7',
        borderBottomWidth: 1,
    },
    navmenu: {
        flexDirection: 'row',
        borderBottomColor: '#f7f7f7',
        borderBottomWidth: 1,
    },
    menuitem: {
        flex: 1,
        justifyContent: 'center',
        aspectRatio: 1,
        alignItems: 'center',
        borderLeftColor: '#f7f7f7',
        borderLeftWidth: 1,
        borderRightColor: '#f7f7f7',
        borderRightWidth: 1
    },
    touchButton: {
        justifyContent: 'center',
        alignItems: 'center',
    },
    menuitemicon: {
        color: '#999999',
        fontSize: 32
    },
    menuitemiconreport: {
        color: '#999999',
        fontSize: 40
    },
    menuitemtext: {
        marginTop: 15,
        color: '#999999'
    },
    toast: {
        position: 'absolute',
        backgroundColor: 'rgba(0,0,0,0)'
    },
    toastText: {
        fontWeight: 'bold',
        color: '#555'
    },
});