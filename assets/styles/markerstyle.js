import { StyleSheet } from 'react-native';

export const MarkerStyle = StyleSheet.create({
    container: {
        flex: 1,
        width: '100%',
        alignItems: 'center',
        justifyContent: 'center'
    },
    markerContainer: {
        alignContent: 'center',
        alignItems: 'center',
        height: 56
    },
    infoBox: {
        padding: 5,
        backgroundColor: '#f8f8f8',
        borderWidth: 1,
        borderColor: '#d8d8d8',
        marginBottom: 5
    },
    vehName: {
        fontWeight: 'bold',
        fontSize: 18
    },
    vehInfo: {
        flexDirection: 'row',
        alignItems: 'center'
    },
    vehSpeedIcon: {
        marginRight: 5,
        fontSize: 16,
        color: '#D35400'
    },
    vehSpeedText: {
        textAlign: 'left',
        fontSize: 16,
        color: '#D35400'
    },
    navigator: {
        fontSize: 20,
        color: '#27ae60',
        position: 'absolute',
        right: -25
    },
    vehIcon: {
        backgroundColor: 'rgba(246, 248, 250, 0.8)',
        justifyContent: 'center',
        alignItems: 'center',
        borderWidth: 6,
        borderRadius: 48,
        width: 48,
        height: 48
    },
    downIcon: {
        padding: 0,
        margin: 0,
        fontSize: 36,
        position: 'absolute',
        bottom: -20
    }
});

export const GeoMarkerStyle = StyleSheet.create({
    container: {
        alignContent: 'center',
        alignItems: 'center'
    },
    infoBox: {
        padding: 5,
        backgroundColor: '#f8f8f8',
        borderWidth: 1,
        color: '#d8d8d8',
        marginBottom: 5
    },
    vehName: {
        fontWeight: 'bold',
        fontSize: 16
    },
    vehInfo: {
        flexDirection: 'row',
        alignItems: 'center'
    },
    vehIcon: {
        backgroundColor: 'rgba(246, 248, 250, 0.8)',
        justifyContent: 'center',
        alignItems: 'center',
        borderWidth: 6,
        borderRadius: 18,
        width: 18,
        height: 18
    }
});

export const styles = StyleSheet.create({
    markerView: {
        flex: 1,
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'center',
    },
    calloutView: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        flexDirection: 'row',
        backgroundColor: '#FFF',
        borderColor: '#DDE3E2',
        borderWidth: 1,
        padding: 5,
        margin: 5
    },
    calloutText: {
        fontSize: 12,
        color: '#666',
        marginRight: 3
    },
    imageView: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        width: 35,
        height: 35,
    },
    image: {
        width: '100%',
        height: '100%'
    },
    transparentCallout: {
        opacity: 0,
        backgroundColor: 'rgba(0,0,0,0)'
    }
});