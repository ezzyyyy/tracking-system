import { StyleSheet, Platform } from 'react-native';

export const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    header: {
        zIndex: 10,
        flexDirection: 'row',
        alignItems: 'center',
        padding: 10,
        justifyContent: 'flex-start',
        elevation: 30,
        shadowColor: '#000',
        shadowOffset: {
            width: 0,
            height: 10,
        },
        shadowOpacity: 0.1,
        backgroundColor: 'rgb(255,255,255)'
    },
    headerTitle: {
        paddingTop: Platform.OS === 'ios' ? 10 : 0,
        marginVertical: 5,
        fontWeight: 'bold',
        fontSize: 16,
        color: '#555'
    },
    icon: {
        marginVertical: 5,
        color: 'rgb(50, 154, 152)'
    },
    button: {
        paddingTop: Platform.OS === 'ios' ? 10 : 0,
        paddingRight: 10
    },
    map: {
        flex: 1,
        top: 0,
        left: 0,
        right: 0,
        bottom: 0
    },
    duration: {
        fontSize: 14, 
        fontWeight: 'bold'
    },
    location: {
        fontSize: 12
    },
    circle: {
        width: 30,
        height: 30,
        borderRadius: 30 / 2
    },
    markerIcon: {
        width: 30, 
        height: 30
    },
    pinText: {
        color: 'white',
        fontWeight: 'bold',
        textAlign: 'center',
        fontSize: 13,
        marginBottom: 10,
    },
    vehIcon: { 
        backgroundColor: 'rgba(246, 248, 250, 0.8)', 
        justifyContent: 'center', 
        alignItems: 'center', 
        borderWidth: 6, 
        borderRadius: 48, 
        width: 48, 
        height: 48 
    },
    markerContainer: {
        alignContent: 'center', 
        alignItems: 'center' 
    },
    downIcon: {
        padding: 0, 
        margin: 0, 
        fontSize: 36, 
        position: 'absolute', 
        bottom: -20 
    }
})