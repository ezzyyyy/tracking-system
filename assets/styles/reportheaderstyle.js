import { StyleSheet, Platform } from 'react-native';

export const dropdownStyles = StyleSheet.create({
    viewContainer: {
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'center',
        paddingHorizontal: 10,
        borderBottomWidth: 1,
        borderColor: '#abadac20'
    },
    container: {
        paddingVertical: Platform.OS === 'ios' ? 5 : 0,
        paddingHorizontal: 0,
    },
    textInput: {
        paddingVertical: 5,
        marginVertical: 5,
        color: '#666',
        textAlign: 'center'
    },
    item: {
        padding: 10,
        marginTop: 2,
    },
    itemText: {
        color: '#666'
    },
    itemsContainer: {
        backgroundColor: 'rgb(255,255,255)',
        zIndex: 3,
        maxHeight: 140
    },
})

export const datePickerStyles = StyleSheet.create({
    dateInput: {
        margin: 5,
        borderWidth: 0,
    },
    dateText: {
        color: '#555'
    }
});

export const reportHeaderStyles = StyleSheet.create({
    headerBody: {
        backgroundColor: '#FFF',
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'space-between',
        zIndex: 1
    },
    pickerGroupBody: {
        paddingVertical: 5,
        flexDirection: 'row',
        alignItems: 'flex-start',
        justifyContent: 'space-between',
        width: '100%',
        paddingHorizontal: 10,
        borderBottomWidth: 1,
        borderColor: '#abadac20'
    },
    datePickerStyle: {
        alignSelf: 'center'
    },
    dateBtn: {
        alignItems: 'center'
    },
    label: {
        fontSize: 14,
        alignSelf: 'center',
        color: '#abadac'
    },
    line: {
        borderColor: '#D8D8D8',
        borderLeftWidth: 1,
        height: '90%',
        marginHorizontal: 10,
        alignSelf: 'center'
    },
    vehiclePickerBody: {
        flexDirection: 'row',
        alignItems: 'flex-start',
        justifyContent: 'center',
        borderColor: '#999'
    }
});
