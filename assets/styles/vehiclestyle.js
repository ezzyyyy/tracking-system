import { StyleSheet } from 'react-native';

export const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'rgb(50, 154, 152)'
    },
    searchContainer: {
        paddingVertical: 5,
        flexDirection: 'row',
    },
    searchBarContainer: {
        backgroundColor: 'transparent',
        borderBottomWidth: 0,
        borderTopWidth: 0,
        width: '75%'
    },
    searchInputContainer: {
        backgroundColor: 'transparent'
    },
    searchInput: {
        fontSize: 13,
        color: '#ffffff',
    },
    statusToggleBtn: {
        flex: 1,
        backgroundColor: '#FFFFFF',
        margin: 7,
        borderRadius: 5,
        alignItems: 'center',
        justifyContent: 'center',
    },
    statusText: {
        fontWeight: 'bold',
        fontSize: 12,
        color: 'rgb(50, 154, 152)'
    },
    scrollView: {
        flex: 1,
        width: '100%',
        backgroundColor: '#f7f7f7'
    },
    contentContainer: {
        paddingVertical: 7,
    },
    infoContainer: {
        flex: 1,
        backgroundColor: '#FFFFFF',
        marginVertical: 1,
        padding: 5,
        flexDirection: 'row'
    },
    iconContainer: {
        alignItems: 'center',
        justifyContent: 'center',
        padding: 10
    },
    detailsContainer: {
        flex: 1,
        padding: 10
    },
    name: {
        fontSize: 15,
        color: '#3d3f4c',
        fontWeight: 'bold'
    },
    otherText: {
        fontSize: 13,
        color: '#abadac'
    },
    highlight: {
        backgroundColor: 'yellow',
        fontWeight: 'bold'
    },
    loadingView: {
        flex: 1,
        ...StyleSheet.absoluteFillObject,
        alignItems: 'center',
        justifyContent: 'center',
        zIndex: 100
    },
    spinnerView: {
        backgroundColor: 'rgba(255,255,255,1)',
        width: 150,
        height: 150,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 5,
        shadowColor: '#000',
        shadowOffset: {
            width: 0,
            height: 1,
        },
        shadowOpacity: 0.22,
        shadowRadius: 2.22,
        elevation: 3
    },
    spinnerText: {
        color: 'rgb(50, 154, 152)',
        fontSize: 13,
        margin: 10
    },
    spinner: {
        width: 75,
        height: 75
    },
});
