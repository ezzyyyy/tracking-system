import { StyleSheet, Platform } from 'react-native';

export const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    backIcon: {
        paddingTop: Platform.OS === 'ios' ? 10 : 0,
        marginVertical: 5,
        color: 'rgba(50, 154, 152, 1.0)',
        alignSelf: 'center'
    },
    header: {
        width: '100%',
        padding: 10,
        flexDirection: 'column',
        alignItems: 'flex-start',
    },
    forgotTitle: { 
        padding: 10, 
        paddingHorizontal: 20, 
        fontSize: 14, 
        fontWeight: 'bold' 
    },
    forgotText: {
        paddingTop: 20,
        paddingHorizontal: 20, 
        fontSize: 14 
    },
    icon: {
        fontSize: 100, 
        color: 'rgba(50, 154, 152, 0.7)',
        transform: [{ rotate: '-25deg' }]
    },
    resetBtn: { 
        padding: 15,
        marginTop: 30, 
        backgroundColor: 'rgba(50, 154, 152, 0.8)',
        alignItems: 'center'
    },
    resetText: {
        fontWeight: 'bold',
        color: '#FFF'
    },
    userInput: {
        borderBottomColor: '#E0E0E0'
    },
    emailInput: {
        paddingTop: 10, 
        borderBottomColor: '#E0E0E0'
    },
    leftIcon: {
        paddingBottom: 5, 
        color: 'rgba(50, 154, 152, 0.8)',
    },
    inputView: {
        padding: 20
    },
    searchSection: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        borderBottomColor: 'rgba(50, 154, 152, 0.7)',
        borderBottomWidth: 1
    },
    searchIcon: {
        padding: 10,
        paddingHorizontal: 20
    },
    input: {
        flex: 1,
        paddingTop: 10,
        paddingRight: 10,
        paddingBottom: 10,
        paddingLeft: 0,
        color: 'rgb(50, 154, 152)',
    },
});