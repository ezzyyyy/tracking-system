import { StyleSheet } from 'react-native';

export const styles = StyleSheet.create({
    container: {
        backgroundColor: '#FFF',
        flex: 1
    },
    scrollView: {
        marginBottom: 20
    },
    reportList: {
        flex: 1
    },
    note: {
        flex: 1,
        margin: 20,
        alignItems: 'center',
        justifyContent: 'center'
    },
    noteText: {
        color: '#999999',
        fontSize: 13
    },
    loadingView: {
        flex: 1,
        ...StyleSheet.absoluteFillObject,
        alignItems: 'center',
        justifyContent: 'center',
        zIndex: 100
    },
    spinnerView: {
        backgroundColor: 'rgba(255,255,255,1)',
        width: 150, 
        height: 150,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 5,
        shadowColor: '#000',
        shadowOffset: {
            width: 0,
            height: 1,
        },
        shadowOpacity: 0.22,
        shadowRadius: 2.22,
        elevation: 3
    },
    spinnerText: {
        color: 'rgb(50, 154, 152)',
        fontSize: 13,
        margin: 10
    },
    spinner: {
        width: 100,
        height: 100,
        color: 'rgb(102, 204, 153)'
    }
});