import { StyleSheet, Platform } from 'react-native';

const LiveViewStyle = StyleSheet.create({
    container: {
        flex: 1
    },
    mapContainer: {
        flex: 1,
        width: '100%',
        alignItems: 'center',
        justifyContent: 'center'
    },
    map: {
        position: 'absolute',
        top: 0,
        left: 0,
        right: 0,
        bottom: 0
    },
    geoContainer: {
        position: 'absolute',
        right: 15,
        top: 15,
        flexDirection: 'row',
        alignItems: 'center',
        backgroundColor: '#FFFFFF',
        borderRadius: 5
    },
    geoText: {
        fontSize: 11,
        color: '#e67e23',
        fontWeight: 'bold',
        marginLeft: 10
    },
    actionButton: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'rgba(255,255,255,1.0)',
        marginBottom: 10,
        padding: 10,
        width: 48,
        height: 48,
        borderRadius: 3,
        elevation: 3,
        shadowColor: '#000',
        shadowOffset: {
            width: 0,
            height: 0,
        },
        shadowOpacity: 0.3,
    },
    disabledBtn: {
        backgroundColor: 'rgba(255,255,255,0.6)',
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        marginBottom: 10,
        padding: 10,
        width: 48,
        height: 48,
        borderRadius: 3,
    },
    buttonsView: {
        position: 'absolute',
        flexDirection: 'column',
        alignItems: 'flex-start',
        zIndex: Platform.OS === 'android' ? 9 : 0,
        padding: 10,
        left: 5,
        top: 5,
    },
    bottomSheet: {
        position: 'absolute',
        zIndex: 10,
        left: 0,
        right: 0,
        bottom: 0
    },
    toast: {
        position: 'absolute',
        backgroundColor: '#FFF'
    },
    toastText: {
        fontWeight: 'bold',
        color: '#555'
    },
    sideBtnIcon: {
        color: 'rgb(50, 154, 152)',
        fontSize: 26,
    },
    loadingView: {
        flex: 1,
        ...StyleSheet.absoluteFillObject,
        alignItems: 'center',
        justifyContent: 'center',
        zIndex: 100
    },
    spinnerView: {
        backgroundColor: 'rgba(255,255,255,1)',
        width: 150,
        height: 150,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 5,
        shadowColor: '#000',
        shadowOffset: {
            width: 0,
            height: 1,
        },
        shadowOpacity: 0.22,
        shadowRadius: 2.22,
        elevation: 3
    },
    spinnerText: {
        color: 'rgb(50, 154, 152)',
        fontSize: 13,
        margin: 10
    },
    spinner: {
        width: 75,
        height: 75
    },
});

export default LiveViewStyle;